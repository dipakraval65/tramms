import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:TRAMMS/components/common.dart';
import 'package:TRAMMS/controllers/helpers/connectivity.dart';
import 'package:TRAMMS/controllers/helpers/database_helper.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/models/allocate_staff_users.dart' as AllocateStaffUsers;
import 'package:TRAMMS/models/pm_jobcard_timeline_data_model.dart' as PMJobCardTimeLineData;
import 'package:TRAMMS/models/login_model.dart';
import 'package:TRAMMS/models/monthly_inspection.dart' as MonthlyInspection;
import 'package:TRAMMS/models/offline_weekly_model.dart'as OfflineWeeklyModel;
import 'package:TRAMMS/models/view_weekly_plan_model.dart'
    as ViewWeeklyPlanModel;
import 'package:TRAMMS/views/authentication/login_screen.dart';
import 'package:TRAMMS/views/dashboard/home_screen.dart';
import 'package:background_location/background_location.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_mac/get_mac.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/pm_jobdata_model.dart' as PMJobDataModel;
import 'package:multi_image_picker2/multi_image_picker2.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

class UserProvider with ChangeNotifier {
  String? _globalURL = "";
  String? _macAddress = '';
  bool? _loadingAuth = false;
  LoginModel? loginModel;
  Location? _location;
  PackageInfo? _packageInfo;

  PackageInfo? get packageInfo => _packageInfo;
  Status _status = Status.Uninitialized;
  Status get status => _status;
  String? get macAddress => _macAddress;
  String? get globalURL => _globalURL;
  bool? get loadingAuth => _loadingAuth;
  LoginModel? get userData => loginModel;
  TextEditingController apiURLCtrl = new TextEditingController();

  Map _source = {ConnectivityResult.none: false};
  MyConnectivity _connectivity = MyConnectivity.instance;
  Map get source => _source;

  Timer? timer;

  UserProvider.initialize() {
    timer = Timer.periodic(
        Duration(seconds: 10), (Timer t) => checkLocationOnOff());
    checkConnectivity();
    loadGlobalURL();
    getAndroidPackageInfo();
    _checkUserIsLoggedIn();
  }
  checkConnectivity() {
    _connectivity.initialise();
    _connectivity.myStream.listen((source) {
      _source = source;
      notifyListeners();
      String status = "Offline";
      switch (source.keys.toList()[0]) {
        case ConnectivityResult.none:
          Fluttertoast.showToast(
              msg: "You are offline, please On your Mobile Network");
          status = "You are offline, please on your Mobile Network";
          break;
        case ConnectivityResult.wifi:
          status = "You are online, enjoy services";
          break;
      }
    });
  }

  checkLocationOnOff() async {
    print("First Time: " + serviceEnabled.toString());
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    notifyListeners();
    print("After Time: " + serviceEnabled.toString());
    if (this.serviceEnabled == false) {
      Fluttertoast.showToast(
          msg: "Your Location is Off, Please Turn on Location");
    }
  }

  _checkUserIsLoggedIn() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    _status = Status.Uninitialized;
    notifyListeners();
    if (sharedPreferences.getString('userDataTRAMMS') == null) {
      _status = Status.Unauthenticated;
      notifyListeners();
    } else {
      _status = Status.Authenticated;
      notifyListeners();
    }
    if (_status == Status.Authenticated) {
      loadLoginUserData();
      startLocationService();
    }
  }

  loadGlobalURL() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    int rowId = 1;
    Word? word = await helper.queryWord(rowId);
    if (word != null) {
      apiURLCtrl = new TextEditingController(text: word.word.toString());
      _globalURL = "${word.word.toString()}";
      notifyListeners();
    }
  }

  loadLoginUserData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? encodedMap = sharedPreferences.getString('userDataTRAMMS');
    Map<String, dynamic> decodedMap = json.decode(encodedMap!);
    loginModel = new LoginModel(
        loginid: decodedMap['id'],
        username: decodedMap['username'],
        userrole: decodedMap['userrole'],
        designationname: decodedMap['designationname']);
    notifyListeners();
  }

  String? formattedDateCheck = "";
  startLocationService() {
    BackgroundLocation.stopLocationService();
    BackgroundLocation.setAndroidConfiguration(30000);
    BackgroundLocation.startLocationService();
    BackgroundLocation.getLocationUpdates((location) {
      if (_location == null) {
        _location = location;
        notifyListeners();
      }
      if (_location!.latitude != location.latitude &&
          _location!.longitude != location.longitude) {
        DateTime dateTime = DateTime.now();
        updateLocation(location.latitude.toString(),
            location.longitude.toString(), dateTime.toString());
      }
      _location = location;
      notifyListeners();
    });
  }

  enableLoading() {
    _loadingAuth = true;
    notifyListeners();
  }

  launchURL(String openURL) async {
    if (await canLaunch(openURL)) {
      await launch(openURL);
    } else {
      throw 'Could not launch $openURL';
    }
  }

  login(BuildContext context, String loginID, String password, String ip,
      String deviceVersion, Position currentLocation) async {
    if (_globalURL == null || _globalURL == "") {
      Fluttertoast.showToast(msg: "Please Configure URL Connection");
      _loadingAuth = false;
      notifyListeners();
    } else {
      _loadingAuth = true;
      notifyListeners();
      try {
        Random random = new Random();
        Map params = {
          'LoginID': loginID,
          'Password': password,
          'SessionID': random.nextInt(10000).toString(),
          'IP': ip,
          'PageVisted': "Login",
          'DeviceUsed': deviceVersion,
          'Action': "Login",
          'Location':
              "${currentLocation.latitude},${currentLocation.longitude}",
        };
        final response = await http
            .post(Uri.parse(_globalURL! + "/APIs/login_check.php"),
                body: params)
            .timeout(Duration(seconds: 30));

        print(response.statusCode);
        var response1 = jsonDecode(response.body);
        if (response1['Message'] != null &&
            response1['Message'].toString() != "") {
          Fluttertoast.showToast(msg: "${response1['Message'].toString()}");
        } else {
          if (response.statusCode == 200) {
            loginModel = new LoginModel.fromJson(json.decode(response.body));
            notifyListeners();
            SharedPreferences sharedPreferences =
                await SharedPreferences.getInstance();
            sharedPreferences.remove('userDataTRAMMS');
            Map<String, dynamic> userData = {
              "id": loginModel!.loginid.toString(),
              "username": loginModel!.username.toString(),
              "userrole": loginModel!.userrole.toString(),
              "designationname": loginModel!.designationname.toString(),
              "globalURL": _globalURL,
            };
            String encodedMap = json.encode(userData);
            sharedPreferences.setString('userDataTRAMMS', encodedMap);
            getMacAddress();
            startLocationService();
            Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (context, animation, secondaryAnimation) =>
                      HomeScreen(),
                ));
          }
        }
        _loadingAuth = false;
        notifyListeners();
      } catch (e) {
        _loadingAuth = false;
        notifyListeners();
        Fluttertoast.showToast(msg: e.toString());
      }
    }
  }

  forgot(BuildContext context, String loginID, String password, String ip,
      String deviceVersion, Position currentLocation) async {
    if (_globalURL == null || _globalURL == "") {
      Fluttertoast.showToast(msg: "Please Configure URL Connection");
    } else {
      _loadingAuth = true;
      notifyListeners();
      try {
        Map params = {
          'LoginID': loginID,
          'EmailID': password,
          'IP': ip,
          'PageVisted': "Forgot",
          'DeviceUsed': deviceVersion,
          'Action': "Login",
          'Location':
              "${currentLocation.latitude},${currentLocation.longitude}",
        };
        print(params);
        final response = await http
            .post(Uri.parse(_globalURL! + "/APIs/forgotpass.php"), body: params)
            .timeout(Duration(seconds: 30));

        print(response.statusCode);
        var response1 = jsonDecode(response.body);
        if (response1['Message'] != null &&
            response1['Message'].toString() != "") {
          Fluttertoast.showToast(msg: "${response1['Message'].toString()}");
        } else {
          if (response.statusCode == 200) {
            Fluttertoast.showToast(msg: "${response1['Message'].toString()}");
            Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (context, animation, secondaryAnimation) =>
                      LoginPage(),
                ));
          }
        }
        _loadingAuth = false;
        notifyListeners();
      } catch (e) {
        _loadingAuth = false;
        notifyListeners();
        Fluttertoast.showToast(msg: e.toString());
      }
    }
  }

  String ip = "", deviceUsed = "";
  Position? currentPosition;
  getIPAndDeviceVersionAndLocation(BuildContext context) async {
    currentPosition = await this.checkLocation(context);
    final ipv4 = await Ipify.ipv4();
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    deviceUsed =
        "android ${androidInfo.version.release.toString()},${packageInfo!.version.toString()}";
    ip = ipv4;
    notifyListeners();
  }

  logoutApi(BuildContext context, String loginID) async {
    _viewMonthlyInspectionList.clear();
    _viewWeeklyPlanList.clear();
    _viewAllInspectionList.clear();
    _loadingAuth = true;
    notifyListeners();
    this.logOut(context);
    try {
      print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      final uri = Uri.parse('$_globalURL/APIs/logout_user.php')
          .replace(queryParameters: {
        'IP': '$ip',
        'PageVisted': 'logout',
        'DeviceUsed': '$deviceUsed',
        'Action': 'Logout',
        'Location':
            '${currentPosition?.latitude.toString()},${currentPosition?.longitude.toString()}'
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '$loginID', preserveHeaderCase: true);
      HttpClientResponse response = await request.close();

      var resStream = response.transform(Utf8Decoder());
      await for (var data in resStream) {
        var response1 = jsonDecode(data);
        if (response1['Message'] != null &&
            response1['Message'].toString() != "" &&
            response1['Message'].toString() == "Logout Successfully.") {
          Common.showTopFlash("Logout", "${response1['Message']}", context, 2);
        }
      }
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  static bool trustSelfSigned = true;

  static HttpClient getHttpClient() {
    HttpClient httpClient = new HttpClient()
      ..connectionTimeout = const Duration(seconds: 40)
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);

    return httpClient;
  }

  bool _offlineLoading = false;
  bool get offlineLoading => _offlineLoading;
  //TODO
  uploadOfflineStoredData(BuildContext context)
  async {
    _offlineLoading = true;
    notifyListeners();

    bool check = await isInternet();
    if(check == false)
      {
        Common.showTopFlash("No Internet Connection", "Please Check Your Internet Connection", context, 2);
      }
    else
      {
        DatabaseHelper helper = DatabaseHelper.instance;
        List<OfflineDataModel> offlineApiList = await helper.getAllOfflineRecords();
        print(offlineApiList.length);
        offlineApiList.forEach((element) async {
          print(element);
          if(element.apiName.toString() == "weeklyScheduleApi")
            {
              Map map = jsonDecode(element.apiParam.toString());
              print("Map Data:::::" + map['IP']);
              await weeklyScheduleApi(context, userData!.loginid.toString(), map['DeviceUsed'], map['txt_ScheduleFrom'], map['txt_ScheduleTo'], "NA", new File(element.pdfData));
              await helper.deleteRecord("weeklyScheduleApi");
            }
        });
      }
    _offlineLoading = false;
    notifyListeners();
  }
  bool _isErrorInWeeklyScheduleAPI = false;
  //TODO END
  weeklyScheduleApi(
      BuildContext context,
      String loginID,
      String deviceUsed,
      String fromDate,
      String toDate,
      String pdfName,
      File pdfPath) async {
    _loadingAuth = true;
    notifyListeners();
    Map<String,dynamic> uploadWeekScheduleMap = {
      'IP': '$ip',
      'PageVisted': 'weekly_schedule',
      'DeviceUsed': '$deviceUsed',
      'Action': 'Insert',
      'txt_ScheduleFrom': '$fromDate',
      'txt_ScheduleTo': '$toDate',
      'hdn_location':
      '${_location?.latitude.toString()},${_location?.longitude.toString()}',
      'Method': 'FormData',
    };
    print(uploadWeekScheduleMap);
    bool check = await isInternet();
    if(check == false)
      {
        _loadingAuth = false;
        notifyListeners();
        bool _isExist = false;
        DatabaseHelper helper = DatabaseHelper.instance;
        //TODO
        List<OfflineDataModel> offlineApiList = await helper.getAllOfflineRecords();
        offlineApiList.forEach((element) async {
          Map map = jsonDecode(element.apiParam.toString());
          if(map['txt_ScheduleFrom'] == uploadWeekScheduleMap['txt_ScheduleFrom'] && map['txt_ScheduleTo'] == uploadWeekScheduleMap['txt_ScheduleTo'])
            {
              _isExist = true;
              notifyListeners();
            }
        });
        if(_isExist == true)
          {
            Common.showTopFlash("Already Exist", "Data Already Exist to Offline", context, 3);
          }
        else
          {
            OfflineDataModel offlineDataModel = new OfflineDataModel("weeklyScheduleApi",json.encode(uploadWeekScheduleMap),true,pdfPath.path);
            await helper.insertOfflineData(offlineDataModel,context);
          }
        //TODO END
      }
    else
      {
        _isErrorInWeeklyScheduleAPI = false;
        notifyListeners();
        print("Pushed.....................");
        try {
          int byteCount = 0;
          var requestMultipart = http.MultipartRequest(
              "POST", Uri.parse('$_globalURL/APIs/weeklyschedule.php'));

          var multipart =
          await http.MultipartFile.fromPath('txt_UploadPDFPath', pdfPath.path);

          final uri = Uri.parse('$_globalURL/APIs/weeklyschedule.php')
              .replace(queryParameters: uploadWeekScheduleMap);
          requestMultipart.files.add(multipart);
          notifyListeners();
          var msStream = requestMultipart.finalize();

          final client = getHttpClient();
          final request = await client.postUrl(uri);

          var totalByteLength = requestMultipart.contentLength;
          request.contentLength = totalByteLength;

          request.headers.set(HttpHeaders.contentTypeHeader,
              requestMultipart.headers[HttpHeaders.contentTypeHeader]!);
          request.headers.set('LoginID', '$loginID', preserveHeaderCase: true);
          request.headers.set(
              'DesignationName', '${userData!.designationname.toString()}',
              preserveHeaderCase: true);
          // request.contentLength = totalByteLength;

          Stream<List<int>> streamUpload = msStream.transform(
            new StreamTransformer.fromHandlers(
              handleData: (data, sink) {
                sink.add(data);
                byteCount += data.length;
                notifyListeners();
              },
              handleError: (error, stack, sink) {
                throw error;
              },
              handleDone: (sink) {
                sink.close();
                // UPLOAD DONE;
              },
            ),
          );

          await request.addStream(streamUpload);
          notifyListeners();

          HttpClientResponse response = await request.close();

          response
              .transform(utf8.decoder)
              .transform(json.decoder)
              .listen((contents) {
            print(contents);
            Fluttertoast.showToast(
                msg: "${jsonDecode(json.encode(contents))['Message'].toString()}");
            if("${jsonDecode(json.encode(contents))['Message'].toString()}" == "Data Save Successfully.")
              {
                _isErrorInWeeklyScheduleAPI = false;
                notifyListeners();
              }
            else
              {
                _isErrorInWeeklyScheduleAPI = true;
                notifyListeners();
              }
          });
          _loadingAuth = false;
          notifyListeners();
        } catch (e) {
          _loadingAuth = false;
          notifyListeners();
          Fluttertoast.showToast(msg: e.toString());
        }
      }
  }

  List _viewWeeklyPlanList = [];
  List get viewWeeklyPlanList => _viewWeeklyPlanList;

  getWeeklyScheduleApi(
      BuildContext context, String monthYear, String loginID) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri = Uri.parse('$_globalURL/APIs/weeklyschedule.php')
          .replace(queryParameters: {
        'Action': 'Select',
        'txt_S_Month': '$monthYear',
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '$loginID', preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);
      HttpClientResponse response = await request.close();

      response
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((contents) {
        if (jsonDecode(json.encode(contents)).toString().length == 33 &&
            jsonDecode(json.encode(contents))['Message'] ==
                "Weekly Plan not Found!") {
          Fluttertoast.showToast(
              msg:
                  "${jsonDecode(json.encode(contents))['Message'].toString()}");
        }
        _viewWeeklyPlanList.clear();
        _viewWeeklyPlanList = jsonDecode(json.encode(contents));
        notifyListeners();
        print("....................");
        // for(int i=0; i<newList.length;i++)
        //   {
        //     _viewWeeklyPlanList.add(new ViewWeeklyPlanModel.ViewWeeklyPlanModel(username: newList[i]["username"],loginid: newList[i]["loginid"],designationmasterid: newList[i]["designationmasterid"],filename: newList[i]["filename"],path: newList[i]["path"],schduleby: newList[i]["schduleby"],schedulefrom: newList[i]["schedulefrom"],scheduleto: newList[i]["scheduleto"],weeklyplanid: newList[i]["weeklyplanid"]));
        //     notifyListeners();
        //   }
      });
      print(_viewWeeklyPlanList.length);
      // var resStream = response.transform(Utf8Decoder());
      // await for (dynamic data in resStream) {
      // }
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  //TODO Monthly Inspection GET
  String? _selectedInspection;
  String? get selectedInspection => _selectedInspection;
  List _viewAllInspectionList = [];
  List get viewAllInspectionList => _viewAllInspectionList;

  getAllInspectionListApi(BuildContext context) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri = Uri.parse('$_globalURL/APIs/inspection.php')
          .replace(queryParameters: {
        'Action': 'Select',
        'InspectionName': 'ALL',
        'InspectionType': 'PM',
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      print(request.uri.queryParameters);
      HttpClientResponse response = await request.close();

      response
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((contents) {
        print(contents);
        _viewAllInspectionList.clear();
        _viewAllInspectionList = jsonDecode(json.encode(contents));
        notifyListeners();
      });
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  changeInspection(String newValue, BuildContext context, String monthYear) {
    this._selectedInspection = newValue;
    notifyListeners();
  }

  clearPreventData() {
    viewPreventiveMaintenanceData.clear();
  }

  String? _selectedCorridor;
  String? get selectedCorridor => _selectedCorridor;
  List _viewAllCorridorList = [];
  List get viewAllCorridorList => _viewAllCorridorList;

  getAllCorridorListApi(BuildContext context) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri =
          Uri.parse('$_globalURL/APIs/corridor.php').replace(queryParameters: {
        'Action': 'Select',
        'CorridorName': 'ALL',
        'CorridorStatus': '1',
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      print(request.uri.queryParameters);
      HttpClientResponse response = await request.close();

      response
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((contents) {
        print(contents);
        _viewAllCorridorList.clear();
        _viewAllCorridorList = jsonDecode(json.encode(contents));
        notifyListeners();
      });
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  changeCorridor(String newValue, BuildContext context, String monthYear) {
    this._selectedCorridor = newValue;
    notifyListeners();
  }

  String? _selectedBICorridor;
  String? get selectedBICorridor => _selectedBICorridor;
  List _viewBICorridorList = [];
  List get viewBICorridorList => _viewBICorridorList;

  getBICorridorListApi(BuildContext context) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri =
          Uri.parse('$_globalURL/APIs/corridor.php').replace(queryParameters: {
        'Action': 'Select',
        'CorridorName': 'ALL',
        'CorridorStatus': '1',
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      print(request.uri.queryParameters);
      HttpClientResponse response = await request.close();

      response
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((contents) {
        print(contents);
        _viewBICorridorList.clear();
        _viewBICorridorList = jsonDecode(json.encode(contents));
        notifyListeners();
      });
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  changeBICorridor(String newValue, BuildContext context) async {
    this._selectedBICorridor = newValue;
    notifyListeners();
    this._selectedBIStationCode = null;
    this._viewBIStationCodeList = [];
    notifyListeners();
    await getBIStationCodeListApi(context);
  }

  String? _selectedBIStationCode;
  String? get selectedBIStationCode => _selectedBIStationCode;
  List _viewBIStationCodeList = [];
  List get viewBIStationCodeList => _viewBIStationCodeList;

  TextEditingController _chainage = TextEditingController();
  TextEditingController get chainage => _chainage;

  getBIStationCodeListApi(BuildContext context) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri =
          Uri.parse('$_globalURL/APIs/station.php').replace(queryParameters: {
        'Action': 'SelectStation',
        'StationName': '',
        'CorridorName':
            '${_viewAllCorridorList.where((element) => element['corridormasterid'].toString() == _selectedBICorridor.toString()).first['corridorname']}',
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      print(request.uri.queryParameters);
      HttpClientResponse response = await request.close();

      response
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((contents) {
        print(contents);
        _viewBIStationCodeList.clear();
        _viewBIStationCodeList = jsonDecode(json.encode(contents));
        notifyListeners();
      });
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  changeBIStationCode(String newValue) {
    _selectedBIStationCode = null;
    this._selectedBIStationCode = newValue;
    _chainage = new TextEditingController(text: _viewBIStationCodeList.where((element) => element['stationmasterid'] == newValue).first['chainage'].toString());
    notifyListeners();
  }

  String? _selectedVILine;
  String? get selectedVILine => _selectedVILine;
  List _viewBILineList = [];
  List get viewBILineList => _viewBILineList;

  getBILineListApi(BuildContext context) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri =
          Uri.parse('$_globalURL/APIs/line.php').replace(queryParameters: {
        'Action': 'Select',
        'LineName': 'ALL',
        'LineStatus': '1',
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      print(request.uri.queryParameters);
      HttpClientResponse response = await request.close();

      response
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((contents) {
        print(contents);
        _viewBILineList.clear();
        _viewBILineList = jsonDecode(json.encode(contents));
        notifyListeners();
      });
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  changeBILine(String newValue) {
    this._selectedVILine = newValue;
    notifyListeners();
  }

  //TODO Monthly Inspection POST
  DateTime _selectedSearchInspectionMonth = DateTime.now();
  DateTime get selectedSearchInspectionMonth => _selectedSearchInspectionMonth;

  changeInspectionMonthYear(var newValue) {
    this._selectedSearchInspectionMonth = newValue;
    notifyListeners();
  }

  List<MonthlyInspection.MonthlyInspection> _viewMonthlyInspectionList = [];
  List<MonthlyInspection.MonthlyInspection> get viewMonthlyInspectionList =>
      _viewMonthlyInspectionList;

  getMonthlyInspectionListApi(
      BuildContext context, String inspectionMasterID, String monthYear) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri = Uri.parse('$_globalURL/APIs/annualplan.php')
          .replace(queryParameters: {
        'Action': 'Select',
        'InspectionID': _viewAllInspectionList
            .where((element) =>
                element['inspectionmasterid'] == inspectionMasterID)
            .first['inspectionid']
            .toString(),
        'MonthYear': monthYear,
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      HttpClientResponse response = await request.close();

      // response.transform(utf8.decoder).transform(json.decoder).listen((contents) {
      //   print(contents);
      // });

      response.cast<List<int>>().transform(utf8.decoder).listen(((contents) {
        print(contents);

        _viewMonthlyInspectionList.clear();
        Iterable l = json.decode(contents);
        List<MonthlyInspection.MonthlyInspection> posts =
            List<MonthlyInspection.MonthlyInspection>.from(l.map((model) =>
                MonthlyInspection.MonthlyInspection.fromJson(model)));
        _viewMonthlyInspectionList.addAll(posts);
        notifyListeners();
      }));
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  List<PMJobCardTimeLineData.PMJobCardTimeLineData> _viewJobCardTimeLineList =
      [];
  List<PMJobCardTimeLineData.PMJobCardTimeLineData>
      get viewJobCardTimeLineList => _viewJobCardTimeLineList;

  getPMJobCardDataApi(
      BuildContext context, String JobCardID, String Status) async {
    _viewJobCardTimeLineList.clear();
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri = Uri.parse('$_globalURL/APIs/trackinspections.php')
          .replace(queryParameters: {
        'Action': 'Select',
        'JobCardID': "$JobCardID",
        'Status': "$Status",
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      HttpClientResponse response = await request.close();

      // response.transform(utf8.decoder).transform(json.decoder).listen((contents) {
      //   print(contents);
      // });

      response.cast<List<int>>().transform(utf8.decoder).listen(((contents) {
        print(contents);

        Iterable l = json.decode(contents);
        List<PMJobCardTimeLineData.PMJobCardTimeLineData> posts =
            List<PMJobCardTimeLineData.PMJobCardTimeLineData>.from(l.map(
                (model) => PMJobCardTimeLineData.PMJobCardTimeLineData.fromJson(
                    model)));
        _viewJobCardTimeLineList.addAll(posts);
        notifyListeners();
      }));
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  //TODO edit/update monthly inspection
  updateMonthlyInspectionApi(
      {required BuildContext context,
      required String hdn_AnnualPlanID,
      required String hdn_Type,
      required String hdn_InspOpen,
      required String txt_InspNotOpen,
      required String hdn_CorridorName,
      required String hdn_InspCode,
      required String Value,
      required String IP,
      required String location,
      required String deviceUsed}) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri = Uri.parse('$_globalURL/APIs/annualplan.php')
          .replace(queryParameters: {
        'Action': "Update",
        'hdn_AnnualPlanID': "$hdn_AnnualPlanID",
        'hdn_Type': "$hdn_Type",
        'hdn_InspOpen': "$hdn_InspOpen",
        'txt_InspNotOpen': "$txt_InspNotOpen",
        'hdn_CorridorName': "$hdn_CorridorName",
        'hdn_InspCode': "$hdn_InspCode",
        'Value': "$Value",
        'IP': "$IP",
        'DeviceUsed': "$deviceUsed",
        'PageVisted': "monthly_inspection",
        'hdn_location': "$location",
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      HttpClientResponse response = await request.close();

      // response.transform(utf8.decoder).transform(json.decoder).listen((contents) {
      //   print(contents);
      // });

      response.cast<List<int>>().transform(utf8.decoder).listen(((contents) {
        print(contents);
        var response = json.decode(contents);
        Fluttertoast.showToast(msg: "${response['Message']}");
        if (selectedSearchInspectionMonth.month.toInt() <= 9) {
          getMonthlyInspectionListApi(context, "${selectedInspection}",
              "${selectedSearchInspectionMonth.year}-0${selectedSearchInspectionMonth.month}");
        } else {
          getMonthlyInspectionListApi(context, "${selectedInspection}",
              "${selectedSearchInspectionMonth.year}-${selectedSearchInspectionMonth.month}");
        }
      }));
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  updatePMStatusApi(
      {required BuildContext context,
      required String hdn_JobCardID,
      required String hdn_PreventiveMasterID,
      required String txt_Remarks,
      required String IP,
      required String DeviceUsed,
      required String hdn_location}) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri = Uri.parse('$_globalURL/APIs/preventivemaintenance.php')
          .replace(queryParameters: {
        'Action': "Update",
        'hdn_Status': "inprogress",
        'hdn_JobCardID': "$hdn_JobCardID",
        'hdn_PreventiveMasterID': "$hdn_PreventiveMasterID",
        'txt_Remarks': "$txt_Remarks",
        'IP': "$IP",
        'DeviceUsed': "$DeviceUsed",
        'PageVisted': "preventive_maintenance",
        'hdn_location': "$hdn_location",
        'Method': "FormData",
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      HttpClientResponse response = await request.close();

      // response.transform(utf8.decoder).transform(json.decoder).listen((contents) {
      //   print(contents);
      // });

      response.cast<List<int>>().transform(utf8.decoder).listen(((contents) {
        print(contents);
      }));
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  //TODO edit inspection ctrl
  TextEditingController _inspectionsCtrl = new TextEditingController();
  TextEditingController get inspectionsCtrl => _inspectionsCtrl;
  List<String> _blueData = [];
  List<String> _greenData = [];
  List get blueData => _blueData;
  List get greenData => _greenData;

  changeInspectionCtrlData(String newData) {
    _inspectionsCtrl = new TextEditingController(text: newData);
    notifyListeners();
  }

  changeBlueData(newBlueData) {
    _blueData.add(newBlueData.trim().replaceAll(' ', '').toString());
    notifyListeners();
    print(_blueData);
  }

  changeGreenData(newGreenData) {
    _greenData.add(newGreenData.trim().replaceAll(' ', '').toString());
    notifyListeners();
  }

  clearBlueGreenData() {
    _blueData = [];
    _greenData = [];
    notifyListeners();
  }

  //TODO Allocate Staff
  DateTime selectedPickedNewDate = DateTime.now();

  TextEditingController _inspectionPlanedDateCtrl = new TextEditingController(
      text: DateTime.now().month <= 9
          ? "${DateTime.now().day <= 9 ? "0${DateTime.now().day}" : "${DateTime.now().day}"}-0${DateTime.now().month}-${DateTime.now().year}"
          : "${DateTime.now().day}-${DateTime.now().month}-${DateTime.now().year}");
  TextEditingController _remarkAllocateUser = new TextEditingController();
  TextEditingController get inspectionPlanedDateCtrl =>
      _inspectionPlanedDateCtrl;
  TextEditingController get remarkAllocateUser => _remarkAllocateUser;

  Future<void> pickNewDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedPickedNewDate,
        builder: (BuildContext? context, Widget? child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme:
                  ColorScheme.light(primary: CustomColors.kPrimaryColor),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child!,
          );
        },
        firstDate: DateTime.now(),
        lastDate: DateTime(2500));
    if (picked != null && picked != selectedPickedNewDate)
      selectedPickedNewDate = picked;
    notifyListeners();
    this._inspectionPlanedDateCtrl = new TextEditingController(
        text: selectedPickedNewDate.month <= 9
            ? "${selectedPickedNewDate.day <= 9 ? "0${selectedPickedNewDate.day}" : selectedPickedNewDate.day}-0${selectedPickedNewDate.month}-${selectedPickedNewDate.year}"
            : "${selectedPickedNewDate.day}-0${selectedPickedNewDate.month}-${selectedPickedNewDate.year}");
  }

  List<AllocateStaffUsers.AllocateStaffUsers> _viewAllocateStaffUsersList = [];
  List<AllocateStaffUsers.AllocateStaffUsers> get viewAllocateStaffUsersList =>
      _viewAllocateStaffUsersList;

  String? _selectedAllocateUser;
  String? get selectedAllocateUser => _selectedAllocateUser;

  getAllocateStaffUsersListApi(BuildContext context) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri =
          Uri.parse('$_globalURL/APIs/user.php').replace(queryParameters: {
        'Action': 'Select',
        'LoginIDMobile': '',
        'UserRole': '${userData?.userrole.toString()}',
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      HttpClientResponse response = await request.close();

      // response.transform(utf8.decoder).transform(json.decoder).listen((contents) {
      //   print(contents);
      // });

      response.cast<List<int>>().transform(utf8.decoder).listen(((contents) {
        print(contents);

        _viewAllocateStaffUsersList.clear();
        Iterable l = json.decode(contents);
        List<AllocateStaffUsers.AllocateStaffUsers> posts =
            List<AllocateStaffUsers.AllocateStaffUsers>.from(l.map((model) =>
                AllocateStaffUsers.AllocateStaffUsers.fromJson(model)));
        _viewAllocateStaffUsersList.addAll(posts);
        notifyListeners();
      }));
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  changeAllocateStaffUserMI(String newValue) {
    _selectedAllocateUser = newValue;
    notifyListeners();
  }

  allocateStaffApi(
      {required BuildContext context,
      required String hdn_AnnualPlanID,
      required String hdn_Type,
      required String hdn_CorridorName,
      required String hdn_InspCode,
      required String Value,
      required String IP,
      required String location,
      required String deviceUsed,
      required String hdnMonthYear,
      required String hdnLevel}) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      var dateParsed =
          convertDateTimePtBR("${_inspectionPlanedDateCtrl.text.toString()}");

      final uri = Uri.parse('$_globalURL/APIs/preventivemaintenance.php')
          .replace(queryParameters: {
        'Action': "Insert",
        'hdn_AnnualPlanID': "$hdn_AnnualPlanID",
        'hdn_Type': "$hdn_Type",
        'hdn_InspCode': "$hdn_InspCode",
        'hdn_Value': "$Value",
        'hdn_CorridorName': "$hdn_CorridorName",
        'txt_DateInspectionPlanned': "${dateParsed.toString().split(' ')[0]}",
        'ddl_EmpID':
            "$_selectedAllocateUser^${_viewAllocateStaffUsersList.where((element) => element.employeeid.toString().trim() == _selectedAllocateUser.toString().trim()).first.designationname}",
        'txt_Remarks': "${_remarkAllocateUser.text.toString()}",
        'hdn_MonthYear': "$hdnMonthYear",
        'hdn_LevelOfInspection': "$hdnLevel",
        'IP': "$IP",
        'DeviceUsed': "$deviceUsed",
        'PageVisted': "monthly_inspection",
        'hdn_location': "$location",
        'Method': 'FormData',
      });
      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);

      HttpClientResponse response = await request.close();

      // response.transform(utf8.decoder).transform(json.decoder).listen((contents) {
      //   print(contents);
      // });

      response.cast<List<int>>().transform(utf8.decoder).listen(((contents) {
        print(contents);
        var response = json.decode(contents);
        Common.showTopFlash("JobCardID: ${response['JobCardID']}",
            "${response['Message']}", context, 4);
        if (selectedSearchInspectionMonth.month.toInt() <= 9) {
          getMonthlyInspectionListApi(context, "${selectedInspection}",
              "${selectedSearchInspectionMonth.year}-0${selectedSearchInspectionMonth.month}");
        } else {
          getMonthlyInspectionListApi(context, "${selectedInspection}",
              "${selectedSearchInspectionMonth.year}-${selectedSearchInspectionMonth.month}");
        }
      }));
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  DateTime convertDateTimePtBR(String validade) {
    DateTime parsedDate = DateTime.parse('0001-11-30');

    List<String> validadeSplit = validade.split('-');

    if (validadeSplit.length > 1) {
      String day = validadeSplit[0].toString();
      String month = validadeSplit[1].toString();
      String year = validadeSplit[2].toString();

      parsedDate = DateTime.parse('$year-$month-$day');
    }

    return parsedDate;
  }

  List<String> _preventItemsList = [
    'ALL',
    'Open',
    'InProgress',
    'Completed',
    'Closed',
  ];
  List<String> get preventItemsList => _preventItemsList;
  String? _selectedPreventItem;
  String? get selectedPreventItem => _selectedPreventItem;

  changePreventiveItems(String newValue) {
    _selectedPreventItem = newValue;
    notifyListeners();
  }

  List<String> _preventLevelItemsList = [
    'Er',
    'EE',
    'SEE',
    'EE/Er',
    'SEE/EE/Er',
  ];
  List<String> get preventLevelItemsList => _preventLevelItemsList;
  String? _selectedPreventLevelItem;
  String? get selectedPreventLevelItem => _selectedPreventLevelItem;

  changePreventiveLevelItems(String newValue) {
    _selectedPreventLevelItem = newValue;
    notifyListeners();
  }

  List<String> _preventTypeItemsList = [
    'Under Ground',
    'Elevated',
  ];
  List<String> get preventTypeItemsList => _preventTypeItemsList;
  String? _selectedPreventTypeItem;
  String? get selectedPreventTypeItem => _selectedPreventTypeItem;

  changePreventiveTypeItems(String newValue) {
    _selectedPreventTypeItem = newValue;
    notifyListeners();
  }

  List<String> _changeStatusList = [
    'Yes',
    'No',
  ];
  List<String> get changeStatusList => _changeStatusList;
  String? _selectedStatusItem;
  String? get selectedStatusItem => _selectedStatusItem;

  changeStatusItems(String newValue) {
    _selectedStatusItem = newValue;
    notifyListeners();
  }

  List<String> _changeDefectsList = ['Nil', 'Feasible', 'Not Feasible'];
  List<String> get changeDefectsList => _changeDefectsList;
  String? _selectedDefectsItem;
  String? get selectedDefectsItem => _selectedDefectsItem;

  changeDefectsItems(String newValue) {
    _selectedDefectsItem = newValue;
    notifyListeners();
  }

  List<String> _changeTypeBufferstopList = [
    'ALL',
    'Friction 14Zeb/5',
    'Friction 10Zeb/2',
    'Fixed',
    'Wheelstop',
  ];
  List<String> get changeTypeBufferstopList => _changeTypeBufferstopList;
  String? _selectedTypeBufferstopItem;
  String? get selectedTypeBufferstopItem => _selectedTypeBufferstopItem;

  changeTypeBufferstopItems(String newValue) {
    _selectedTypeBufferstopItem = newValue;
    notifyListeners();
  }

  List<String> _changeRHList1 = [
    'Good',
    'Yes',
    'Not ok',
    'No',
  ];

  List<String> _changeLHList1 = [
    'Good',
    'Yes',
    'Not ok',
    'No',
  ];

  List<String> _selectedRHValueList = [
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
  ];

  List<String> _selectedLHValueList = [
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
    "Select Status",
  ];

  List<String> get selectedRHValueList => _selectedRHValueList;
  List<String> _changeRHList2 = [
    'Select Status',
    'Good',
    'Yes',
    'Not ok',
    'No'
  ];
  List<String> get changeRHList2 => _changeRHList2;

  List<String> get selectedLHValueList => _selectedLHValueList;
  List<String> _changeLHList2 = [
    'Select Status',
    'Good',
    'Yes',
    'Not ok',
    'No'
  ];
  List<String> get changeLHList2 => _changeLHList2;

  List<TextEditingController> _selectedRHCTRLList = [
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
  ];

  List<TextEditingController> _selectedLHCTRLList = [
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
    new TextEditingController(),
  ];

  List<TextEditingController> get selectedRHCTRLList => _selectedRHCTRLList;
  List<String> get changeRHList1 => _changeRHList1;
  String? _selectedRHItem1;
  String? get selectedRHItem1 => _selectedRHItem1;

  List<TextEditingController> get selectedLHCTRLList => _selectedLHCTRLList;
  List<String> get changeLHList1 => _changeLHList1;
  String? _selectedLHItem1;
  String? get selectedLHItem1 => _selectedLHItem1;

  changeRHItems1(String newValue, int index) {
    _selectedRHValueList[index] = newValue;
    notifyListeners();
  }

  changeLHItems1(String newValue, int index) {
    _selectedLHValueList[index] = newValue;
    notifyListeners();
  }

  List<List<Asset>> _allFilesList = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
  List<List<Asset>> get allFilesList => _allFilesList;

  clearCheckListImagePickerList() {
    _allFilesList.clear();
    notifyListeners();
  }

  Future<void> loadAssets(int index) async {
    List<Asset> resultList = <Asset>[];
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 5,
        enableCamera: true,
        cupertinoOptions: CupertinoOptions(
          takePhotoIcon: "chat",
          doneButtonTitle: "Fatto",
        ),
      );
    } on Exception catch (e) {}

    _allFilesList.insert(index, resultList);
    notifyListeners();
  }

  List<PMJobDataModel.PMJobDataModel> _viewPreventiveMaintenanceData = [];
  List<PMJobDataModel.PMJobDataModel> get viewPreventiveMaintenanceData =>
      _viewPreventiveMaintenanceData;

  getPreventiveMaintenanceData(BuildContext context, String monthYearPrev,
      String jobCardIDPrevent) async {
    _loadingAuth = true;
    notifyListeners();
    try {
      final uri = Uri.parse('$_globalURL/APIs/preventivemaintenance.php')
          .replace(queryParameters: {
        'Action': 'Select',
        'txt_S_MonthYear': '$monthYearPrev',
        'txt_S_JobCardID':
            selectedPreventItem?.toLowerCase() == "all" ? '' : jobCardIDPrevent,
        'ddl_S_PreventiveStatus': selectedPreventItem?.toLowerCase() == "all"
            ? "ALL"
            : selectedPreventItem?.toLowerCase(),
        'Method': 'FormData',
      });

      final client = HttpClient();
      final request = await client.postUrl(uri);
      request.headers.set('LoginID', '${userData!.loginid.toString()}',
          preserveHeaderCase: true);
      request.headers.set(
          'DesignationName', '${userData!.designationname.toString()}',
          preserveHeaderCase: true);
      HttpClientResponse response = await request.close();

      // response.cast<List<int>>().transform(utf8.decoder).listen(((contents) {
      // }));
      response
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((contents) {
        print(contents);
        _viewPreventiveMaintenanceData.clear();
        Iterable l = json.decode(json.encode(contents));
        List<PMJobDataModel.PMJobDataModel> posts =
            List<PMJobDataModel.PMJobDataModel>.from(l
                .map((model) => PMJobDataModel.PMJobDataModel.fromJson(model)));
        _viewPreventiveMaintenanceData.addAll(posts);

        // // Iterable l = json.decode(contents);
        // _viewPreventiveMaintenanceData.addAll(json.decode(json.encode(contents)));
        notifyListeners();
      });
      _loadingAuth = false;
      notifyListeners();
    } catch (e) {
      _loadingAuth = false;
      notifyListeners();
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  Future<bool> isInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network, make sure there is actually a net connection.
      if (await InternetConnectionChecker().hasConnection) {
        // Mobile data detected & internet connection confirmed.
        return true;
      } else {
        // Mobile data detected but no internet connection found.
        return false;
      }
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a WIFI network, make sure there is actually a net connection.
      if (await InternetConnectionChecker().hasConnection) {
        // Wifi detected & internet connection confirmed.
        return true;
      } else {
        // Wifi detected but no internet connection found.
        return false;
      }
    } else {
      // Neither mobile data or WIFI detected, not internet connection found.
      return false;
    }
  }

  String name = "";
  uploadPMDataAPI(BuildContext context) async {
    _loadingAuth = true;
    notifyListeners();
    bool check = await isInternet();
    if (check == false) {
      print("Offline.....................................");
      // Common.showTopFlash("Offline Storing...", "Your Data is Stored to Offline", context, 2);
      Map map = {
        "test":"1"
      };
      OfflineDataModel offlineDataModel = new OfflineDataModel("uploadPMDataAPI",jsonEncode(map),false,"NA");
      DatabaseHelper helper = DatabaseHelper.instance;
      await helper.insertOfflineData(offlineDataModel,context);
    } else {
      print("Online.....................................");

      try {
        _loadingAuth = false;
        notifyListeners();
      } on SocketException catch (_) {
        print('not connected');
        Fluttertoast.showToast(msg: "No Internet Connection");
      } catch (e) {
        _loadingAuth = false;
        notifyListeners();
      }
    }
  }

  //TODO End previous
  Future<String?> getMacAddress() async {
    try {
      _macAddress = await GetMac.macAddress;
      notifyListeners();
    } on PlatformException {
      _macAddress = '';
      notifyListeners();
    }
    return _macAddress;
  }

  updateLocation(String lat, String lon, String datetime) async {
    try {
      if (_macAddress == null || _macAddress!.isEmpty) {
        await getMacAddress();
      }
      Map params = {
        'DefaultKey':
            "a8c4e7d8fdf1af5bc110eccc5b995c57569a1ed1ae989157eb7a719b487477b4",
        'device_id':
            _macAddress == null ? "" : _macAddress!.replaceAll(RegExp(':'), ''),
        'device_latitude': lat,
        'device_longitude': lon,
        'lastupdate_datetime': datetime,
        'Method': 'FormData',
      };
      print(params);
      final response = await http
          .post(Uri.parse(_globalURL! + "/APIs/GIS/getdevicelog.php"),
              body: params)
          .timeout(Duration(seconds: 30));
      print(response.body);

      var response1 = jsonDecode(response.body);
      if (response1['Message'] != null &&
          response1['Message'].toString() != "") {
        // Fluttertoast.showToast(msg: "${response1['Message'].toString()}");
      } else {
        if (response.statusCode == 200) {
          print(response1['Message']);
        }
      }
    } catch (e) {}
  }

  Future logOut(BuildContext context) async {
    BackgroundLocation.stopLocationService();
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove("userDataTRAMMS");
    sharedPreferences.clear();
    _status = Status.Unauthenticated;
    _viewWeeklyPlanList.clear();
    this._globalURL = "";
    this._macAddress = "";
    this.loginModel = LoginModel();
    notifyListeners();
  }

  checkLocation(BuildContext context) async {
    bool? serviceEnabled = true;
    LocationPermission? permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      Fluttertoast.showToast(msg: "Please enable your location");
    }

    permission = await Geolocator.checkPermission();
    notifyListeners();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      notifyListeners();
      if (permission == LocationPermission.denied) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Row(
                  children: [
                    Icon(Icons.network_check),
                    Text("  Can't Get Current location"),
                  ],
                ),
                content: const Text(
                    'Location Service are denied. \n\nPlease make sure you enable Location and try again'),
                actions: <Widget>[
                  FlatButton(
                      child: Text('Ok'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              );
            });
      }
    }

    if (permission == LocationPermission.deniedForever) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Row(
                children: [
                  Icon(Icons.network_check),
                  Text("  Can't Get Current location"),
                ],
              ),
              content: const Text(
                  'Location Service are Permanently denied. \n\nPlease make sure you enable Location and try again'),
              actions: <Widget>[
                FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    })
              ],
            );
          });
    }
    return await Geolocator.getCurrentPosition();
  }

  getAndroidPackageInfo() async {
    _packageInfo = await PackageInfo.fromPlatform();
    notifyListeners();
  }

  bool serviceEnabled = true;
  LocationPermission? permission;
  bool get checkServiceEnabled => serviceEnabled;

  Future<Position?> checkLocationPermission() async {
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    notifyListeners();
    if (!serviceEnabled) {}

    permission = await Geolocator.checkPermission();
    notifyListeners();
    if (permission == LocationPermission.denied) {
      notifyListeners();
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {}
    }

    if (permission == LocationPermission.deniedForever) {}
  }

  Future gpsService(BuildContext context) async {
    if (!(await Geolocator.isLocationServiceEnabled())) {
      checkLocationPermission();
      return null;
    } else
      return true;
  }
}

class Inspection {
  String? inspectionmasterid;
  String? inspectionid;
  String? inspectionname;
  String? inspectiontype;
  String? isactive;

  Inspection(
      {this.inspectionmasterid,
      this.inspectionid,
      this.inspectionname,
      this.inspectiontype,
      this.isactive});

  Inspection.fromJson(Map<String, dynamic> json) {
    inspectionmasterid = json['inspectionmasterid'];
    inspectionid = json['inspectionid'];
    inspectionname = json['inspectionname'];
    inspectiontype = json['inspectiontype'];
    isactive = json['isactive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['inspectionmasterid'] = this.inspectionmasterid;
    data['inspectionid'] = this.inspectionid;
    data['inspectionname'] = this.inspectionname;
    data['inspectiontype'] = this.inspectiontype;
    data['isactive'] = this.isactive;
    return data;
  }
}

class Corridor {
  String? corridormasterid;
  String? corridorname;
  String? isactive;

  Corridor({this.corridormasterid, this.corridorname, this.isactive});

  Corridor.fromJson(Map<String, dynamic> json) {
    corridormasterid = json['corridormasterid'];
    corridorname = json['corridorname'];
    isactive = json['isactive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['corridormasterid'] = this.corridormasterid;
    data['corridorname'] = this.corridorname;
    data['isactive'] = this.isactive;
    return data;
  }
}

class BICorridor {
  String? corridormasterid;
  String? corridorname;
  String? isactive;

  BICorridor({this.corridormasterid, this.corridorname, this.isactive});

  BICorridor.fromJson(Map<String, dynamic> json) {
    corridormasterid = json['corridormasterid'];
    corridorname = json['corridorname'];
    isactive = json['isactive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['corridormasterid'] = this.corridormasterid;
    data['corridorname'] = this.corridorname;
    data['isactive'] = this.isactive;
    return data;
  }
}

class BIStationCodeList {
  String? stationmasterid;
  String? stationname;
  String? stationcode;
  String? chainage;
  String? corridormasterid;
  String? isactive;
  String? corridorname;

  BIStationCodeList(
      {this.stationmasterid,
      this.stationname,
      this.stationcode,
      this.chainage,
      this.corridormasterid,
      this.isactive,
      this.corridorname});

  BIStationCodeList.fromJson(Map<String, dynamic> json) {
    stationmasterid = json['stationmasterid'];
    stationname = json['stationname'];
    stationcode = json['stationcode'];
    chainage = json['chainage'];
    corridormasterid = json['corridormasterid'];
    isactive = json['isactive'];
    corridorname = json['corridorname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['stationmasterid'] = this.stationmasterid;
    data['stationname'] = this.stationname;
    data['stationcode'] = this.stationcode;
    data['chainage'] = this.chainage;
    data['corridormasterid'] = this.corridormasterid;
    data['isactive'] = this.isactive;
    data['corridorname'] = this.corridorname;
    return data;
  }
}

class BILineList {
  String? linemasterid;
  String? linename;
  String? isactive;

  BILineList({this.linemasterid, this.linename, this.isactive});

  BILineList.fromJson(Map<String, dynamic> json) {
    linemasterid = json['linemasterid'];
    linename = json['linename'];
    isactive = json['isactive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['linemasterid'] = this.linemasterid;
    data['linename'] = this.linename;
    data['isactive'] = this.isactive;
    return data;
  }
}

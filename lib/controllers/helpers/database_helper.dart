import 'dart:convert';
import 'dart:io';
import 'package:TRAMMS/components/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

final String tableWords = 'words';
final String columnId = '_id';
final String columnWord = 'globalURLTable';
final String offlineDataTable = 'offlineDataTable';
final String columnFrequency = 'frequency';

class DatabaseHelper {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "TRAMMS.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 5;

  // Make this a singleton class.
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // Only allow a single open connection to the database.
  static Database? _database;
  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }
  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableWords (
                $columnId INTEGER PRIMARY KEY,
                $columnWord TEXT NOT NULL,
                $columnFrequency INTEGER NOT NULL
              )
              ''');
    await db.execute('''
              CREATE TABLE offlineDataTable (
                $columnId INTEGER PRIMARY KEY,
                apiName TEXT NOT NULL,
                apiParam TEXT NOT NULL,
                isPDF BOOL NOT NULL,
                pdfData TEXT NOT NULL
              )
              ''');
  }

  Future<int> insert(Word word) async {
    Database? db = await database;

    int? id = 1;
    List<Map<String, dynamic>> maps = await db!.query(tableWords,
        columns: [columnId, columnWord, columnFrequency],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      id = await db.update(tableWords, word.toMap(), where: '$columnId = 1');
    } else {
      id = await db.insert(tableWords, word.toMap());
    }
    return id;
  }

  //TODO Insert Offline Data
  // insertOfflineData(OfflineDataModel offlineDataModel,BuildContext context) async {
  //   Database? db = await database;
  //
  //   List result = await db!.rawQuery('SELECT * FROM $offlineDataTable WHERE apiName=?', ['${offlineDataModel.apiName}']);
  //
  //   if (result.length > 0) {
  //     print("Found...................................");
  //     Common.showTopFlash("Offline Data Already Found", "Please Upload Your Existing Offline Data", context, 3);
  //     // await db.update(offlineDataTable,offlineDataModel.toMap(),where: 'apiName = ?',whereArgs: ['apiName']);
  //   } else {
  //     print("Not Found...................................");
  //     await db.insert(offlineDataTable, offlineDataModel.toMap(),);
  //     Common.showTopFlash("No Internet Connection", "Your Data is Stored to Offline", context, 2);
  //   }
  //
  // }
  //TODO insert data
  insertOfflineData(OfflineDataModel offlineDataModel,BuildContext context) async {
    Database? db = await database;
    await db?.insert(offlineDataTable, offlineDataModel.toMap(),);
    Common.showTopFlash("No Internet Connection", "Your Data is Stored to Offline", context, 2);
  }

  //TODO Get ALl Offline Stored Data
  Future<List<OfflineDataModel>> getAllOfflineRecords() async {
    Database? db = await database;
    final List<Map<String, dynamic>> maps = await db!.query('$offlineDataTable');
    return List.generate(maps.length, (i) {
      return OfflineDataModel(
        maps[i]['apiName'],
        maps[i]['apiParam'],
        maps[i]['isPDF'],
        maps[i]['pdfData'],
      );
    });
  }

   deleteRecord(String apiName) async {
    print(":::::::::::::::::::::::");
    Database? db = await database;
    await db!.delete(offlineDataTable, where: 'apiName = ?', whereArgs: [apiName]);
  }

  Future<Word?> queryWord(int id) async {
    Database? db = await database;
    List<Map<String, dynamic>> maps = await db!.query(tableWords,
        columns: [columnId, columnWord, columnFrequency],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return Word.fromMap(maps.first);
    }
    return null;
  }

}

class Word {
  int? id;
  String? word;
  int? frequency;

  Word();

  // convenience constructor to create a Word object
  Word.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    word = map[columnWord];
    frequency = map[columnFrequency];
  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{columnWord: word, columnFrequency: frequency};
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

class OfflineDataModel {
  dynamic apiName;
  Object? apiParam;
  dynamic isPDF;
  dynamic pdfData;

  OfflineDataModel(this.apiName, this.apiParam,this.isPDF,this.pdfData);

  // convenience constructor to create a Word object
  OfflineDataModel.fromMap(Map<String, dynamic> map) {
    apiName = map['apiName'].toString();
    apiParam = jsonDecode(jsonEncode(map['apiParam']));
    isPDF = map['isPDF'].toString();
    pdfData = map['pdfData'].toString();
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{"apiName": apiName, "apiParam": apiParam,"isPDF": isPDF,"pdfData": pdfData};
    return map;
  }
}

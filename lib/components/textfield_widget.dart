import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final IconData iconData;
  const TextFieldWidget({
    required this.controller,
    required this.hint,
    required this.iconData
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: TextFormField(
        obscureText: false,
        style: TextStyle(
          color: Colors.black,
        ),
        enabled: false,
        controller: controller,
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(01)
            ),
            hintText: hint,
            labelText: hint,
            prefixIcon: Icon(iconData),
            labelStyle: TextStyle(
                fontSize: 15
            )
        ),
      ),
    );
  }
}

class TextFieldWidgetEnabled extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final IconData iconData;
  const TextFieldWidgetEnabled({
    required this.controller,
    required this.hint,
    required this.iconData
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: TextFormField(
        obscureText: false,
        style: TextStyle(
          color: Colors.black,
        ),
        enabled: true,
        controller: controller,
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(01)
            ),
            hintText: hint,
            labelText: hint,
            prefixIcon: Icon(iconData),
            labelStyle: TextStyle(
                fontSize: 15
            )
        ),
      ),
    );
  }
}

class TextFieldWidgetEnabledReadOnly extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final bool readOnly;
  final IconData iconData;
  const TextFieldWidgetEnabledReadOnly({
    required this.controller,
    required this.hint,
    required this.iconData,
    required this.readOnly,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: TextFormField(
        readOnly: readOnly,
        obscureText: false,
        style: TextStyle(
          color: Colors.black,
        ),
        enabled: true,
        controller: controller,
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(01)
            ),
            hintText: hint,
            labelText: hint,
            prefixIcon: Icon(iconData),
            labelStyle: TextStyle(
                fontSize: 15
            )
        ),
      ),
    );
  }
}

class TextFieldAreaWidget extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final IconData iconData;
  const TextFieldAreaWidget({
    required this.controller,
    required this.hint,
    required this.iconData
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: TextFormField(
        obscureText: false,
        style: TextStyle(
          color: Colors.black,
        ),
        enabled: true,
        maxLines: null,
        keyboardType: TextInputType.multiline,
        controller: controller,
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(01)
            ),
            hintText: hint,
            labelText: hint,
            prefixIcon: Icon(iconData),
            labelStyle: TextStyle(
                fontSize: 15
            )
        ),
      ),
    );
  }
}
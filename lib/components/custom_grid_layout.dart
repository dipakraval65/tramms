import 'package:TRAMMS/components/loading_widget.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:staggered_grid_view_flutter/widgets/staggered_grid_view.dart';
import 'package:staggered_grid_view_flutter/widgets/staggered_tile.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import '../views/menus/preventive_maintenance/preventive_maintenance_sub/components/preventive_job_card_details.dart';
import 'custom_dialog_box.dart';

class DropdownWidget extends StatefulWidget {
  final String title;
  final List<String> items;
  final ValueChanged<String> itemCallBack;
  final String currentItem;
  final String hintText;

  DropdownWidget({
    required this.title,
    required this.items,
    required this.itemCallBack,
    required this.currentItem,
    required this.hintText,
  });

  @override
  State<StatefulWidget> createState() => _DropdownState(currentItem);
}

class _DropdownState extends State<DropdownWidget> {
  List<DropdownMenuItem<String>> dropDownItems = [];
  String currentItem;

  _DropdownState(this.currentItem);

  @override
  void initState() {
    super.initState();
    for (String item in widget.items) {
      dropDownItems.add(DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: TextStyle(
            fontSize: 16,
          ),
        ),
      ));
    }
  }

  @override
  void didUpdateWidget(DropdownWidget oldWidget) {
    if (this.currentItem != widget.currentItem) {
      setState(() {
        this.currentItem = widget.currentItem;
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 63,
      color: Colors.white,
      child: FormField<String>(builder: (FormFieldState<String> state) {
        return InputDecorator(
          decoration: InputDecoration(
            labelText: "Select Status",
            labelStyle:
            TextStyle(fontWeight: FontWeight.bold, color: Colors.black38),
            errorStyle: TextStyle(color: Colors.redAccent, fontSize: 14.0),
            hintText: 'Select Status',
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              value: currentItem,
              isExpanded: true,
              items: dropDownItems,
              onChanged: (String? selectedItem) => setState(() {
                currentItem = selectedItem!;
                widget.itemCallBack(currentItem);
              }),
              hint: Container(
                child: Text(widget.hintText),
              ),
            ),
          ),
        );
      }),
    );
  }
}

class CustomGridView extends StatelessWidget {
  const CustomGridView({
    required this.userProvider,
    required this.ratio,
    required this.pdfViewerController,
  });

  final UserProvider userProvider;
  final ratio;
  final PdfViewerController pdfViewerController;

  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.countBuilder(
      primary: false,
      crossAxisCount: 12,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: userProvider.viewWeeklyPlanList.length,
      itemBuilder: (BuildContext context, int index) => Padding(
        padding: EdgeInsets.symmetric(horizontal: 05),
        child: Container(
          decoration: BoxDecoration(
              color: kPrimaryColor,
              borderRadius: BorderRadius.circular(4),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black26, offset: Offset(0, 2), blurRadius: 6)
              ]),
          margin: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 07, vertical: 03),
                  child: Text(
                    "(${index + 1}) Uploaded By: ${userProvider.viewWeeklyPlanList[index]["username"]}",
                    style: TextStyle(fontSize: 17),
                  ),
                ),
                Divider(),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 07, vertical: 01),
                  child: Text(
                      "Designation: ${userProvider.viewWeeklyPlanList[index]["designationmasterid"]}"),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 07, vertical: 01),
                  child: Text(
                      "Schedule From: ${userProvider.viewWeeklyPlanList[index]["schedulefrom"]}"),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 07, vertical: 01),
                  child: Text(
                      "Schedule To: ${userProvider.viewWeeklyPlanList[index]["scheduleto"]}"),
                ),
                InkWell(
                  onTap: () {
                    print(
                        "${userProvider.globalURL}/uploads/Weekly Plan/${userProvider.viewWeeklyPlanList[index]["filename"]}");

                    userProvider.launchURL(
                        "${userProvider.globalURL}/uploads/Weekly Plan/${userProvider.viewWeeklyPlanList[index]["filename"]}");
                  },
                  child: Container(
                    height: 40,
                    color: kPrimaryColor,
                    child: Center(
                      child: Text(
                        "View PDF",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      staggeredTileBuilder: (index) => StaggeredTile.fit(ratio),
    );
  }
}

class CustomGridViewMonthlyInspection extends StatelessWidget {
  const CustomGridViewMonthlyInspection({
    required this.userProvider,
    required this.ratio,
  });

  final UserProvider userProvider;
  final ratio;

  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.countBuilder(
      primary: false,
      crossAxisCount: 12,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: userProvider.viewMonthlyInspectionList.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 05),
          child: Container(
            decoration: BoxDecoration(
                color: kPrimaryColor,
                borderRadius: BorderRadius.circular(4),
                boxShadow: const [
                  BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0, 2),
                      blurRadius: 6)
                ]),
            margin: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
            child: Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 07, vertical: 03),
                    child: Text(
                      "(${index + 1}) ${userProvider.viewMonthlyInspectionList[index].inspectionname}(${userProvider.viewMonthlyInspectionList[index].inspectioncode})",
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w600,
                          color: Colors.black54,
                          letterSpacing: 0.2),
                    ),
                  ),
                  Divider(),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 07, vertical: 01),
                    child: Text(
                      "Inspection Official: ${userProvider.viewMonthlyInspectionList[index].levelofinspection}",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.black54,
                          letterSpacing: 0.3),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(3.0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black54)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Center(
                              child: Text(
                            "Corridor 1",
                            style: TextStyle(fontSize: 18),
                          )),
                          const Divider(
                            thickness: 03,
                          ),
                          Text(
                            "Under Ground(SWA - SSA Ramp): ",
                            style: TextStyle(fontSize: 15),
                          ),
                          Wrap(
                            children: List.generate(
                                userProvider
                                    .viewMonthlyInspectionList[index].details!
                                    .split("{")
                                    .elementAt(0)
                                    .split("^")
                                    .first
                                    .split(",")
                                    .toList()
                                    .length, (index1) {
                              return InkWell(
                                onTap: () {
                                  // print(userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").first.split(",").toList()[index1]);
                                  if (userProvider
                                              .viewMonthlyInspectionList[index]
                                              .details!
                                              .split("{")
                                              .elementAt(0)
                                              .split("^")
                                              .first ==
                                          "Based on Inspections" ||
                                      userProvider
                                              .viewMonthlyInspectionList[index]
                                              .details!
                                              .split("{")
                                              .elementAt(0)
                                              .split("^")
                                              .first ==
                                          "-") {
                                  } else {
                                    userProvider.clearBlueGreenData();
                                    Map dataList = userProvider
                                        .viewMonthlyInspectionList[index]
                                        .details!
                                        .split("{")
                                        .elementAt(0)
                                        .split("^")
                                        .first
                                        .split(",")
                                        .asMap();
                                    List<String>? underDataList = userProvider
                                        .viewMonthlyInspectionList[index]
                                        .pMUnderGround
                                        ?.split(",")
                                        .toList();

                                    dataList.forEach((key, value) {
                                      if (underDataList?.any((element) =>
                                              element
                                                  .trim()
                                                  .replaceAll(' ', '') ==
                                              value
                                                  .trim()
                                                  .replaceAll(' ', '')) ==
                                          true) {
                                        userProvider.changeGreenData(value);
                                      } else {
                                        userProvider.changeBlueData(value);
                                      }
                                    });
                                    if (userProvider.userData?.userrole
                                                .toString()
                                                .toLowerCase() ==
                                            "cmrl-ex" &&
                                        userProvider.blueData.isEmpty) {
                                    } else {
                                      userProvider.changeInspectionCtrlData(
                                          "${userProvider.blueData.join(",").toString()}");
                                      showDialog(
                                          context: context,
                                          builder:
                                              (BuildContext dialogcontext) {
                                            return StatefulBuilder(
                                                builder: (context, setState) {
                                              return CustomDialogBox(
                                                  userProvider.selectedSearchInspectionMonth
                                                              .month
                                                              .toInt() <
                                                          9
                                                      ? "${userProvider.selectedSearchInspectionMonth.year}-0${userProvider.selectedSearchInspectionMonth.month}"
                                                      : "${userProvider.selectedSearchInspectionMonth.year}-${userProvider.selectedSearchInspectionMonth.month}",
                                                  "${userProvider.viewMonthlyInspectionList[index].inspectionname!}",
                                                  "${userProvider.viewMonthlyInspectionList[index].levelofinspection!}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(3)} Under Ground",
                                                  "${userProvider.greenData.isEmpty ? "Not Started Yet" : userProvider.greenData.join(",").toString()}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(2)}",
                                                  "Under Ground",
                                                  "${userProvider.greenData.isEmpty ? "" : userProvider.greenData.join(",").toString()}",
                                                  "${userProvider.inspectionsCtrl.text.toString()}",
                                                  "Corridor 1",
                                                  "${userProvider.viewMonthlyInspectionList[index].inspectioncode}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").first.split(",").toList().join(",").toString()}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").first.split(",").toList().elementAt(index1)}",
                                                  "Under Ground");
                                            });
                                          });
                                    }
                                  }
                                },
                                child: Text(
                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").first.split(",").toList().elementAt(index1)},",
                                  style: TextStyle(
                                      color: userProvider
                                                  .viewMonthlyInspectionList[
                                                      index]
                                                  .pMUnderGround
                                                  ?.split(",")
                                                  .toList()
                                                  .any((element) =>
                                                      element
                                                          .trim()
                                                          .replaceAll(' ', '')
                                                          .toString() ==
                                                      "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").first.split(",").elementAt(index1).trim().replaceAll(' ', '')}") ==
                                              true
                                          ? Colors.green
                                          : Colors.blue,
                                      fontSize: 15),
                                ),
                              );
                            }),
                          ),
                          Text(
                            "Total: ${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").first == "Based on Inspections" ? "Based on Inspections" : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").first == "-" ? 0 : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").first.split(",").toList().length.toString()}",
                            style: TextStyle(fontSize: 15),
                          ),
                          const Divider(),
                          Text(
                            "Elevated(SSA Ramp - SAP): ",
                            style: TextStyle(fontSize: 15),
                          ),
                          Wrap(
                            children: List.generate(
                                userProvider
                                    .viewMonthlyInspectionList[index].details!
                                    .split("{")
                                    .elementAt(0)
                                    .split("^")
                                    .elementAt(1)
                                    .split(",")
                                    .toList()
                                    .length, (index1) {
                              return InkWell(
                                onTap: () {
                                  print(userProvider
                                      .viewMonthlyInspectionList[index]
                                      .pMEleveted
                                      ?.split(","));
                                  if (userProvider
                                              .viewMonthlyInspectionList[index]
                                              .details!
                                              .split("{")
                                              .elementAt(0)
                                              .split("^")
                                              .elementAt(1) ==
                                          "-" ||
                                      userProvider
                                              .viewMonthlyInspectionList[index]
                                              .details!
                                              .split("{")
                                              .elementAt(0)
                                              .split("^")
                                              .elementAt(1) ==
                                          "Based on Inspections") {
                                  } else {
                                    userProvider.clearBlueGreenData();
                                    Map dataList = userProvider
                                        .viewMonthlyInspectionList[index]
                                        .details!
                                        .split("{")
                                        .elementAt(0)
                                        .split("^")
                                        .elementAt(1)
                                        .split(",")
                                        .asMap();
                                    List<String>? underDataList = userProvider
                                        .viewMonthlyInspectionList[index]
                                        .pMEleveted
                                        ?.split(",")
                                        .toList();

                                    dataList.forEach((key, value) {
                                      if (underDataList?.any((element) =>
                                              element
                                                  .trim()
                                                  .replaceAll(' ', '') ==
                                              value
                                                  .trim()
                                                  .replaceAll(' ', '')) ==
                                          true) {
                                        userProvider.changeGreenData(value);
                                      } else {
                                        userProvider.changeBlueData(value);
                                      }
                                    });
                                    if (userProvider.userData?.userrole
                                                .toString()
                                                .toLowerCase() ==
                                            "cmrl-ex" &&
                                        userProvider.blueData.isEmpty) {
                                    } else {
                                      userProvider.changeInspectionCtrlData(
                                          "${userProvider.blueData.join(",").toString()}");
                                      showDialog(
                                          context: context,
                                          builder:
                                              (BuildContext dialogcontext) {
                                            return StatefulBuilder(
                                                builder: (context, setState) {
                                              return CustomDialogBox(
                                                  userProvider.selectedSearchInspectionMonth
                                                              .month
                                                              .toInt() <
                                                          9
                                                      ? "${userProvider.selectedSearchInspectionMonth.year}-0${userProvider.selectedSearchInspectionMonth.month}"
                                                      : "${userProvider.selectedSearchInspectionMonth.year}-${userProvider.selectedSearchInspectionMonth.month}",
                                                  "${userProvider.viewMonthlyInspectionList[index].inspectionname!}",
                                                  "${userProvider.viewMonthlyInspectionList[index].levelofinspection!}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(3)} Elevated",
                                                  "${userProvider.greenData.isEmpty ? "Not Started Yet" : userProvider.greenData.join(",").toString()}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(2)}",
                                                  "Elevated",
                                                  "${userProvider.greenData.isEmpty ? "" : userProvider.greenData.join(",").toString()}",
                                                  "${userProvider.inspectionsCtrl.text.toString()}",
                                                  "Corridor 1",
                                                  "${userProvider.viewMonthlyInspectionList[index].inspectioncode}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(1).split(",").toList().join(",").toString()}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(1).split(",").toList().elementAt(index1)}",
                                                  "Elevated");
                                            });
                                          });
                                    }
                                  }
                                },
                                child: Text(
                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(1).split(",").toList().elementAt(index1)},",
                                  style: TextStyle(
                                      color: userProvider
                                                  .viewMonthlyInspectionList[
                                                      index]
                                                  .pMEleveted
                                                  ?.split(",")
                                                  .any((element) =>
                                                      element
                                                          .trim()
                                                          .replaceAll(' ', '')
                                                          .toString() ==
                                                      "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(1).split(",").elementAt(index1).trim().replaceAll(' ', '').toString()}") ==
                                              true
                                          ? Colors.green
                                          : Colors.blue,
                                      fontSize: 15),
                                ),
                              );
                            }),
                          ),
                          Text(
                            "Total: ${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(1) == "Based on Inspections" ? "Based on Inspections" : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(1) == "-" ? 0 : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(0).split("^").elementAt(1).split(",").toList().length.toString()}",
                            style: TextStyle(fontSize: 15),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(3.0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black54)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Center(
                              child: Text(
                            "Corridor 2",
                            style: TextStyle(fontSize: 18),
                          )),
                          const Divider(
                            thickness: 03,
                          ),
                          Text(
                            "Under Ground(SWA - SSA Ramp): ",
                            style: TextStyle(fontSize: 15),
                          ),
                          Wrap(
                            children: List.generate(
                                userProvider
                                    .viewMonthlyInspectionList[index].details!
                                    .split("{")
                                    .elementAt(1)
                                    .split("^")
                                    .first
                                    .split(",")
                                    .toList()
                                    .length, (index1) {
                              return InkWell(
                                onTap: () {
                                  if (userProvider
                                              .viewMonthlyInspectionList[index]
                                              .details!
                                              .split("{")
                                              .elementAt(1)
                                              .split("^")
                                              .first ==
                                          "-" ||
                                      userProvider
                                              .viewMonthlyInspectionList[index]
                                              .details!
                                              .split("{")
                                              .elementAt(1)
                                              .split("^")
                                              .first ==
                                          "Based on Inspections") {
                                  } else {
                                    userProvider.clearBlueGreenData();
                                    Map dataList = userProvider
                                        .viewMonthlyInspectionList[index]
                                        .details!
                                        .split("{")
                                        .elementAt(1)
                                        .split("^")
                                        .first
                                        .split(",")
                                        .asMap();
                                    List<String>? underDataList = userProvider
                                        .viewMonthlyInspectionList[index]
                                        .pMUnderGround
                                        ?.split(",")
                                        .toList();

                                    dataList.forEach((key, value) {
                                      if (underDataList?.any((element) =>
                                              element
                                                  .trim()
                                                  .replaceAll(' ', '') ==
                                              value
                                                  .trim()
                                                  .replaceAll(' ', '')) ==
                                          true) {
                                        userProvider.changeGreenData(value);
                                      } else {
                                        userProvider.changeBlueData(value);
                                      }
                                    });
                                    if (userProvider.userData?.userrole
                                                .toString()
                                                .toLowerCase() ==
                                            "cmrl-ex" &&
                                        userProvider.blueData.isEmpty) {
                                    } else {
                                      userProvider.changeInspectionCtrlData(
                                          "${userProvider.blueData.join(",").toString()}");
                                      showDialog(
                                          context: context,
                                          builder:
                                              (BuildContext dialogcontext) {
                                            return StatefulBuilder(
                                                builder: (context, setState) {
                                              return CustomDialogBox(
                                                  userProvider.selectedSearchInspectionMonth
                                                              .month
                                                              .toInt() <
                                                          9
                                                      ? "${userProvider.selectedSearchInspectionMonth.year}-0${userProvider.selectedSearchInspectionMonth.month}"
                                                      : "${userProvider.selectedSearchInspectionMonth.year}-${userProvider.selectedSearchInspectionMonth.month}",
                                                  "${userProvider.viewMonthlyInspectionList[index].inspectionname!}",
                                                  "${userProvider.viewMonthlyInspectionList[index].levelofinspection!}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(3)} Under Ground",
                                                  "${userProvider.greenData.isEmpty ? "Not Started Yet" : userProvider.greenData.join(",").toString()}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(2)}",
                                                  "Under Ground",
                                                  "${userProvider.greenData.isEmpty ? "" : userProvider.greenData.join(",").toString()}",
                                                  "${userProvider.inspectionsCtrl.text.toString()}",
                                                  "Corridor 2",
                                                  "${userProvider.viewMonthlyInspectionList[index].inspectioncode}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").first.split(",").toList().join(",").toString()}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").first.split(",").toList().elementAt(index1)}",
                                                  "Under Ground");
                                            });
                                          });
                                    }
                                  }
                                },
                                child: Text(
                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").first.split(",").toList().elementAt(index1)},",
                                  style: TextStyle(
                                      color: userProvider
                                                  .viewMonthlyInspectionList[
                                                      index]
                                                  .pMUnderGround
                                                  ?.split(",")
                                                  .toList()
                                                  .any((element) =>
                                                      element
                                                          .trim()
                                                          .replaceAll(' ', '')
                                                          .toString() ==
                                                      "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").first.split(",").elementAt(index1).trim().replaceAll(' ', '')}") ==
                                              true
                                          ? Colors.green
                                          : Colors.blue,
                                      fontSize: 15),
                                ),
                              );
                            }),
                          ),
                          Text(
                            "Total: ${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").first == "Based on Inspections" ? "Based on Inspections" : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").first == "-" ? 0 : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").first.split(",").toList().length.toString()}",
                            style: TextStyle(fontSize: 16),
                          ),
                          const Divider(),
                          Text(
                            "Elevated(SSA Ramp - SAP): ",
                            style: TextStyle(fontSize: 15),
                          ),
                          Wrap(
                            children: List.generate(
                                userProvider
                                    .viewMonthlyInspectionList[index].details!
                                    .split("{")
                                    .elementAt(1)
                                    .split("^")
                                    .elementAt(1)
                                    .split(",")
                                    .toList()
                                    .length, (index1) {
                              return InkWell(
                                onTap: () {
                                  if (userProvider
                                              .viewMonthlyInspectionList[index]
                                              .details!
                                              .split("{")
                                              .elementAt(1)
                                              .split("^")
                                              .elementAt(1) ==
                                          "-" ||
                                      userProvider
                                              .viewMonthlyInspectionList[index]
                                              .details!
                                              .split("{")
                                              .elementAt(1)
                                              .split("^")
                                              .elementAt(1) ==
                                          "Based on Inspections") {
                                  } else {
                                    userProvider.clearBlueGreenData();
                                    Map dataList = userProvider
                                        .viewMonthlyInspectionList[index]
                                        .details!
                                        .split("{")
                                        .elementAt(1)
                                        .split("^")
                                        .elementAt(1)
                                        .split(",")
                                        .asMap();
                                    List<String>? underDataList = userProvider
                                        .viewMonthlyInspectionList[index]
                                        .pMEleveted
                                        ?.split(",")
                                        .toList();

                                    dataList.forEach((key, value) {
                                      if (underDataList?.any((element) =>
                                              element
                                                  .trim()
                                                  .replaceAll(' ', '') ==
                                              value
                                                  .trim()
                                                  .replaceAll(' ', '')) ==
                                          true) {
                                        userProvider.changeGreenData(value);
                                      } else {
                                        userProvider.changeBlueData(value);
                                      }
                                    });

                                    if (userProvider.userData?.userrole
                                                .toString()
                                                .toLowerCase() ==
                                            "cmrl-ex" &&
                                        userProvider.blueData.isEmpty) {
                                    } else {
                                      userProvider.changeInspectionCtrlData(
                                          "${userProvider.blueData.join(",").toString()}");
                                      showDialog(
                                          context: context,
                                          builder:
                                              (BuildContext dialogcontext) {
                                            return StatefulBuilder(
                                                builder: (context, setState) {
                                              return CustomDialogBox(
                                                  userProvider.selectedSearchInspectionMonth
                                                              .month
                                                              .toInt() <
                                                          9
                                                      ? "${userProvider.selectedSearchInspectionMonth.year}-0${userProvider.selectedSearchInspectionMonth.month}"
                                                      : "${userProvider.selectedSearchInspectionMonth.year}-${userProvider.selectedSearchInspectionMonth.month}",
                                                  "${userProvider.viewMonthlyInspectionList[index].inspectionname!}",
                                                  "${userProvider.viewMonthlyInspectionList[index].levelofinspection!}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(3)} Elevated",
                                                  "${userProvider.greenData.isEmpty ? "Not Started Yet" : userProvider.greenData.join(",").toString()}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(2)}",
                                                  "Elevated",
                                                  "${userProvider.greenData.isEmpty ? "" : userProvider.greenData.join(",").toString()}",
                                                  "${userProvider.inspectionsCtrl.text.toString()}",
                                                  "Corridor 2",
                                                  "${userProvider.viewMonthlyInspectionList[index].inspectioncode}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(1).split(",").toList().join(",").toString()}",
                                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(1).split(",").toList().elementAt(index1)}",
                                                  "Elevated");
                                            });
                                          });
                                    }
                                  }
                                },
                                child: Text(
                                  "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(1).split(",").toList().elementAt(index1)},",
                                  style: TextStyle(
                                      color: userProvider
                                                  .viewMonthlyInspectionList[
                                                      index]
                                                  .pMEleveted
                                                  ?.split(",")
                                                  .toList()
                                                  .any((element) =>
                                                      element
                                                          .trim()
                                                          .replaceAll(' ', '')
                                                          .toString() ==
                                                      "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(1).split(",").elementAt(index1).trim().replaceAll(' ', '')}") ==
                                              true
                                          ? Colors.green
                                          : Colors.blue,
                                      fontSize: 15),
                                ),
                              );
                            }),
                          ),
                          Text(
                            "Total: ${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(1) == "Based on Inspections" ? "Based on Inspections" : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(1) == "-" ? 0 : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(1).split("^").elementAt(1).split(",").toList().length.toString()}",
                            style: TextStyle(fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                  ),
                  //coridor 2 depot
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Table(
                      border: TableBorder.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1),
                      children: [
                        TableRow(children: [
                          Column(children: [
                            Text('Corridor 2-Depot',
                                style: TextStyle(fontSize: 17.0))
                          ]),
                          Column(children: [
                            Text('Total', style: TextStyle(fontSize: 17.0))
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Wrap(
                              children: List.generate(
                                  userProvider
                                      .viewMonthlyInspectionList[index].details!
                                      .split("{")
                                      .elementAt(2)
                                      .split("^")
                                      .first
                                      .split(",")
                                      .toList()
                                      .length, (index1) {
                                return InkWell(
                                  onTap: () {
                                    if (userProvider
                                                .viewMonthlyInspectionList[
                                                    index]
                                                .details!
                                                .split("{")
                                                .elementAt(2)
                                                .split("^")
                                                .first ==
                                            "-" ||
                                        userProvider
                                                .viewMonthlyInspectionList[
                                                    index]
                                                .details!
                                                .split("{")
                                                .elementAt(2)
                                                .split("^")
                                                .first ==
                                            "Based on Inspections") {
                                    } else {
                                      userProvider.clearBlueGreenData();
                                      Map dataList = userProvider
                                          .viewMonthlyInspectionList[index]
                                          .details!
                                          .split("{")
                                          .elementAt(2)
                                          .split("^")
                                          .first
                                          .split(",")
                                          .asMap();
                                      List<String>? underDataList = userProvider
                                          .viewMonthlyInspectionList[index]
                                          .pMUnderGround
                                          ?.split(",")
                                          .toList();

                                      dataList.forEach((key, value) {
                                        if (underDataList?.any((element) =>
                                                element
                                                    .trim()
                                                    .replaceAll(' ', '') ==
                                                value
                                                    .trim()
                                                    .replaceAll(' ', '')) ==
                                            true) {
                                          userProvider.changeGreenData(value);
                                        } else {
                                          userProvider.changeBlueData(value);
                                        }
                                      });
                                      if (userProvider.userData?.userrole
                                                  .toString()
                                                  .toLowerCase() ==
                                              "cmrl-ex" &&
                                          userProvider.blueData.isEmpty) {
                                      } else {
                                        userProvider.changeInspectionCtrlData(
                                            "${userProvider.blueData.join(",").toString()}");
                                        showDialog(
                                            context: context,
                                            builder:
                                                (BuildContext dialogcontext) {
                                              return StatefulBuilder(
                                                  builder: (context, setState) {
                                                return CustomDialogBox(
                                                    userProvider.selectedSearchInspectionMonth
                                                                .month
                                                                .toInt() <
                                                            9
                                                        ? "${userProvider.selectedSearchInspectionMonth.year}-0${userProvider.selectedSearchInspectionMonth.month}"
                                                        : "${userProvider.selectedSearchInspectionMonth.year}-${userProvider.selectedSearchInspectionMonth.month}",
                                                    "${userProvider.viewMonthlyInspectionList[index].inspectionname!}",
                                                    "${userProvider.viewMonthlyInspectionList[index].levelofinspection!}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(2).split("^").elementAt(3)} Under Ground",
                                                    "${userProvider.greenData.isEmpty ? "Not Started Yet" : userProvider.greenData.join(",").toString()}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(2).split("^").elementAt(2)}",
                                                    "Under Ground",
                                                    "${userProvider.greenData.isEmpty ? "" : userProvider.greenData.join(",").toString()}",
                                                    "${userProvider.inspectionsCtrl.text.toString()}",
                                                    "Corridor 2-Depot",
                                                    "${userProvider.viewMonthlyInspectionList[index].inspectioncode}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(2).split("^").first.split(",").toList().join(",").toString()}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(2).split("^").first.split(",").toList().elementAt(index1)}",
                                                    "Under Ground");
                                              });
                                            });
                                      }
                                    }
                                  },
                                  child: Text(
                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(2).split("^").first.split(",").toList().elementAt(index1)},",
                                    style: TextStyle(
                                        color: userProvider
                                                    .viewMonthlyInspectionList[
                                                        index]
                                                    .pMUnderGround
                                                    ?.split(",")
                                                    .toList()
                                                    .any((element) =>
                                                        element
                                                            .trim()
                                                            .replaceAll(' ', '')
                                                            .toString() ==
                                                        "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(2).split("^").first.split(",").elementAt(index1).trim().replaceAll(' ', '')}") ==
                                                true
                                            ? Colors.green
                                            : Colors.blue,
                                        fontSize: 15),
                                  ),
                                );
                              }),
                            )
                          ]),
                          Column(children: [
                            Text(
                                '${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(2).split("^").first == "Based on Inspections" ? "Based on Inspections" : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(2).split("^").first == "-" ? 0 : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(2).split("^").first.split(",").toList().length.toString()}')
                          ]),
                        ]),
                      ],
                    ),
                  ),
                  //coridor 1 depot
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Table(
                      border: TableBorder.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1),
                      children: [
                        TableRow(children: [
                          Column(children: [
                            Text('Corridor 1-Depot',
                                style: TextStyle(fontSize: 17.0))
                          ]),
                          Column(children: [
                            Text('Total', style: TextStyle(fontSize: 17.0))
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Wrap(
                              children: List.generate(
                                  userProvider
                                      .viewMonthlyInspectionList[index].details!
                                      .split("{")
                                      .elementAt(3)
                                      .split("^")
                                      .first
                                      .split(",")
                                      .toList()
                                      .length, (index1) {
                                return InkWell(
                                  onTap: () {
                                    if (userProvider
                                                .viewMonthlyInspectionList[
                                                    index]
                                                .details!
                                                .split("{")
                                                .elementAt(3)
                                                .split("^")
                                                .first ==
                                            "-" ||
                                        userProvider
                                                .viewMonthlyInspectionList[
                                                    index]
                                                .details!
                                                .split("{")
                                                .elementAt(3)
                                                .split("^")
                                                .first ==
                                            "Based on Inspections") {
                                    } else {
                                      userProvider.clearBlueGreenData();
                                      Map dataList = userProvider
                                          .viewMonthlyInspectionList[index]
                                          .details!
                                          .split("{")
                                          .elementAt(3)
                                          .split("^")
                                          .first
                                          .split(",")
                                          .asMap();
                                      List<String>? underDataList = userProvider
                                          .viewMonthlyInspectionList[index]
                                          .pMUnderGround
                                          ?.split(",")
                                          .toList();

                                      dataList.forEach((key, value) {
                                        if (underDataList?.any((element) =>
                                                element
                                                    .trim()
                                                    .replaceAll(' ', '') ==
                                                value
                                                    .trim()
                                                    .replaceAll(' ', '')) ==
                                            true) {
                                          userProvider.changeGreenData(value);
                                        } else {
                                          userProvider.changeBlueData(value);
                                        }
                                      });
                                      if (userProvider.userData?.userrole
                                                  .toString()
                                                  .toLowerCase() ==
                                              "cmrl-ex" &&
                                          userProvider.blueData.isEmpty) {
                                      } else {
                                        userProvider.changeInspectionCtrlData(
                                            "${userProvider.blueData.join(",").toString()}");
                                        showDialog(
                                            context: context,
                                            builder:
                                                (BuildContext dialogcontext) {
                                              return StatefulBuilder(
                                                  builder: (context, setState) {
                                                return CustomDialogBox(
                                                    userProvider.selectedSearchInspectionMonth
                                                                .month
                                                                .toInt() <
                                                            9
                                                        ? "${userProvider.selectedSearchInspectionMonth.year}-0${userProvider.selectedSearchInspectionMonth.month}"
                                                        : "${userProvider.selectedSearchInspectionMonth.year}-${userProvider.selectedSearchInspectionMonth.month}",
                                                    "${userProvider.viewMonthlyInspectionList[index].inspectionname!}",
                                                    "${userProvider.viewMonthlyInspectionList[index].levelofinspection!}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(3).split("^").elementAt(3)} Under Ground",
                                                    "${userProvider.greenData.isEmpty ? "Not Started Yet" : userProvider.greenData.join(",").toString()}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(3).split("^").elementAt(2)}",
                                                    "Under Ground",
                                                    "${userProvider.greenData.isEmpty ? "" : userProvider.greenData.join(",").toString()}",
                                                    "${userProvider.inspectionsCtrl.text.toString()}",
                                                    "Corridor 1-Depot",
                                                    "${userProvider.viewMonthlyInspectionList[index].inspectioncode}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(3).split("^").first.split(",").toList().join(",").toString()}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(3).split("^").first.split(",").toList().elementAt(index1)}",
                                                    "Under Ground");
                                              });
                                            });
                                      }
                                    }
                                  },
                                  child: Text(
                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(3).split("^").first.split(",").toList().elementAt(index1)},",
                                    style: TextStyle(
                                        color: userProvider
                                                    .viewMonthlyInspectionList[
                                                        index]
                                                    .pMUnderGround
                                                    ?.split(",")
                                                    .toList()
                                                    .any((element) =>
                                                        element
                                                            .trim()
                                                            .replaceAll(' ', '')
                                                            .toString() ==
                                                        "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(3).split("^").first.split(",").elementAt(index1).trim().replaceAll(' ', '')}") ==
                                                true
                                            ? Colors.green
                                            : Colors.blue,
                                        fontSize: 15),
                                  ),
                                );
                              }),
                            )
                          ]),
                          Column(children: [
                            Text(
                                '${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(3).split("^").first == "Based on Inspections" ? "Based on Inspections" : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(3).split("^").first == "-" ? 0 : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(3).split("^").first.split(",").toList().length.toString()}')
                          ]),
                        ]),
                      ],
                    ),
                  ),
                  //
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Table(
                      border: TableBorder.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1),
                      children: [
                        TableRow(children: [
                          Column(children: [
                            Text('Corridor 1-Extn',
                                style: TextStyle(fontSize: 17.0))
                          ]),
                          Column(children: [
                            Text('Total', style: TextStyle(fontSize: 17.0))
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Wrap(
                              children: List.generate(
                                  userProvider
                                      .viewMonthlyInspectionList[index].details!
                                      .split("{")
                                      .elementAt(4)
                                      .split("^")
                                      .first
                                      .split(",")
                                      .toList()
                                      .length, (index1) {
                                return InkWell(
                                  onTap: () {
                                    if (userProvider
                                                .viewMonthlyInspectionList[
                                                    index]
                                                .details!
                                                .split("{")
                                                .elementAt(4)
                                                .split("^")
                                                .first ==
                                            "-" ||
                                        userProvider
                                                .viewMonthlyInspectionList[
                                                    index]
                                                .details!
                                                .split("{")
                                                .elementAt(4)
                                                .split("^")
                                                .first ==
                                            "Based on Inspections") {
                                    } else {
                                      userProvider.clearBlueGreenData();
                                      Map dataList = userProvider
                                          .viewMonthlyInspectionList[index]
                                          .details!
                                          .split("{")
                                          .elementAt(4)
                                          .split("^")
                                          .first
                                          .split(",")
                                          .asMap();
                                      List<String>? underDataList = userProvider
                                          .viewMonthlyInspectionList[index]
                                          .pMUnderGround
                                          ?.split(",")
                                          .toList();

                                      dataList.forEach((key, value) {
                                        if (underDataList?.any((element) =>
                                                element
                                                    .trim()
                                                    .replaceAll(' ', '') ==
                                                value
                                                    .trim()
                                                    .replaceAll(' ', '')) ==
                                            true) {
                                          userProvider.changeGreenData(value);
                                        } else {
                                          userProvider.changeBlueData(value);
                                        }
                                      });
                                      if (userProvider.userData?.userrole
                                                  .toString()
                                                  .toLowerCase() ==
                                              "cmrl-ex" &&
                                          userProvider.blueData.isEmpty) {
                                      } else {
                                        userProvider.changeInspectionCtrlData(
                                            "${userProvider.blueData.join(",").toString()}");
                                        showDialog(
                                            context: context,
                                            builder:
                                                (BuildContext dialogcontext) {
                                              return StatefulBuilder(
                                                  builder: (context, setState) {
                                                return CustomDialogBox(
                                                    userProvider.selectedSearchInspectionMonth
                                                                .month
                                                                .toInt() <
                                                            9
                                                        ? "${userProvider.selectedSearchInspectionMonth.year}-0${userProvider.selectedSearchInspectionMonth.month}"
                                                        : "${userProvider.selectedSearchInspectionMonth.year}-${userProvider.selectedSearchInspectionMonth.month}",
                                                    "${userProvider.viewMonthlyInspectionList[index].inspectionname!}",
                                                    "${userProvider.viewMonthlyInspectionList[index].levelofinspection!}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(4).split("^").elementAt(3)} Under Ground",
                                                    "${userProvider.greenData.isEmpty ? "Not Started Yet" : userProvider.greenData.join(",").toString()}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(4).split("^").elementAt(2)}",
                                                    "Under Ground",
                                                    "${userProvider.greenData.isEmpty ? "" : userProvider.greenData.join(",").toString()}",
                                                    "${userProvider.inspectionsCtrl.text.toString()}",
                                                    "Corridor 1-Extn",
                                                    "${userProvider.viewMonthlyInspectionList[index].inspectioncode}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(4).split("^").first.split(",").toList().join(",").toString()}",
                                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(4).split("^").first.split(",").toList().elementAt(index1)}",
                                                    "Under Ground");
                                              });
                                            });
                                      }
                                    }
                                  },
                                  child: Text(
                                    "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(4).split("^").first.split(",").toList().elementAt(index1)},",
                                    style: TextStyle(
                                        color: userProvider
                                                    .viewMonthlyInspectionList[
                                                        index]
                                                    .pMUnderGround
                                                    ?.split(",")
                                                    .toList()
                                                    .any((element) =>
                                                        element
                                                            .trim()
                                                            .replaceAll(' ', '')
                                                            .toString() ==
                                                        "${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(4).split("^").first.split(",").elementAt(index1).trim().replaceAll(' ', '')}") ==
                                                true
                                            ? Colors.green
                                            : Colors.blue,
                                        fontSize: 15),
                                  ),
                                );
                              }),
                            )
                          ]),
                          Column(children: [
                            Text(
                                '${userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(4).split("^").first == "Based on Inspections" ? "Based on Inspections" : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(4).split("^").first == "-" ? 0 : userProvider.viewMonthlyInspectionList[index].details!.split("{").elementAt(4).split("^").first.split(",").toList().length.toString()}')
                          ]),
                        ]),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: 40,
                      color: kPrimaryColor,
                      child: Center(
                        child: Text(
                          "",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
      staggeredTileBuilder: (index) => StaggeredTile.fit(ratio),
    );
  }
}

class CustomGridViewPMJobData extends StatelessWidget {
  const CustomGridViewPMJobData({
    required this.jobCardCtrl,
    required this.userProvider,
    required this.ratio,
    required this.pdfViewerController,
  });

  final UserProvider userProvider;
  final ratio;
  final PdfViewerController pdfViewerController;
  final TextEditingController jobCardCtrl;

  @override
  Widget build(BuildContext context) {
    return userProvider.loadingAuth == true
        ? LoadingWidget(70.0, "Loading..")
        : StaggeredGridView.countBuilder(
            primary: false,
            crossAxisCount: 12,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: userProvider.viewPreventiveMaintenanceData.length,
            itemBuilder: (BuildContext context, int index) => Padding(
              padding: EdgeInsets.symmetric(horizontal: 05),
              child: Container(
                decoration: BoxDecoration(
                    color: kPrimaryColor,
                    borderRadius: BorderRadius.circular(4),
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.black26,
                          offset: Offset(0, 2),
                          blurRadius: 6)
                    ]),
                margin: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
                child: Container(
                  color: userProvider
                              .viewPreventiveMaintenanceData[index].status ==
                          "open"
                      ? Colors.red[100]
                      : userProvider.viewPreventiveMaintenanceData[index]
                                  .status ==
                              "inprogress"
                          ? Colors.amber[100]
                          : userProvider.viewPreventiveMaintenanceData[index]
                                      .status ==
                                  "completed"
                              ? Colors.cyan[100]
                              : Colors.green[100],
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          userProvider.getPMJobCardDataApi(
                              context,
                              "${userProvider.viewPreventiveMaintenanceData[index].jobcardid}",
                              "${userProvider.viewPreventiveMaintenanceData[index].status}");
                          Navigator.push(
                              context,
                              PageRouteBuilder(
                                  pageBuilder: (context, animation,
                                          secondaryAnimation) =>
                                      JobCardDetails(
                                          index: index,
                                          isButton: false,
                                          cardId: userProvider
                                              .viewPreventiveMaintenanceData[
                                                  index]
                                              .jobcardid,
                                          inspection:
                                              jobCardCtrl.text.toString(),
                                          inspectionId: userProvider
                                              .viewPreventiveMaintenanceData[
                                                  index]
                                              .inspectionid,
                                          status: userProvider
                                              .viewPreventiveMaintenanceData[
                                                  index]
                                              .status,
                                          corridor: userProvider
                                              .viewPreventiveMaintenanceData[
                                                  index]
                                              .corridor,
                                          inspectionScheduled:
                                              "${userProvider.viewPreventiveMaintenanceData[index].allocateduser}",
                                          inspectionScheduledOn: userProvider
                                              .viewPreventiveMaintenanceData[
                                                  index]
                                              .scheduledon,
                                          allocatedStaff:
                                              "${userProvider.viewPreventiveMaintenanceData[index].allocateduser}",
                                          inspectionPlan: userProvider
                                              .viewPreventiveMaintenanceData[
                                                  index]
                                              .dateofinspectionplanned)));
                        },
                        child: Container(
                          height: 40,
                          width: double.infinity,
                          color: kPrimaryColor,
                          child: Text(
                            " ${index + 1}. JobCard ID : ${userProvider.viewPreventiveMaintenanceData[index].jobcardid}",
                            style: TextStyle(
                                fontSize: 18,
                                decoration: TextDecoration.underline,
                                color: Colors.white),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 07, vertical: 01),
                        child: Text(
                            "Inspection : ${userProvider.viewPreventiveMaintenanceData[index].inspectionid}"),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 07, vertical: 01),
                        child: Text(
                            "Corridor : ${userProvider.viewPreventiveMaintenanceData[index].corridor}"),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 07, vertical: 01),
                        child: Text(
                            "Inspection Details : ${userProvider.viewPreventiveMaintenanceData[index].type} - ${userProvider.viewPreventiveMaintenanceData[index].value}"),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 07, vertical: 01),
                        child: Text(
                            "Allocated Staff : ${userProvider.viewPreventiveMaintenanceData[index].allocateduser} (${userProvider.viewPreventiveMaintenanceData[index].staffdesignation})"),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 07, vertical: 01),
                        child: Text(
                            "Inspection Created On : ${userProvider.viewPreventiveMaintenanceData[index].createddate}"),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 07, vertical: 01),
                        child: Text(
                            "Inspection Planned On : ${userProvider.viewPreventiveMaintenanceData[index].dateofinspectionplanned}"),
                      ),
                      Container(
                        color: Colors.amber,
                        width: double.infinity,
                        height: 40,
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-je" ||
                                          userProvider.userData?.userrole
                                                  .toString()
                                                  .toLowerCase() ==
                                              "cmrl-ex"
                                      ? showDialog(
                                          context: context,
                                          builder:
                                              (BuildContext dialogcontext) {
                                            return StatefulBuilder(
                                                builder: (context, setState) {
                                              return PreventiveMaintenance(
                                                index: index,
                                                jobCardId: userProvider
                                                    .viewPreventiveMaintenanceData[
                                                        index]
                                                    .jobcardid,
                                                inspection:
                                                    jobCardCtrl.text.toString(),
                                                status: userProvider
                                                    .viewPreventiveMaintenanceData[
                                                        index]
                                                    .status,
                                                corridor: userProvider
                                                    .viewPreventiveMaintenanceData[
                                                        index]
                                                    .corridor,
                                                inspectionDetails:
                                                    "${userProvider.viewPreventiveMaintenanceData[index].type} - ${userProvider.viewPreventiveMaintenanceData[index].value}",
                                                inspectionScheduled:
                                                    "${userProvider.viewPreventiveMaintenanceData[index].allocateduser} on ${userProvider.viewPreventiveMaintenanceData[index].createddate}",
                                                allocatedStaff:
                                                    "${userProvider.viewPreventiveMaintenanceData[index].allocateduser} (${userProvider.viewPreventiveMaintenanceData[index].staffdesignation})",
                                                inspectionPlan: userProvider
                                                    .viewPreventiveMaintenanceData[
                                                        index]
                                                    .dateofinspectionplanned,
                                              );
                                            });
                                          })
                                      : userProvider.userData?.userrole
                                                      .toString()
                                                      .toLowerCase() ==
                                                  "contractor" &&
                                              userProvider.viewPreventiveMaintenanceData[index].status ==
                                                  "open"
                                          ? showDialog(
                                              context: context,
                                              builder:
                                                  (BuildContext dialogcontext) {
                                                return StatefulBuilder(builder:
                                                    (context, setState) {
                                                  return PreventiveMaintenance(
                                                    index: index,
                                                    jobCardId: userProvider
                                                        .viewPreventiveMaintenanceData[
                                                            index]
                                                        .jobcardid,
                                                    inspection: jobCardCtrl.text
                                                        .toString(),
                                                    status: userProvider
                                                        .viewPreventiveMaintenanceData[
                                                            index]
                                                        .status,
                                                    corridor: userProvider
                                                        .viewPreventiveMaintenanceData[
                                                            index]
                                                        .corridor,
                                                    inspectionDetails:
                                                        "${userProvider.viewPreventiveMaintenanceData[index].type} - ${userProvider.viewPreventiveMaintenanceData[index].value}",
                                                    inspectionScheduled:
                                                        "${userProvider.viewPreventiveMaintenanceData[index].allocateduser} on ${userProvider.viewPreventiveMaintenanceData[index].createddate}",
                                                    allocatedStaff:
                                                        "${userProvider.viewPreventiveMaintenanceData[index].allocateduser} (${userProvider.viewPreventiveMaintenanceData[index].staffdesignation})",
                                                    inspectionPlan: userProvider
                                                        .viewPreventiveMaintenanceData[
                                                            index]
                                                        .dateofinspectionplanned,
                                                  );
                                                });
                                              })
                                          : Navigator.push(
                                              context,
                                              PageRouteBuilder(
                                                  pageBuilder: (context, animation, secondaryAnimation) => JobCardDetails(
                                                      index: index,
                                                      isButton: true,
                                                      inspection: jobCardCtrl.text
                                                          .toString(),
                                                      cardId: userProvider
                                                          .viewPreventiveMaintenanceData[index]
                                                          .jobcardid,
                                                      inspectionId: userProvider.viewPreventiveMaintenanceData[index].inspectionid,
                                                      status: userProvider.viewPreventiveMaintenanceData[index].status,
                                                      corridor: userProvider.viewPreventiveMaintenanceData[index].corridor,
                                                      inspectionScheduled: "${userProvider.viewPreventiveMaintenanceData[index].allocateduser}",
                                                      inspectionScheduledOn: userProvider.viewPreventiveMaintenanceData[index].scheduledon,
                                                      allocatedStaff: "${userProvider.viewPreventiveMaintenanceData[index].allocateduser}",
                                                      inspectionPlan: userProvider.viewPreventiveMaintenanceData[index].dateofinspectionplanned)));
                                },
                                child: Container(
                                  height: 40,
                                  color: userProvider
                                              .viewPreventiveMaintenanceData[
                                                  index]
                                              .status ==
                                          "open"
                                      ? Colors.red
                                      : userProvider
                                                  .viewPreventiveMaintenanceData[
                                                      index]
                                                  .status ==
                                              "inprogress"
                                          ? Colors.amber
                                          : userProvider
                                                      .viewPreventiveMaintenanceData[
                                                          index]
                                                      .status ==
                                                  "completed"
                                              ? Colors.cyan
                                              : Colors.green,
                                  child: Center(
                                    child: Text(
                                      "${userProvider.viewPreventiveMaintenanceData[index].status == "open" ? "Open" : userProvider.viewPreventiveMaintenanceData[index].status == "inprogress" ? "Inprogress" : userProvider.viewPreventiveMaintenanceData[index].status == "completed" ? "Completed" : "Closed"}",
                                      style: TextStyle(
                                          color: Colors.white,
                                          decoration: userProvider
                                                      .userData?.userrole
                                                      .toString()
                                                      .toLowerCase() ==
                                                  "contractor"
                                              ? TextDecoration.none
                                              : TextDecoration.underline),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  userProvider.launchURL(
                                      "${userProvider.globalURL}/uploads/Weekly Plan/${userProvider.viewPreventiveMaintenanceData[index]}");
                                },
                                child: Container(
                                  height: 40,
                                  color: kPrimaryColor,
                                  child: Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const Icon(Icons.insert_drive_file,
                                            color: Colors.white),
                                        const SizedBox(width: 10),
                                        const Text("View PDF",
                                            style:
                                                TextStyle(color: Colors.white)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            staggeredTileBuilder: (index) => StaggeredTile.fit(ratio),
          );
  }
}

class CustomGridViewPMBufferstopInspection extends StatelessWidget {
  final UserProvider userProvider;
  final ratio;
  CustomGridViewPMBufferstopInspection(
      {required this.userProvider,
        required this.ratio});

  List<String> particulars = [
    "Condition of leg assembly whether any crack or any chipping or any damage to galvinization.",
    "Position of Buffer stop/Wheel stop (Whether any Displacement)",
    "Condition of Bolts of External Unit",
    "Shoe plate No.LH/RH",
    "Condition of nuts /washer",
    "Torque of bolts in friction element after clamping",
    "Torque of bolts in guide claw after clamping",
    "Torque of fixing bolts in rail",
    "Condition of rail behind bufferstop rusted/grease/concrete/other impurities",
    "Condition of Rubber face plate",
    "Cleaning of parts of all friction elements, Guide claw and additional retarder",
    "Condition of High visibility reflector",
    "Condition of slave units",
    "Condition of Bolts Whether Cracked/Chipped",
    "Condition of Bolts Whether Properly Greased",
  ];

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);

    return StaggeredGridView.countBuilder(
      primary: false,
      crossAxisCount: 12,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: 15,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(
              color: kPrimaryColor,
              borderRadius: BorderRadius.circular(4),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black26, offset: Offset(0, 2), blurRadius: 6)
              ]),
          margin: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 07, vertical: 01),
                  child: Container(
                    child: Row(
                      children: [
                        Expanded(
                            child: RichTextWidget(
                                title: "${index + 1}. Particulars:",
                                value: "${particulars[index]}")),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black54)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Center(
                            child: Text("Detail of Inspection",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600))),
                        const Divider(thickness: 03),
                        Text("RH: ", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600)),
                        SizedBox(height: 10),
                        index == 3 || index == 5 || index == 6 || index == 7
                            ? Container(
                          color: Colors.white,
                          child: TextFormField(
                            controller:
                            userProvider.selectedRHCTRLList[index],
                            obscureText: false,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                            enabled: true,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                    BorderRadius.circular(01)),
                                labelStyle: TextStyle(fontSize: 15)),
                          ),
                        )
                            : DropdownWidget(
                          hintText: "",
                          currentItem:
                          userProvider.selectedRHValueList[index],
                          items: userProvider.changeRHList2,
                          itemCallBack: (String newvalue) {
                            userProvider.changeRHItems1(newvalue, index);
                          },
                          title: '',
                        ),
                        const Divider(),
                        Text("LH: ", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600)),
                        SizedBox(height: 10),
                        index == 3 || index == 5 || index == 6 || index == 7
                            ? Container(
                          color: Colors.white,
                          child: TextFormField(
                            controller:
                            userProvider.selectedLHCTRLList[index],
                            obscureText: false,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                            enabled: true,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                    BorderRadius.circular(01)),
                                labelStyle: TextStyle(fontSize: 15)),
                          ),
                        )
                            : DropdownWidget(
                          hintText: "",
                          currentItem:
                          userProvider.selectedLHValueList[index],
                          items: userProvider.changeLHList2,
                          itemCallBack: (String newvalue) {
                            userProvider.changeLHItems1(newvalue, index);
                          },
                          title: '',
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black54)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Center(
                            child: Text("Upload Image",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600))),
                        const Divider(thickness: 03),
                        Row(
                          children: [
                            SizedBox(width: 10),
                            RaisedButton(
                              child: Text("Select Images"),
                              onPressed: () async {
                                await userProvider.loadAssets(index);
                              },
                            ),
                            SizedBox(width: 10),
                           Text("${userProvider.allFilesList.asMap().containsKey(index) == false ? "0" : userProvider.allFilesList[index].length} Image Selected"),
                          ],
                        ),
                        SizedBox(height: 5),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
      staggeredTileBuilder: (index) => StaggeredTile.fit(ratio),
    );
  }

}
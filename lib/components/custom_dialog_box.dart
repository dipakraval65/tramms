import 'package:TRAMMS/components/textfield_widget.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';

class CustomDialogBox extends StatefulWidget {

  final String iMonth,iName,iLevel,corName,iOnComplete;
  final String hdn_AnnualPlanID,hdn_Type,hdn_InspOpen,txt_InspNotOpen,hdn_CorridorName,hdn_InspCode,Value;
  final String singleValue,underOrElevate;
  const CustomDialogBox(this.iMonth,this.iName,this.iLevel,this.corName,this.iOnComplete,this.hdn_AnnualPlanID,
      this.hdn_Type,this.hdn_InspOpen,this.txt_InspNotOpen,this.hdn_CorridorName,this.hdn_InspCode,this.Value,this.singleValue,this.underOrElevate);
  @override
  _CustomDialogBoxState createState() => _CustomDialogBoxState();
}

class _CustomDialogBoxState extends State<CustomDialogBox> {
  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      insetPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.080,vertical: MediaQuery.of(context).size.height * 0.10),
      backgroundColor: Colors.transparent,
      child: Container(
        margin: EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(color: Colors.black,offset: Offset(0,02),
                  blurRadius: 02
              ),
            ]
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  height: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
                    color: kPrimaryColor,
                  ),
                  width: double.infinity,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-je" && userProvider.greenData.any((element) => element.toString().trim() == widget.singleValue.toString().trim()) ? "Inspection Details" : userProvider.userData?.userrole.toString().toLowerCase() == "contractor" && userProvider.greenData.any((element) => element.toString().trim() == widget.singleValue.toString().trim()) ? "Inspection Details" : userProvider.userData?.userrole.toString().toLowerCase() == "contractor" && userProvider.blueData.isNotEmpty ? "Plan Weekly Schedule" : userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-ex" ? "Edit Annual Plan" : "Inspection Details",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600,color: Colors.white),),
                          InkWell(
                              onTap: ()
                              {
                                Navigator.pop(context);
                              },
                              child: Icon(Icons.close,color: Colors.white,))
                        ],
                      ),
                    ),
                  )),
              const SizedBox(height: 15,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Text("Inspection Details:",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w600,color: kPrimaryColor),),
              ),
              Divider(),
              Visibility(
                visible: userProvider.userData?.userrole.toString().toLowerCase() == "contractor" ? false : true,
                child: RichTextWidget(title: "Inspection Month:",value: widget.iMonth,),
              ),
              RichTextWidget(title: "Inspection Name:",value: "${widget.iName} (${widget.hdn_InspCode})",),
              RichTextWidget(title: "Level Of Inspection:",value: widget.iLevel,),
              RichTextWidget(title: "Corridor Name:",value: userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-ex" ? widget.corName : widget.hdn_CorridorName,),
              Visibility(
                visible: userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-ex" ? true : false,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichTextWidget(title: "Inspections On-Going/Completed:",value: widget.iOnComplete,),
                      const SizedBox(height: 07,),
                      const Divider(),
                      const SizedBox(height: 05,),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: Text("Edit Inspections:",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w600,color: kPrimaryColor),),
                      ),
                      const SizedBox(height: 07,),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: TextFieldWidgetEnabled(controller: userProvider.inspectionsCtrl,hint: "Enter New Data",iconData: Icons.edit,),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: Text("Note: While Adding Inspections, use comma(',') as separator",style: TextStyle(color: Colors.blue,fontSize: 12),),
                      ),
                      const SizedBox(height: 22,),
                    ],
                  ),
                ),
              ),
              Visibility(
                visible: userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-ex" ? false : true,
                child: RichTextWidget(title: "Inspection Performed On:",value: "${widget.underOrElevate}-${widget.singleValue}",),
              ),
              Visibility(
                visible: userProvider.userData?.userrole.toString().toLowerCase() == "contractor" && userProvider.blueData.any((element) => element.toString().trim() == widget.singleValue.trim().toString()) ? true : false,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Divider(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: Text("Allocate Date & Staff:",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w600,color: kPrimaryColor),),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                        child: InkWell(
                          onTap: ()
                          {
                                userProvider.pickNewDate(context);
                          },
                          child: TextFieldWidget(
                            hint: "Date of Inspection Planned ",
                            iconData: Icons.date_range, controller: userProvider.inspectionPlanedDateCtrl,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                        child: Container(
                          height: 63,
                          color: Colors.white,
                          child: FormField<String>(
                            builder: (FormFieldState<String>
                            state) {
                              return InputDecorator(
                                decoration: InputDecoration(
                                  labelText:
                                  "Select Accompany Staff Name",
                                  labelStyle: TextStyle(
                                      fontWeight:
                                      FontWeight.bold,
                                      color: Colors.black38),
                                  errorStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontSize: 14.0),
                                  hintText: 'Select Accompany Staff Name',
                                  border: OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius
                                          .circular(0)),
                                ),
                                child:
                                DropdownButtonHideUnderline(
                                  child:
                                  DropdownButton<String>(
                                    value:
                                    userProvider.selectedAllocateUser,
                                    isDense: true,
                                    isExpanded: true,
                                    onChanged: (String? newValue) async{
                                      userProvider.changeAllocateStaffUserMI(newValue.toString());
                                    },
                                    items: userProvider.viewAllocateStaffUsersList
                                        .map((item) {
                                      return new DropdownMenuItem(
                                        child: new Text(
                                            "${item.employeeid}-${item.username} (${item.designationname})"),
                                        value: item.employeeid
                                            .toString(),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                        child: TextFieldAreaWidget(
                          hint: "Remark",
                          iconData: Icons.description_outlined, controller: userProvider.remarkAllocateUser,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15,top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Visibility(
                      visible: userProvider.userData?.userrole == "cmrl-ex" ? true : false,
                      child: MaterialButton(
                        color: kPrimaryColor,
                          onPressed: () async {
                          Navigator.pop(context);
                            Position currentPosition =
                                await userProvider.checkLocation(context);
                            final ipv4 = await Ipify.ipv4();
                            DeviceInfoPlugin deviceInfo =
                            DeviceInfoPlugin();
                            AndroidDeviceInfo androidInfo =
                                await deviceInfo.androidInfo;
                            userProvider.updateMonthlyInspectionApi(context: context, hdn_AnnualPlanID: widget.hdn_AnnualPlanID, hdn_Type: widget.hdn_Type, hdn_InspOpen: widget.hdn_InspOpen, txt_InspNotOpen: widget.txt_InspNotOpen, hdn_CorridorName: widget.hdn_CorridorName, hdn_InspCode: widget.hdn_InspCode, Value: widget.Value, IP: "$ipv4", location: "${currentPosition.latitude.toString()},${currentPosition.longitude.toString()}",deviceUsed: "android ${androidInfo.version.release.toString()},${userProvider.packageInfo!.version.toString()}");
                          },
                          child: Text("Save",style: TextStyle(fontSize: 18,color: Colors.white),)),
                    ),
                    Visibility(
                      visible: userProvider.userData?.userrole == "contractor" && userProvider.blueData.any((element) => element.toString().trim() == widget.singleValue.trim().toString()) ? true : false,
                      child: MaterialButton(
                        color: kPrimaryColor,
                          onPressed: () async {
                          Navigator.pop(context);
                            Position currentPosition =
                                await userProvider.checkLocation(context);
                            final ipv4 = await Ipify.ipv4();
                            DeviceInfoPlugin deviceInfo =
                            DeviceInfoPlugin();
                            AndroidDeviceInfo androidInfo =
                                await deviceInfo.androidInfo;
                            userProvider.allocateStaffApi(context: context, hdn_AnnualPlanID: widget.hdn_AnnualPlanID, hdn_Type: widget.hdn_Type, hdn_CorridorName: widget.hdn_CorridorName, hdn_InspCode: widget.hdn_InspCode, Value: widget.singleValue, IP: "$ipv4", location: "${currentPosition.latitude.toString()},${currentPosition.longitude.toString()}",deviceUsed: "android ${androidInfo.version.release.toString()},${userProvider.packageInfo!.version.toString()}",hdnMonthYear: widget.iMonth,hdnLevel: widget.iLevel);
                          },
                          child: Text("Allocate",style: TextStyle(fontSize: 18,color: Colors.white),)),
                    ),
                    const SizedBox(width: 30,),
                    MaterialButton(
                        color: kPrimaryColor,
                        onPressed: (){
                          Navigator.of(context).pop();
                        },
                        child: Text("Close",style: TextStyle(fontSize: 18,color: Colors.white),)),
                  ],
                ),
              ),
              const SizedBox(height: 10,),
            ],
          ),
        ),
      ),
    );
  }
}

class RichTextWidget extends StatelessWidget {
  const RichTextWidget({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  final String title,value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 05),
      child: Text.rich(
        TextSpan(
          children: [
            TextSpan(text: '$title ',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600,color: Colors.black87),),
            TextSpan(text: "$value",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600,color: Colors.black54),),
          ],
        ),
      ),
    );
  }
}

class PreventiveMaintenance extends StatefulWidget {
  final int index;
  final String? jobCardId,inspection,status,corridor,inspectionDetails,inspectionScheduled,allocatedStaff,inspectionPlan;
  const PreventiveMaintenance({required this.index,this.jobCardId,this.inspection,this.status,this.corridor,this.inspectionDetails,this.inspectionScheduled,this.allocatedStaff,this.inspectionPlan});
  @override
  PM createState() => PM();
}

class PM extends State<PreventiveMaintenance> {
  var PMStatusCtr = TextEditingController();

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      insetPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.080,vertical: MediaQuery.of(context).size.height * 0.10),
      backgroundColor: Colors.transparent,
      child: Container(
        margin: EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(color: Colors.black,offset: Offset(0,02),
                  blurRadius: 02
              ),
            ]
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  height: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
                    color: kPrimaryColor,
                  ),
                  width: double.infinity,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(child: Text(userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-je" || userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-ex" ? "Job CardID Details" : "Change Job CardID Status",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600,color: Colors.white),)),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(Icons.close,color: Colors.white,))
                        ],
                      ),
                    ),
                  )),
              Column(
                children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 07, vertical: 03),
                      child: Column(
                        children: [
                          SizedBox(height: 8),
                          Text(
                            "Inspection Details For Job CardID : ${widget.jobCardId}",
                            style: TextStyle(fontSize: 18,fontWeight: FontWeight.w600,color: kPrimaryColor),
                          ),
                        ],
                      )
                  ),
                  Divider(thickness: 3),
                  Align(
                    alignment: Alignment.topLeft,
                    child: RichTextWidget(title: "Inspection Month, Year:",value: "${widget.inspection}"),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: RichTextWidget(title: "Current Status:",value: "${widget.status}"),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: RichTextWidget(title: "Corridor Name:",value: "${widget.corridor}"),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: RichTextWidget(title: "Inspection Details:",value: "${widget.inspectionDetails}"),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: RichTextWidget(title: "Inspection Scheduled By:",value: "${widget.inspectionScheduled}"),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: RichTextWidget(title: "Allocated Staff:",value: "${widget.allocatedStaff}"),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: RichTextWidget(title: "Date Of Inspection Planned:",value: "${widget.inspectionPlan}"),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 07, vertical: 01),
                    child: Column(
                      children: [
                        userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-je" || userProvider.userData?.userrole.toString().toLowerCase() == "cmrl-ex" ?
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 15, 5),
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: MaterialButton(
                              color: kPrimaryColor,
                              onPressed: (){
                                Navigator.of(context).pop();
                              },
                              child: Text("Close",style: TextStyle(fontSize: 18,color: Colors.white)),
                            ),
                          ),
                        ) :
                        Column(
                          children: [
                            Divider(thickness: 3),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text("Change Status To In-progress :",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15)),
                            ),
                            Divider(),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text("Are you sure that you want to change the status of Job CardID from Open to Inprogress?"),
                            ),
                            SizedBox(height: 15),
                            Container(
                              height: 63,
                              color: Colors.white,
                              child: FormField<String>(
                                builder: (FormFieldState<String>
                                state) {
                                  return InputDecorator(
                                    decoration: InputDecoration(
                                      labelText:
                                      "Change Status",
                                      labelStyle: TextStyle(
                                          fontWeight:
                                          FontWeight.bold,
                                          color: Colors.black38),
                                      errorStyle: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 14.0),
                                      hintText: 'Change Status',
                                      border: OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius
                                              .circular(0)),
                                    ),
                                    child:
                                    DropdownButtonHideUnderline(
                                      child:
                                      DropdownButton<String>(
                                        value: userProvider.selectedStatusItem,
                                        isDense: true,
                                        onChanged: (String? newValue) async{
                                          userProvider.changeStatusItems(newValue!);
                                        },
                                        items: userProvider.changeStatusList.map((String items) {
                                          return DropdownMenuItem(
                                            value: items,
                                            child: Text(items),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            SizedBox(height: 6),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text("Remarks"),
                            ),
                            SizedBox(height: 5),
                            Container(
                              color: Colors.white,
                              child: TextFormField(
                                controller: PMStatusCtr,
                                obscureText: false,
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                                enabled: true,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(01)
                                    ),
                                    labelStyle: TextStyle(
                                        fontSize: 15
                                    )
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Divider(thickness: 3),
                            ButtonBar(
                              children: [
                                userProvider.selectedStatusItem=="Yes" ?
                                RaisedButton(
                                  color: kPrimaryColor,
                                  onPressed: () async {
                                    Position currentPosition =
                                        await userProvider.checkLocation(context);
                                    final ipv4 = await Ipify.ipv4();
                                    DeviceInfoPlugin deviceInfo =
                                    DeviceInfoPlugin();
                                    AndroidDeviceInfo androidInfo =
                                        await deviceInfo.androidInfo;
                                    userProvider.updatePMStatusApi(context: context, hdn_JobCardID: widget.jobCardId.toString(), hdn_PreventiveMasterID: userProvider.viewPreventiveMaintenanceData[widget.index].preventivemasterid!, txt_Remarks: PMStatusCtr.text.toString(), IP: ipv4, DeviceUsed: "android ${androidInfo.version.release.toString()},${userProvider.packageInfo!.version.toString()}", hdn_location: "${currentPosition.latitude.toString()},${currentPosition.longitude.toString()}");
                                    Navigator.of(context).pop();
                                  },
                                  child: Text("Change Status",style: TextStyle(fontSize: 18,color: Colors.white)),
                                ) :
                                Center(),
                                RaisedButton(
                                  color: kPrimaryColor,
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                  child: Text("Close",style: TextStyle(fontSize: 18,color: Colors.white)),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
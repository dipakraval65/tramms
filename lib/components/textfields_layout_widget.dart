import 'dart:math';

import 'package:flutter/material.dart';

import 'textfield_widget.dart';

class TextFieldMobileWidget extends StatelessWidget {

  final TextEditingController scheduleFromCtrl;
  final TextEditingController scheduleToCtrl;
  final VoidCallback voidCallbackFrom;
  final VoidCallback voidCallbackTo;

  const TextFieldMobileWidget(this.scheduleFromCtrl,this.scheduleToCtrl,this.voidCallbackFrom,this.voidCallbackTo);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: voidCallbackFrom,
          child: TextFieldWidget(
            controller: scheduleFromCtrl,
            hint: "Schedule From",
            iconData: Icons.date_range,
          ),
        ),
        SizedBox(height: 20,),
        InkWell(
          onTap: voidCallbackTo,
          child: TextFieldWidget(
            controller: scheduleToCtrl,
            hint: "Schedule To",
            iconData: Icons.date_range,
          ),
        ),
      ],
    );
  }
}
class TextFieldTabletWidget extends StatelessWidget {

  final TextEditingController scheduleFromCtrl;
  final TextEditingController scheduleToCtrl;
  final VoidCallback voidCallbackFrom;
  final VoidCallback voidCallbackTo;
  const TextFieldTabletWidget(this.scheduleFromCtrl,this.scheduleToCtrl,this.voidCallbackFrom,this.voidCallbackTo);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: voidCallbackFrom,
          child: TextFieldWidget(
            controller: scheduleFromCtrl,
            hint: "Schedule From",
            iconData: Icons.date_range,
          ),
        ),
        SizedBox(height: 20,),
        InkWell(
          onTap: voidCallbackTo,
          child: TextFieldWidget(
            controller: scheduleToCtrl,
            hint: "Schedule To",
            iconData: Icons.date_range,
          ),
        ),
      ],
    );
  }
}

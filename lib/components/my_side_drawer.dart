import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:TRAMMS/main.dart';
import 'package:TRAMMS/views/dashboard/home_screen.dart';
import 'package:TRAMMS/views/menus/preventive_maintenance/monthly_inspection/main_monthly_inspection.dart';
import 'package:TRAMMS/views/menus/preventive_maintenance/preventive_maintenance_sub/main_preventive_maintenance.dart';
import 'package:TRAMMS/views/menus/preventive_maintenance/upload_weekly_schedule/main_upload_schedule_screen.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';

class NavigationDrawerWidget extends StatefulWidget {
  @override
  _NavigationDrawerWidgetState createState() => _NavigationDrawerWidgetState();
}

class _NavigationDrawerWidgetState extends State<NavigationDrawerWidget> {
  final padding = EdgeInsets.symmetric(horizontal: 20);

  final List<Entry> exUserMenus = <Entry>[
    Entry(
      'Dashboard',
      <Entry>[],
    ),
    Entry(
      'Plan V/S Actual',
      <Entry>[
        Entry('Plan V/S Actual Master', []),
        Entry('Plan V/S Actual', []),
      ],
    ),
    Entry(
      'Audit',
      <Entry>[
        Entry('Approve Audit Details', []),
        Entry('Audit Report', []),
      ],
    ),
    Entry(
      'Track Masters',
      <Entry>[
        Entry('User Master', []),
        Entry('Designation Master', []),
        Entry('Inspection Master', []),
        Entry('Line Master', []),
        Entry('Corridor Master', []),
        Entry('Station Master', []),
        Entry('Defect Master', []),
        Entry('Notification Master', []),
        Entry('Email Master', []),
        Entry('Foot Master', []),
        Entry('Penalty Master', []),
        Entry('Cumulative Performance', []),
        Entry('CPS Master', []),
      ],
    ),
    Entry(
      'Device',
      <Entry>[
        Entry('Device Master', []),
        Entry('Live Track Device', []),
        Entry('Device History', []),
      ],
    ),
    Entry(
      'Bill Generation',
      <Entry>[
      ],
    ),
    Entry(
      'Tolerance Matrix',
      <Entry>[
        Entry('Curve Matrix', []),
        Entry(
          'Turnout Matrix',
          <Entry>[
            Entry('TO 1 in 9R190', []),
            Entry('TO 1 in 9R300', []),
            Entry('TO 1 in 7R140', []),
            Entry('TO 1 in 7R190', []),
            Entry('TO 1 in 5R100', []),
            Entry('TO 1 in 9R300 Extn', []),
            Entry('TRAP 1 in 7R190', []),
            Entry('TS 1 in 7R140', []),
          ],
        ),
      ],
    ),

    Entry(
      'Store Maintenance',
      <Entry>[
        Entry('Store Master', [
          Entry('Manufacturer Master', []),
          Entry('Category Master', []),
          Entry('Product Master', []),
        ]),
        Entry('Store Maintenance', [
          Entry('Inward Maintenance', []),
          Entry('Approve Request', []),
          Entry('Request Status', []),
          Entry('Stock Report', []),
          Entry('Reorder Leve', []),
          Entry('View Scrap Product', []),
        ]),
      ],
    ),
    Entry(
      'Preventive Maintenance',
      <Entry>[
        Entry('Monthly Inspection', []),
        Entry('Upload Weekly Schedule', []),
        Entry('Preventive Maintenance', []),
        Entry('Defects in PM', []),
        Entry('Miscellaneous Inspection', []),
        Entry('Inspection Matrix', []),
        Entry('Deviation Found', []),
        Entry('Inspection Report', []),
      ],
    ),
    Entry(
      'Corrective Maintenance',
      <Entry>[
        // Entry('Corrective Maintenance', []),
      ],
    ),
    Entry(
      'Reports',
      <Entry>[
        Entry('User Report', []),
        Entry('Designation Report', []),
        Entry('Inspection Report', []),
        Entry('Line Report', []),
        Entry('Corridor Report', []),
        Entry('Station Report', []),
        Entry('Defect Report', []),
        Entry('Category Report', []),
        Entry('Manufacturer Report', []),
        Entry('Product Report', []),
      ],
    ),
    Entry(
      'Change Password',
      <Entry>[
      ],
    ),
    Entry(
      'Logout',
      <Entry>[
      ],
    ),
  ];

  final List<Entry> jeUserMenus = <Entry>[
    Entry(
      'Dashboard',
      <Entry>[],
    ),
    Entry(
      'Plan V/S Actual',
      <Entry>[
      ],
    ),
    Entry(
      'Audit',
      <Entry>[
        Entry('Plan Audit', []),
        Entry('Audit Status', []),
        Entry('Audit Report', []),
      ],
    ),
    Entry(
      'Bill Generation',
      <Entry>[
      ],
    ),
    Entry(
      'Tolerance Matrix',
      <Entry>[
        Entry('Curve Matrix', []),
        Entry(
          'Turnout Matrix',
          <Entry>[
            Entry('TO 1 in 9R190', []),
            Entry('TO 1 in 9R300', []),
            Entry('TO 1 in 7R140', []),
            Entry('TO 1 in 7R190', []),
            Entry('TO 1 in 5R100', []),
            Entry('TO 1 in 9R300 Extn', []),
            Entry('TRAP 1 in 7R190', []),
            Entry('TS 1 in 7R140', []),
          ],
        ),
      ],
    ),
    //Note: in je user store manin -> store main -> []
    Entry(
      'Store Maintenance',
      <Entry>[
        Entry('Inward Maintenance', []),
        Entry('Request Status', []),
        Entry('Stock Report', []),
        Entry('Reorder Level', []),
        Entry('Balance Products', []),
        Entry('Products Requested', []),
        Entry('Assign Products', []),
      ],
    ),
    Entry(
      'Preventive Maintenance',
      <Entry>[
        Entry('Monthly Inspection', []),
        Entry('Upload Weekly Schedule', []),
        Entry('Preventive Maintenance', []),
        Entry('Defects in PM', []),
        Entry('Miscellaneous Inspection', []),
        Entry('Inspection Matrix', []),
        Entry('Deviation Found', []),
      ],
    ),
    Entry(
      'Corrective Maintenance',
      <Entry>[
        // Entry('Corrective Maintenance', []),
      ],
    ),
    Entry(
      'Change Password',
      <Entry>[
      ],
    ),
    Entry(
      'Logout',
      <Entry>[
      ],
    ),
  ];

  final List<Entry> contractorMenus = <Entry>[
    Entry(
      'Dashboard',
      <Entry>[],
    ),
    Entry(
      'Plan V/S Actual',
      <Entry>[
      ],
    ),
    Entry('Inventory', [
      Entry('Request Products', []),
      Entry('Product Request Status', []),
    ]),
    Entry(
      'Annual Plan',
      <Entry>[
      ],
    ),
    Entry(
      'Preventive Maintenance',
      <Entry>[
        Entry('View Available PTW MOM', []),
        Entry('Weekly Schedule', []),
        Entry('Preventive Maintenance', []),
        Entry('Defects in PM', []),
        Entry('Deviation Found', []),
        Entry('Rescheduled Inspection', []),
        Entry('Allocate Alternate Staff', []),
        Entry('Cancel Inspection', []),
      ],
    ),
    Entry(
      'Corrective Maintenance',
      <Entry>[
        Entry('Corrective Maintenance', []),
        Entry('Add CM inspection', []),
      ],
    ),
    Entry(
      'Change Password',
      <Entry>[
      ],
    ),
    Entry(
      'Logout',
      <Entry>[
      ],
    ),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);
    final urlImage =
        'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';

    return Drawer(
      child: Material(
        color: kPrimaryColor,
        child: ListView(
          children: <Widget>[
            buildHeader(
                name: userProvider.loginModel!.username,
                urlImage: urlImage,
                email: userProvider.loginModel!.userrole,
                onClicked: () {}),
            Divider(color: Colors.white,thickness: 2,),
            Container(
              padding: padding,
              child: Column(
                children: [
                  ListView.separated(
                    separatorBuilder: (context, index) => Divider(color: Colors.white,thickness: 1,),
                    itemCount: userProvider.userData?.userrole == "cmrl-ex" ? exUserMenus.length : userProvider.userData?.userrole == "cmrl-je" ? jeUserMenus.length : contractorMenus.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return _buildTiles(userProvider.userData?.userrole == "cmrl-ex" ? exUserMenus[index] : userProvider.userData?.userrole == "cmrl-je" ? jeUserMenus[index] : contractorMenus[index]);
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTiles(Entry root) {
    if (root.widgets.isEmpty)
      return ListTile(
        onTap: ()
          async {
            if(root.title == "Dashboard")
              {
                Navigator.push(context, PageRouteBuilder(pageBuilder: (context, animation, secondaryAnimation) => HomeScreen(),));
              }
            else if(root.title == "Upload Weekly Schedule" || root.title == "View Available PTW MOM")
              {
                Navigator.push(context, PageRouteBuilder(pageBuilder: (context, animation, secondaryAnimation) => MainUploadScheduleScreen(root.title.toString()),));
              }
            else if(root.title == "Monthly Inspection" || root.title == "Weekly Schedule")
              {
                Navigator.push(context, PageRouteBuilder(pageBuilder: (context, animation, secondaryAnimation) => MainMonthlyInspectionScreen(root.title.toString()),));
              }
            else if(root.title == "Preventive Maintenance")
            {
              Navigator.push(context, PageRouteBuilder(pageBuilder: (context, animation, secondaryAnimation) => MainPreventiveMaintenanceScreen(root.title.toString()),));
            }
            else if(root.title == "Logout")
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp(),));
                UserProvider userProvider = Provider.of<UserProvider>(context,listen: false);
                userProvider.enableLoading();
                await userProvider.getIPAndDeviceVersionAndLocation(context);
                userProvider.logoutApi(context, userProvider.userData!.loginid.toString());
              }
          },
        contentPadding: EdgeInsets.symmetric(vertical: -9),
          title: Text(
        root.title,
        style: TextStyle(color: Colors.white),
      ));
    return ExpansionTile(
      trailing: Icon(
        Icons.arrow_right,
        color: Colors.white,
      ),
      childrenPadding: EdgeInsets.zero,
      tilePadding: EdgeInsets.symmetric(vertical: -9),
      key: PageStorageKey<Entry>(root),
      title: Text(
        root.title,
        style: TextStyle(color: Colors.white),
      ),
      children: root.widgets.map(_buildTiles).toList(),
    );
  }

  Widget buildHeader({
    required String? urlImage,
    required String? name,
    required String? email,
    required VoidCallback? onClicked,
  }) =>
      InkWell(
        onTap: onClicked,
        child: Container(
          padding: padding.add(
              EdgeInsets.symmetric(vertical: 30)),
          child: Row(
            children: [
              CircleAvatar(
                  radius: 35,
                  backgroundImage: AssetImage("assets/images/person.png")),
              SizedBox(width: 15),
              Expanded(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name.toString(),
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                      const SizedBox(height: 4),
                      Text(
                        email.toString(),
                        style: TextStyle(fontSize: 14, color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Widget buildSearchField() {
    final color = Colors.white;

    return TextField(
      style: TextStyle(color: color),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        hintText: 'Search',
        hintStyle: TextStyle(color: color),
        prefixIcon: Icon(Icons.search, color: color),
        filled: true,
        fillColor: Colors.white12,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    required VoidCallback onClicked,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text, style: TextStyle(color: color)),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    Navigator.of(context).pop();

    switch (index) {
      case 0:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => HomeScreen(),
        ));
        break;
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => HomeScreen(),
        ));
        break;
    }
  }
}

class Entry {
  String title;
  List<Entry> widgets;
  Entry(this.title, this.widgets);
}

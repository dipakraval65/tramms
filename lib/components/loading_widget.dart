import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget
{
  final double height;
  final String title;
  const LoadingWidget(this.height,this.title);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset("assets/images/loading1.gif",height: height,),
        Text(title)
      ],
    );
  }

}
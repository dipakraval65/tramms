import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class Common
{
  static void showTopFlash(String title,String msg,BuildContext context,int duration) {
    Flushbar(
      title: "$title",
      message: "$msg",
      backgroundGradient: LinearGradient(colors: [Colors.indigo,kPrimaryColor]),
      backgroundColor: Colors.red,
      duration: Duration(seconds: duration),
      flushbarPosition: FlushbarPosition.TOP,
      boxShadows: [BoxShadow(color: Colors.blue.shade800, offset: Offset(0.0, 2.0), blurRadius: 3.0,)],
    )..show(context);
  }
}
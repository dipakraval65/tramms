import 'dart:async';
import 'dart:io';

import 'package:TRAMMS/components/my_side_drawer.dart';
import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:location/location.dart'as location;
import 'package:provider/provider.dart';

import 'widgets/activity_pie_chart.dart';
import 'widgets/heading_widget.dart';
import 'widgets/hourly_bar_chart_widget.dart';
import 'widgets/weekly_bar_chart_widget.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserProvider userProvider = Provider.of<UserProvider>(context,listen: false);
    userProvider.loadGlobalURL();
    userProvider.loadLoginUserData();
    userProvider.checkLocation(context);
    userProvider.getIPAndDeviceVersionAndLocation(context);
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final List<int> weeklyData = [10, 12, 4, 16, 20, 16, 14];
  final List<int> hourlyData = [
    5,7,10,11,17,18,14,15,12,11,8,7,11,19,6,8,10,12,18,16,14,17,11,13
  ];
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);

    return Scaffold(
      key:_key,
      drawer: NavigationDrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Builder(builder: (context) => // Ensure Scaffold is in context
        IconButton(
            icon: Icon(Icons.menu,size: 30,color: CustomColors.kPrimaryColor,),
            onPressed: () => Scaffold.of(context).openDrawer()
        ),
        ),
        title: Text("DASHBOARD",style: TextStyle(color: CustomColors.kPrimaryColor,fontWeight: FontWeight.w700),),
        centerTitle: true,
        actions: [
          InkWell(
            onTap: ()
            {
            },
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Icon(
                Icons.notifications,
                size: 30,
                color: CustomColors.kPrimaryColor,
              ),
            ),
          )
        ],
      ),
      body: WillPopScope(
        onWillPop: ()
        {
          exit(0);
        },
        child: Container(
          height: SizeConfig.screenHeight,
          width: double.infinity,
          color: CustomColors.kBackgroundColor,
          child: SingleChildScrollView(
            child: Column(
              children: [
                HeadingWidget(text1: 'TRAMMS', text2: '',),
                ActivityPieChart(),
                HeadingWidget(text1: 'GOAL COMPLIANCE', text2: '',),
                WeeklyBarChartWidget(
                  weeklyData: weeklyData,
                  maximumValueOnYAxis: 22,
                ),
                HeadingWidget(text1: 'EXERCISE AVG', text2: '',),
                HourlyBarChartWidget(
                  hourlyData: hourlyData,
                  maximumValueOnYAxis: 22,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void showTopFlash(String msg) {
    Fluttertoast.showToast(msg: msg,toastLength: Toast.LENGTH_LONG);
  }
}

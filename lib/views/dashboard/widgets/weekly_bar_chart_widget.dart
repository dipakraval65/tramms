import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

// Chart to display weekly steps count of user.
class WeeklyBarChartWidget extends StatelessWidget {

  final List<int> weeklyData;
  final double maximumValueOnYAxis;
  WeeklyBarChartWidget(
      {required this.weeklyData, required this.maximumValueOnYAxis});

  // Card containing the bar graph.
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: SizeConfig.screenHeight! * 0.0033,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth! * 0.02),
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
          color: Colors.white,
          elevation: 0.0,
          child: Container(
            margin: EdgeInsets.only(top: 15.0,left: 20),
            child: BarChart(
              mainBarData(),
            ),
          ),
        ),
      ),
    );
  }

  // Function to draw the bar.
  BarChartGroupData makeGroupData(
    int x, // data to show on x - axis
    double y, // value for y axis
    ) {
    return BarChartGroupData(
      x: x,
      /// bar rods is an array and is helpful in case of charts need two bars for same x axis value.
      barRods: [
        BarChartRodData(
          y: y,
          colors: [CustomColors.kCyanColor],
          width: 15.0,
          // Background bar.
          backDrawRodData: BackgroundBarChartRodData(
            show: true,
            colors:[CustomColors.kCyanColor.withOpacity(0.2)],
            y: maximumValueOnYAxis
          ),
        ),
      ],
    );
  }

  // Generate the data responsible for displaying the graph of steps count of user.
  List<BarChartGroupData> showingGroups() {
    return List.generate(weeklyData.length, (index) {
      return makeGroupData(index, weeklyData[index].toDouble());
    });
  }

  /// To build x and y axis for graph.
  FlTitlesData _buildAxes() {
    return FlTitlesData(
      show: true,
      // Build X Axis here.
      bottomTitles: SideTitles(
        showTitles: true,
        margin: 12,
        getTitles: (double value) {
          switch (value.toInt()) {
            case 0:
              return 'Mon';
            case 1:
              return 'Tue';
            case 2:
              return 'Wed';
            case 3:
              return 'Thu';
            case 4:
              return 'Fri';
            case 5:
              return 'Sat';
            case 6:
              return 'Sun';
            default:
              return '';
          }
        },
      ),
      leftTitles: SideTitles(showTitles: false),
    );
  }

  BarChartData mainBarData() {
    return BarChartData(
      maxY: maximumValueOnYAxis,
      alignment: BarChartAlignment.center,
      groupsSpace: 30.0,
      /// To add x-axis and y-axis in the graph we use titlesData.
      titlesData: _buildAxes(),
      borderData: FlBorderData(
        show: false,
      ),
      // Function responsible for data bars.
      barGroups: showingGroups(),
    );
  }
}

import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:flutter/material.dart';

class Indicator extends StatelessWidget {
  final Color color;
  final String title;
  final String subtitle;
  final String iconPath;

  const Indicator({required this.color, required this.title, required this.subtitle, required this.iconPath});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        SizedBox(
          width: SizeConfig.blockSizeHorizontal! * 1,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            Text(
              subtitle,
              style: TextStyle(
                  color: CustomColors.kLightColor, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ],
    );
  }
}

import 'dart:io';

import 'package:TRAMMS/controllers/helpers/database_helper.dart';
import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import '../../controllers/providers/user_provider.dart';
import '../../components/loading_widget.dart';

class ForgotPassPage extends StatefulWidget {
  @override
  _ForgotPassPageState createState() => new _ForgotPassPageState();
}

class _ForgotPassPageState extends State<ForgotPassPage> {
  TextEditingController loginIDCtrl = new TextEditingController(),
      emailCtrl = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);
    String status = "Offline";
    switch (userProvider.source.keys.toList()[0]) {
      case ConnectivityResult.none:
        showTopFlash("You are offline, please On your Mobile Network");
        status = "You are offline, please on your Mobile Network";
        break;
      case ConnectivityResult.mobile:
        status = "You are online, enjoy services";
        break;
      case ConnectivityResult.wifi:
        status = "WiFi: Online";
        break;
    }
    return new Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Container(
        width: SizeConfig.screenWidth,
        height: SizeConfig.screenHeight,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
          color: Colors.white,
        ),
        child: userProvider.loadingAuth!
            ? Center(
          child: LoadingWidget(70.0, "Loading Details"),
        )
            : Stack(
          children: [
            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.screenWidth! * 0.08),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: SizeConfig.screenWidth! * 0.08),
                          child: Image.asset("assets/images/logo.png",
                              height: 100),
                        )),
                    Text(
                      'Forgot Password',
                      style: TextStyle(
                          fontSize: 20,
                          letterSpacing: 0.2,
                          fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                      child: Container(
                        color: Color(0xfff5f5f5),
                        child: TextFormField(
                          controller: loginIDCtrl,
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'SFUIDisplay'),
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Login ID',
                              prefixIcon: Icon(Icons.person_outline),
                              labelStyle: TextStyle(fontSize: 15)),
                        ),
                      ),
                    ),
                    Container(
                      color: Color(0xfff5f5f5),
                      child: TextFormField(
                        controller: emailCtrl,
                        obscureText: false,
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'SFUIDisplay'),
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Email ID',
                            prefixIcon: Icon(Icons.lock_outline),
                            labelStyle: TextStyle(fontSize: 15)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: MaterialButton(
                        onPressed: () async {
                          switch (userProvider.source.keys.toList()[0]) {
                            case ConnectivityResult.none:
                              showTopFlash("You are offline, please On your Mobile Network");
                              status = "You are offline, please on your Mobile Network";
                              break;
                            }

                          Position currentPosition =
                          await userProvider.checkLocation(context);
                          setState(() {});
                          final ipv4 = await Ipify.ipv4();
                          DeviceInfoPlugin deviceInfo =
                          DeviceInfoPlugin();
                          AndroidDeviceInfo androidInfo =
                          await deviceInfo.androidInfo;
                          setState(() {});
                          if (currentPosition.latitude == null ||
                              currentPosition.longitude == null) {
                            userProvider.checkLocationPermission();
                          } else if (ipv4 == null) {
                            Fluttertoast.showToast(
                                msg:
                                "IP Address Not Found\nPlease try again");
                          } else {
                            await userProvider.loadGlobalURL();

                            await userProvider.forgot(
                                context,
                                loginIDCtrl.text.toString(),
                                emailCtrl.text.toString(),
                                ipv4.toString(),
                                "android ${androidInfo.version
                                    .release
                                    .toString()},${userProvider.packageInfo!
                                    .version.toString()}",
                                currentPosition);
                            loginIDCtrl.clear();
                            emailCtrl.clear();
                          }
                        },
                        //since this is only a UI app
                        child: Text(
                          'FORGOT PASSWORD',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'SFUIDisplay',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        color: kPrimaryColor,
                        elevation: 0,
                        minWidth: 400,
                        height: 50,
                        textColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                    const SizedBox(height: 20,),
                    InkWell(
                      onTap: ()
                      {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Back to Login',
                        style: TextStyle(
                            fontSize: 18,
                            letterSpacing: 0.2,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.screenWidth! * 0.2),
                child: Column(
                  children: [
                    Text("Powered By"),
                    SizedBox(
                      height: 01,
                    ),
                    Image.asset("assets/images/ssb_Digital-2.png"),
                    Text(userProvider.packageInfo?.version == null
                        ? ""
                        : "ver:${userProvider.packageInfo!.version.toString()}"),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void showTopFlash(String msg) {
    Fluttertoast.showToast(msg: msg);
  }
}
import 'dart:io';

import 'package:TRAMMS/components/common.dart';
import 'package:TRAMMS/controllers/helpers/database_helper.dart';
import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import '../../controllers/providers/user_provider.dart';
import '../../components/loading_widget.dart';
import 'forgot_screen.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController loginIDCtrl = new TextEditingController(),
      passCtrl = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserProvider userProvider = Provider.of<UserProvider>(
        context, listen: false);
    userProvider.gpsService(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return new Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog<void>(
              context: context,
              barrierDismissible: false,
              // false = user must tap button, true = tap outside dialog
              builder: (BuildContext dialogContext) {
                return StatefulBuilder(
                  builder: (context, setState1) {
                    return Dialog(
                      insetPadding:
                      EdgeInsets.all(SizeConfig.screenWidth! * 0.07),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      elevation: 0,
                      backgroundColor: Colors.transparent,
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 20, top: 40, right: 20, bottom: 20),
                        margin: EdgeInsets.only(top: 30),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black,
                                  offset: Offset(0, 03),
                                  blurRadius: 03),
                            ]),
                        child: SingleChildScrollView(
                          physics: ScrollPhysics(),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 05),
                                  child: Text("URL Configuration"),
                                ),
                              ),
                              Divider(
                                thickness: 02,
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                                child: Container(
                                  height: SizeConfig.screenHeight! * 0.08,
                                  color: Color(0xfff5f5f5),
                                  child: TextFormField(
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                    keyboardType: TextInputType.name,
                                    controller: userProvider.apiURLCtrl,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                            borderRadius:
                                            BorderRadius.circular(10)),
                                        labelText: 'Enter URL',
                                        prefixIcon: Icon(
                                            Icons.drive_file_rename_outline),
                                        labelStyle: TextStyle(fontSize: 15)),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    FlatButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          'Cancel',
                                          style: TextStyle(fontSize: 18),
                                        )),
                                    FlatButton(
                                        onPressed: () async {
                                          Navigator.of(context).pop();
                                          // userProvider.changeGlobalURL(
                                          //     apiURLCtrl.text.toString());
                                          Word word = new Word();
                                          word.word =
                                          '${userProvider.apiURLCtrl.text
                                              .toString()}';
                                          word.frequency = 1;
                                          DatabaseHelper helper =
                                              DatabaseHelper.instance;
                                          int id = await helper.insert(word);
                                          print('inserted row: $id');
                                          userProvider.loadGlobalURL();
                                        },
                                        child: Text(
                                          'Add',
                                          style: TextStyle(fontSize: 18),
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                );
              });
        },
        backgroundColor: Colors.white,
        elevation: 02,
        child: Center(
            child: Icon(Icons.settings,
                size: SizeConfig.screenWidth! * 0.06, color: kPrimaryColor)),
      ),
      body: WillPopScope(
        onWillPop: ()
        {
          exit(0);
        },
        child: Container(
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            color: Colors.white,
          ),
          child: userProvider.loadingAuth!
              ? Center(
            child: LoadingWidget(70.0, "Loading..."),
          )
              : Stack(
            children: [
              Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.screenWidth! * 0.08),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: SizeConfig.screenWidth! * 0.08),
                            child: Image.asset("assets/images/logo.png",
                                height: 100),
                          )),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                        child: Container(
                          color: Color(0xfff5f5f5),
                          child: TextFormField(
                            controller: loginIDCtrl,
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'SFUIDisplay'),
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Employee ID',
                                prefixIcon: Icon(Icons.person_outline),
                                labelStyle: TextStyle(fontSize: 15)),
                          ),
                        ),
                      ),
                      Container(
                        color: Color(0xfff5f5f5),
                        child: TextFormField(
                          controller: passCtrl,
                          obscureText: true,
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'SFUIDisplay'),
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Password',
                              prefixIcon: Icon(Icons.lock_outline),
                              labelStyle: TextStyle(fontSize: 15)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: MaterialButton(
                          onPressed: () async {
                            userProvider.enableLoading();
                            Position currentPosition =
                            await userProvider.checkLocation(context);
                            setState(() {});
                            final ipv4 = await Ipify.ipv4();
                            DeviceInfoPlugin deviceInfo =
                            DeviceInfoPlugin();
                            AndroidDeviceInfo androidInfo =
                            await deviceInfo.androidInfo;
                            setState(() {});
                            if (currentPosition.latitude == null ||
                                currentPosition.longitude == null) {
                              userProvider.checkLocationPermission();
                            } else if (ipv4 == null) {
                              Fluttertoast.showToast(
                                  msg:
                                  "IP Address Not Found\nPlease try again");
                            } else {
                              await userProvider.loadGlobalURL();
                              userProvider.login(
                                  context,
                                  loginIDCtrl.text.toString(),
                                  passCtrl.text.toString(),
                                  ipv4.toString(),
                                  "${androidInfo.version
                                      .toString()},${userProvider.packageInfo!
                                      .version.toString()}",
                                  currentPosition);
                            }
                          },
                          //since this is only a UI app
                          child: Text(
                            'SIGN IN',
                            style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'SFUIDisplay',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          color: kPrimaryColor,
                          elevation: 0,
                          minWidth: 400,
                          height: 50,
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                      ),
                      const SizedBox(height: 20,),
                      InkWell(
                        onTap: ()
                        {
                          Navigator.push(context, PageRouteBuilder(pageBuilder: (context, animation, secondaryAnimation) => ForgotPassPage(),));
                        },
                        child: Text(
                          'Forgot Password',
                          style: TextStyle(
                              fontSize: 18,
                              letterSpacing: 0.2,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.screenWidth! * 0.2),
                  child: Column(
                    children: [
                      Text("Powered By"),
                      SizedBox(
                        height: 01,
                      ),
                      Image.asset("assets/images/ssb_Digital-2.png"),
                      Text(userProvider.packageInfo?.version == null
                          ? ""
                          : "ver:1.0.8"),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


}
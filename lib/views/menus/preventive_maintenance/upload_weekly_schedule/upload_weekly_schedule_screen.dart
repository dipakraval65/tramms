import 'package:TRAMMS/components/textfields_layout_widget.dart';
import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:flutter/material.dart';

class UploadWeeklyScheduleWidget extends StatelessWidget {
  const UploadWeeklyScheduleWidget({
    required this.scheduleFromCtrl,
    required this.scheduleToCtrl,
    required this.voidCallbackFrom,
    required this.voidCallbackTo,
  });

  final TextEditingController scheduleFromCtrl;
  final TextEditingController scheduleToCtrl;
  final VoidCallback voidCallbackFrom;
  final VoidCallback voidCallbackTo;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, dimens) {
        if (dimens.maxWidth <= kMobileBreakpoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: TextFieldMobileWidget(
                scheduleFromCtrl, scheduleToCtrl,voidCallbackFrom,voidCallbackTo),
          );
        } else if (dimens.maxWidth > kMobileBreakpoint &&
            dimens.maxWidth <= kTabletBreakpoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: TextFieldTabletWidget(
                scheduleFromCtrl, scheduleToCtrl,voidCallbackFrom,voidCallbackTo),
          );
        } else if (dimens.maxWidth > kTabletBreakpoint &&
            dimens.maxWidth <= kDesktopBreakPoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: TextFieldTabletWidget(
                scheduleFromCtrl, scheduleToCtrl,voidCallbackFrom,voidCallbackTo),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: TextFieldTabletWidget(
                scheduleFromCtrl, scheduleToCtrl,voidCallbackFrom,voidCallbackTo),
          );
        }
      },
    );
  }
}

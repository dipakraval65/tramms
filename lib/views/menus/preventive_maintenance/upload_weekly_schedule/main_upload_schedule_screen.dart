import 'dart:async';
import 'dart:io';

import 'package:TRAMMS/components/custom_grid_layout.dart';
import 'package:TRAMMS/components/loading_widget.dart';
import 'package:TRAMMS/components/my_side_drawer.dart';
import 'package:TRAMMS/components/textfield_widget.dart';
import 'package:TRAMMS/components/textfields_layout_widget.dart';
import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'upload_weekly_schedule_screen.dart';
import 'view_weekly_schedule_screen.dart';

class MainUploadScheduleScreen extends StatefulWidget {
  final String? title;
  const MainUploadScheduleScreen(this.title);
  @override
  _MainUploadScheduleScreenState createState() =>
      _MainUploadScheduleScreenState();
}

class _MainUploadScheduleScreenState extends State<MainUploadScheduleScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DateTime selectedDateFrom = DateTime.now();
  DateTime selectedDateTo = DateTime.now();
  DateTime selectedSearchMonth = DateTime.now();

  TextEditingController scheduleFromCtrl = new TextEditingController(),
      searchFieldCtrl = new TextEditingController(text: "${DateTime.now().month}-${DateTime.now().year}"),
      scheduleToCtrl = new TextEditingController();

  final PdfViewerController _pdfViewerController = PdfViewerController();

  String selectedFileName = "";
  File selectedFilePath = File("");
  int? selectedIndexOfSearch;
  bool validateTextFieldFrom = false,validateSelectedFile = false;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);

    return DefaultTabController(
      length: userProvider.userData?.userrole == "cmrl-ex" ||
              userProvider.userData?.userrole == "cmrl-je"
          ? 2
          : 1,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: NavigationDrawerWidget(),
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 01,
          bottom: TabBar(
            labelStyle: GoogleFonts.poppins(
              fontWeight: FontWeight.w600,
            ),
            labelColor: kPrimaryColor,
            indicatorColor: kPrimaryColor,
            tabs: userProvider.userData?.userrole == "cmrl-ex" ||
                    userProvider.userData?.userrole == "cmrl-je"
                ? [
                    Tab(
                      text: "Upload Weekly Plan",
                    ),
                    Tab(
                      text: "View Previous Weekly Schedule",
                    ),
                  ]
                : [
                    Tab(
                      text: "View Previous Weekly Schedule",
                    ),
                  ],
          ),
          leading: Builder(
            builder: (context) => // Ensure Scaffold is in context
                IconButton(
                    icon: Icon(
                      Icons.menu,
                      size: 30,
                      color: CustomColors.kPrimaryColor,
                    ),
                    onPressed: () => Scaffold.of(context).openDrawer()),
          ),
          title: Text(
            "${widget.title.toString().toUpperCase()}",
            style: TextStyle(
                color: CustomColors.kPrimaryColor, fontWeight: FontWeight.w700),
          ),
          centerTitle: true,
          actions: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: InkWell(
                onTap: () {
                  print("....");
                  userProvider.uploadOfflineStoredData(context);
                },
                child: Icon(
                  Icons.upload_sharp,
                  size: 30,
                  color: CustomColors.kPrimaryColor,
                ),
              ),
            )
          ],
        ),
        body: userProvider.userData?.userrole == "cmrl-ex" ||
            userProvider.userData?.userrole == "cmrl-je" ? TabBarView(
          children: [
             Container(
              height: SizeConfig.screenHeight,
              width: double.infinity,
              color: CustomColors.kBackgroundColor,
              child: userProvider.offlineLoading ? Center(
                child: LoadingWidget(70.0,"Uploading Offline Data...."),
              ) : SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    UploadWeeklyScheduleWidget(
                      scheduleFromCtrl: scheduleFromCtrl,
                      scheduleToCtrl: scheduleToCtrl,
                      voidCallbackFrom: () {
                        _selectDateFrom(context);
                      },
                      voidCallbackTo: () {
                        _selectDateTo(context);
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Text("Attach PDF Document"),
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: DottedBorder(
                        color: Colors.black,
                        strokeWidth: 1,
                        child: InkWell(
                          onTap: getPdfAndUpload,
                          child: Container(
                            width: double.infinity,
                            height: 300,
                            color: Colors.white,
                            child: Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Icon(Icons.upload_rounded),
                                  const Text("Drag and Drop Here"),
                                  const Text("or"),
                                  SizedBox(
                                    height: 40,
                                    width: SizeConfig.screenWidth! * 0.8,
                                    child: RaisedButton(
                                      onPressed: getPdfAndUpload,
                                      color: kPrimaryColor,
                                      child: Text(
                                        "Choose File",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                      visible: selectedFileName == null ||
                                              selectedFileName.isEmpty
                                          ? false
                                          : true,
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(top: 02),
                                        child: Text("($selectedFileName)"),
                                      ))
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                              "Accepted filetypes is PDF. Max upload size allowed is 64M."),
                        )),
                    const SizedBox(
                      height: 20,
                    ),
                    Visibility(
                      visible: validateTextFieldFrom == false ? false : true,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10,left: 10),
                        child: Row(
                        children: [
                          const Icon(Icons.clear,color: Colors.redAccent,),
                          const SizedBox(width: 05,),
                          const Text("Filed is Empty",style: TextStyle(color: Colors.redAccent),),
                        ],
                    ),
                      ),),
                    Visibility(
                      visible: validateSelectedFile == false ? false : true,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10,left: 10),
                        child: Row(
                          children: [
                            const Icon(Icons.clear,color: Colors.redAccent,),
                            const SizedBox(width: 05,),
                            const Text("Please Select PDF File",style: TextStyle(color: Colors.redAccent),),
                          ],
                        ),
                      ),),
                    userProvider.loadingAuth! ? Center(
                      child: Column(
                        children: [
                          LinearProgressIndicator(
                            color: kPrimaryColor,
                            minHeight: 05,
                          ),
                          Text("Uploading...")
                        ],
                      ),
                    ) : Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: SizedBox(
                          height: 40,
                          width: 300,
                          child: RaisedButton(
                            onPressed: () async {
                              if(scheduleFromCtrl.text.isEmpty || scheduleToCtrl.text.isEmpty)
                                {
                                  setState(() {
                                    validateTextFieldFrom = true;
                                  });
                                }
                              else if(selectedFileName.isEmpty)
                                {
                                  setState(() {
                                    validateSelectedFile = true;
                                  });
                                }
                              else
                                {
                                  print(selectedFilePath.path.split('/').last.toString().toLowerCase().substring(selectedFilePath.path.split('/').last.length - 3));
                                  if(selectedFilePath.path.split('/').last.toString().toLowerCase().substring(selectedFilePath.path.split('/').last.length - 3) == "pdf")
                                    {
                                      setState(() {
                                        validateTextFieldFrom = false;
                                        validateSelectedFile = false;
                                      });
                                      DeviceInfoPlugin deviceInfo =
                                      DeviceInfoPlugin();
                                      AndroidDeviceInfo androidInfo =
                                      await deviceInfo.androidInfo;

                                      await userProvider.weeklyScheduleApi(
                                          context,
                                          userProvider.userData!.loginid.toString(),
                                          "android ${androidInfo.version.release.toString()},${userProvider.packageInfo!.version.toString()}",
                                          "${scheduleFromCtrl.text.toString()}",
                                          "${scheduleToCtrl.text.toString()}","$selectedFileName",
                                          selectedFilePath);
                                      scheduleFromCtrl.clear();
                                      scheduleToCtrl.clear();
                                      selectedFileName = "";
                                      setState(() {
                                      });
                                    }
                                  else
                                    {
                                      Fluttertoast.showToast(msg: "Please Choose Only PDF FIle");
                                    }
                                }
                            },
                            color: kPrimaryColor,
                            child: Text(
                              "Upload",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: SizeConfig.screenHeight,
              width: double.infinity,
              color: CustomColors.kBackgroundColor,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    ViewScheduleWidget(
                      chooseMonthCtrl: searchFieldCtrl,
                      voidCallback: () {
                        if (selectedSearchMonth == null ||
                            selectedSearchMonth.month == "") {
                          Fluttertoast.showToast(
                              msg: "Please Choose Month and Year");
                        } else {
                          if (selectedSearchMonth.month.toInt() <= 9) {
                            userProvider.getWeeklyScheduleApi(
                                context,
                                "${selectedSearchMonth.year}-0${selectedSearchMonth.month}",
                                userProvider.userData!.loginid.toString());
                          } else {
                            userProvider.getWeeklyScheduleApi(
                                context,
                                "${selectedSearchMonth.year}-${selectedSearchMonth.month}",
                                userProvider.userData!.loginid.toString());
                          }
                        }
                      },
                      voidCallbackMonth: () {
                        showMonthPicker(
                          context: context,
                          firstDate: DateTime(DateTime.now().year - 1, 5),
                          lastDate: DateTime(DateTime.now().year + 1, 9),
                          initialDate: selectedSearchMonth,
                        ).then((date) {
                          if (date != null) {
                            setState(() {
                              selectedSearchMonth = date;
                              searchFieldCtrl.text =
                                  "${date.month}-${date.year}";
                            });
                          }
                        });
                      },
                    ),
                    userProvider.loadingAuth! ? LoadingWidget(70.0,"Loading data") : Visibility(
                        visible: userProvider.viewWeeklyPlanList == null ||
                                userProvider.viewWeeklyPlanList.isEmpty
                            ? false
                            : true,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: LayoutBuilder(
                            builder: (context, dimens) {
                              if (dimens.maxWidth <= kMobileBreakpoint) {
                                return CustomGridView(
                                  userProvider: userProvider,
                                  ratio: 12,
                                  pdfViewerController: _pdfViewerController,
                                );
                              } else if (dimens.maxWidth > kMobileBreakpoint &&
                                  dimens.maxWidth <= kTabletBreakpoint) {
                                return CustomGridView(
                                  userProvider: userProvider,
                                  ratio: 6,
                                  pdfViewerController: _pdfViewerController,
                                );
                              } else if (dimens.maxWidth > kTabletBreakpoint &&
                                  dimens.maxWidth <= kDesktopBreakPoint) {
                                return CustomGridView(
                                  userProvider: userProvider,
                                  ratio: 3,
                                  pdfViewerController: _pdfViewerController,
                                );
                              } else {
                                return CustomGridView(
                                  userProvider: userProvider,
                                  ratio: 2,
                                  pdfViewerController: _pdfViewerController,
                                );
                              }
                            },
                          ),
                        )),
                  ],
                ),
              ),
            ),
          ],
        ) : TabBarView(
          children: [
            Container(
              height: SizeConfig.screenHeight,
              width: double.infinity,
              color: CustomColors.kBackgroundColor,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    ViewScheduleWidget(
                      chooseMonthCtrl: searchFieldCtrl,
                      voidCallback: () {
                        if (selectedSearchMonth == null ||
                            selectedSearchMonth.month == "") {
                          Fluttertoast.showToast(
                              msg: "Please Choose Month and Year");
                        } else {
                          if (selectedSearchMonth.month.toInt() <= 9) {
                            userProvider.getWeeklyScheduleApi(
                                context,
                                "${selectedSearchMonth.year}-0${selectedSearchMonth.month}",
                                userProvider.userData!.loginid.toString());
                          } else {
                            userProvider.getWeeklyScheduleApi(
                                context,
                                "${selectedSearchMonth.year}-${selectedSearchMonth.month}",
                                userProvider.userData!.loginid.toString());
                          }
                        }
                      },
                      voidCallbackMonth: () {
                        showMonthPicker(
                          context: context,
                          firstDate: DateTime(DateTime.now().year - 1, 5),
                          lastDate: DateTime(DateTime.now().year + 1, 9),
                          initialDate: selectedSearchMonth,
                        ).then((date) {
                          if (date != null) {
                            setState(() {
                              selectedSearchMonth = date;
                              searchFieldCtrl.text =
                              "${date.month}-${date.year}";
                            });
                          }
                        });
                      },
                    ),
                    Visibility(
                        visible: userProvider.viewWeeklyPlanList == null ||
                            userProvider.viewWeeklyPlanList.isEmpty
                            ? false
                            : true,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: LayoutBuilder(
                            builder: (context, dimens) {
                              if (dimens.maxWidth <= kMobileBreakpoint) {
                                return CustomGridView(
                                  userProvider: userProvider,
                                  ratio: 12,
                                  pdfViewerController: _pdfViewerController,
                                );
                              } else if (dimens.maxWidth > kMobileBreakpoint &&
                                  dimens.maxWidth <= kTabletBreakpoint) {
                                return CustomGridView(
                                  userProvider: userProvider,
                                  ratio: 6,
                                  pdfViewerController: _pdfViewerController,
                                );
                              } else if (dimens.maxWidth > kTabletBreakpoint &&
                                  dimens.maxWidth <= kDesktopBreakPoint) {
                                return CustomGridView(
                                  userProvider: userProvider,
                                  ratio: 3,
                                  pdfViewerController: _pdfViewerController,
                                );
                              } else {
                                return CustomGridView(
                                  userProvider: userProvider,
                                  ratio: 2,
                                  pdfViewerController: _pdfViewerController,
                                );
                              }
                            },
                          ),
                        )),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _selectDateFrom(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDateFrom,
        builder: (BuildContext? context, Widget? child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme:
                  ColorScheme.light(primary: CustomColors.kPrimaryColor),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child!,
          );
        },
        firstDate: DateTime.now().subtract(Duration(days: 3)),
        lastDate: DateTime.now().add(Duration(days: 10)));
    if (picked != null && picked != selectedDateFrom)
      setState(() {
        selectedDateFrom = picked;
        scheduleFromCtrl.text =
            "${"${selectedDateFrom.toLocal()}".split(' ')[0]}";
      });
  }

  Future<void> _selectDateTo(BuildContext context) async {
    print("to........");
    final DateTime? picked = await showDatePicker(
        context: context,
        builder: (BuildContext context, Widget? child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme:
                  ColorScheme.light(primary: CustomColors.kPrimaryColor),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child!,
          );
        },
        firstDate: selectedDateTo,
        initialDate: DateTime.now(),
        lastDate: DateTime.now().add(Duration(days: 18)));
    if (picked != null && picked != selectedDateTo)
      setState(() {
        selectedDateTo = picked;
        scheduleToCtrl.text = "${"${selectedDateTo.toLocal()}".split(' ')[0]}";
      });
    print(selectedDateTo);
  }

  void showTopFlash(String msg) {
    Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_LONG);
  }

  Future getPdfAndUpload() async {
    FilePickerResult? file = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: ['pdf']);
    if (file != null) {
      PlatformFile file1 = file.files.first;
      File fileDor = File(file.files.single.path!);
      setState(() {
        selectedFileName = file1.name;
        selectedFilePath = fileDor;
      });
      print(file1.name);
      print(file1.bytes);
      print(file1.size);
      print(file1.extension);
      print(file1.path);
      print("File Path is:" + file1.toString());
      return file1.name;
    }
  }
}

import 'package:TRAMMS/components/textfield_widget.dart';
import 'package:TRAMMS/components/textfields_layout_widget.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:flutter/material.dart';

class ViewScheduleWidget extends StatelessWidget {
  const ViewScheduleWidget({
    required this.chooseMonthCtrl,
    required this.voidCallback,
    required this.voidCallbackMonth,
  });

  final TextEditingController chooseMonthCtrl;
  final VoidCallback voidCallback;
  final VoidCallback voidCallbackMonth;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, dimens) {
        if (dimens.maxWidth <= kMobileBreakpoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: voidCallbackMonth,
                    child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",)),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Search",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        } else if (dimens.maxWidth > kMobileBreakpoint &&
            dimens.maxWidth <= kTabletBreakpoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: voidCallbackMonth,
                    child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",)),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Search",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        } else if (dimens.maxWidth > kTabletBreakpoint &&
            dimens.maxWidth <= kDesktopBreakPoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: voidCallbackMonth,
                    child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",)),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Search",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: voidCallbackMonth,
                    child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",)),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Search",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        }
      },
    );
  }
}

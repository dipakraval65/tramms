import 'dart:async';
import 'dart:io';

import 'package:TRAMMS/components/custom_grid_layout.dart';
import 'package:TRAMMS/components/loading_widget.dart';
import 'package:TRAMMS/components/my_side_drawer.dart';
import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import 'components/monthly_inspection_widget.dart';

class MainMonthlyInspectionScreen extends StatefulWidget {
  final String? title;
  const MainMonthlyInspectionScreen(this.title);
  @override
  _MainMonthlyInspectionScreenState createState() =>
      _MainMonthlyInspectionScreenState();
}

class _MainMonthlyInspectionScreenState
    extends State<MainMonthlyInspectionScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DateTime selectedDateFrom = DateTime.now();
  DateTime selectedDateTo = DateTime.now();

  TextEditingController scheduleFromCtrl = new TextEditingController(),
      searchFieldCtrl = new TextEditingController(
          text: "${DateTime.now().month}-${DateTime.now().year}"),
      scheduleToCtrl = new TextEditingController();

  final PdfViewerController _pdfViewerController = PdfViewerController();

  String selectedFileName = "";
  File? selectedFilePath;
  int? selectedIndexOfSearch;
  bool validateTextFieldFrom = false, validateSelectedFile = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserProvider userProvider = Provider.of<UserProvider>(context,listen: false);
    userProvider.getAllInspectionListApi(context);
    userProvider.getAllocateStaffUsersListApi(context);
  }
  
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);

    return Scaffold(
      key: _scaffoldKey,
      drawer: NavigationDrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 01,
        leading: Builder(
          builder: (context) => // Ensure Scaffold is in context
              IconButton(
                  icon: Icon(
                    Icons.menu,
                    size: 30,
                    color: CustomColors.kPrimaryColor,
                  ),
                  onPressed: () => Scaffold.of(context).openDrawer()),
        ),
        title: Text(
          "${widget.title.toString().toUpperCase()}",
          style: TextStyle(
              color: CustomColors.kPrimaryColor, fontWeight: FontWeight.w700),overflow: TextOverflow.ellipsis,
        ),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: InkWell(
              onTap: () {
              },
              child: Icon(
                Icons.filter_alt_outlined,
                size: 30,
                color: CustomColors.kPrimaryColor,
              ),
            ),
          )
        ],
      ),
      body: Container(
        height: SizeConfig.screenHeight,
        width: double.infinity,
        color: CustomColors.kBackgroundColor,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              MonthlyInspectionWidget(
                chooseMonthCtrl: searchFieldCtrl,
                voidCallback: () {
                  if (userProvider.selectedSearchInspectionMonth == null ||
                      userProvider.selectedSearchInspectionMonth.month == "") {
                    Fluttertoast.showToast(msg: "Please Choose Month and Year");
                  } else {
                    if (userProvider.selectedSearchInspectionMonth.month.toInt() <= 9) {
                      userProvider.getMonthlyInspectionListApi(
                          context,"${userProvider.selectedInspection}",
                          "${userProvider.selectedSearchInspectionMonth.year}-0${userProvider.selectedSearchInspectionMonth.month}");
                    } else {
                      userProvider.getMonthlyInspectionListApi(
                          context,"${userProvider.selectedInspection}",
                          "${userProvider.selectedSearchInspectionMonth.year}-${userProvider.selectedSearchInspectionMonth.month}");
                    }
                  }
                },
                voidCallbackMonth: () {
                  showMonthPicker(
                    context: context,
                    firstDate: DateTime(DateTime.now().year - 1, 5),
                    lastDate: DateTime(DateTime.now().year + 1, 9),
                    initialDate: userProvider.selectedSearchInspectionMonth,
                  ).then((date) {
                    if (date != null) {
                      setState(() {
                        userProvider.changeInspectionMonthYear(date);
                        searchFieldCtrl.text = "${date.month}-${date.year}";
                      });
                    }
                  });
                },
              ),
              userProvider.loadingAuth!
                  ? LoadingWidget(70.0, "Loading data")
                  : Visibility(
                      visible: userProvider.viewMonthlyInspectionList == null ||
                              userProvider.viewMonthlyInspectionList.isEmpty
                          ? false
                          : true,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: LayoutBuilder(
                          builder: (context, dimens) {
                            if (dimens.maxWidth <= kMobileBreakpoint) {
                              return CustomGridViewMonthlyInspection(
                                userProvider: userProvider,
                                ratio: 12,
                              );
                            } else if (dimens.maxWidth > kMobileBreakpoint &&
                                dimens.maxWidth <= kTabletBreakpoint) {
                              return CustomGridViewMonthlyInspection(
                                userProvider: userProvider,
                                ratio: 6,
                              );
                            } else if (dimens.maxWidth > kTabletBreakpoint &&
                                dimens.maxWidth <= kDesktopBreakPoint) {
                              return CustomGridViewMonthlyInspection(
                                userProvider: userProvider,
                                ratio: 3,
                              );
                            } else {
                              return CustomGridView(
                                userProvider: userProvider,
                                ratio: 2,
                                pdfViewerController: _pdfViewerController,
                              );
                            }
                          },
                        ),
                      )),
            ],
          ),
        ),
      ),
    );
  }
  
}

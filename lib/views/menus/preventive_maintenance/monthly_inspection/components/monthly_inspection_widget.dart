import 'package:TRAMMS/components/textfield_widget.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MonthlyInspectionWidget extends StatelessWidget {
  const MonthlyInspectionWidget({
    required this.chooseMonthCtrl,
    required this.voidCallback,
    required this.voidCallbackMonth,
  });

  final TextEditingController chooseMonthCtrl;
  final VoidCallback voidCallback;
  final VoidCallback voidCallbackMonth;

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          InkWell(
              onTap: voidCallbackMonth,
              child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",)),
          const SizedBox(height: 20,),
          Container(
            height: 63,
            color: Colors.white,
            child: FormField<String>(
              builder: (FormFieldState<String>state) {
                return InputDecorator(
                  decoration: InputDecoration(
                    labelText:
                    "Select Inspection",
                    labelStyle: TextStyle(
                        fontWeight:
                        FontWeight.bold,
                        color: Colors.black38),
                    errorStyle: TextStyle(
                        color: Colors.redAccent,
                        fontSize: 14.0),
                    hintText: 'Select Inspection',
                    border: OutlineInputBorder(
                        borderRadius:
                        BorderRadius
                            .circular(0)),
                  ),
                  child:
                  DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      hint: Text("Select Inspection"),
                      value: userProvider.selectedInspection,
                      isDense: true,
                      onChanged:
                          (String? newValue) async{
                        userProvider.changeInspection(newValue!,context,chooseMonthCtrl.text.toString().trim());
                      },
                      items: userProvider.viewAllInspectionList
                          .map((item) {
                        return new DropdownMenuItem(
                          child: new Text(
                              "${item['inspectionname']}"),
                          value: item['inspectionmasterid']
                              .toString(),
                        );
                      }).toList(),
                    ),
                  ),
                );
              },
            ),
          ),
          const SizedBox(height: 20,),
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: SizedBox(
                height: 40,
                width: 300,
                child: RaisedButton(
                  onPressed: voidCallback,
                  color: kPrimaryColor,
                  child: Text("Search",style: TextStyle(color: Colors.white),),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../../../../components/custom_grid_layout.dart';
import '../../../../../components/my_side_drawer.dart';
import '../../../../../components/textfield_widget.dart';
import '../../../../../controllers/helpers/ui_constants.dart';
import '../../../../../controllers/providers/user_provider.dart';

class PMUpdateCheckList extends StatefulWidget {
  final String? cardId;
  const PMUpdateCheckList({this.cardId});
  @override
  _PMUpdateCheckListState createState() => _PMUpdateCheckListState();
}

class _PMUpdateCheckListState extends State<PMUpdateCheckList> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController date = TextEditingController(text: "${DateFormat('dd-MM-yyyy').format(DateTime.now())}");
  TextEditingController other = TextEditingController();

  @override
  void initState() {
    super.initState();
    UserProvider userProvider = Provider.of<UserProvider>(context, listen: false);
    userProvider.clearCheckListImagePickerList();
  }

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return Scaffold(
      key: _scaffoldKey,
      drawer: NavigationDrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 01,
        leading: Builder(
          builder: (context) => // Ensure Scaffold is in context
              IconButton(
                  icon: Icon(
                    Icons.menu,
                    size: 30,
                    color: CustomColors.kPrimaryColor,
                  ),
                  onPressed: () => Scaffold.of(context).openDrawer()),
        ),
        title: Text(
          "Bufferstop Inspection",
          style: TextStyle(
              color: CustomColors.kPrimaryColor, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: InkWell(
              onTap: () {},
              child: Icon(
                Icons.filter_alt_outlined,
                size: 30,
                color: CustomColors.kPrimaryColor,
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: ListView(
          children: [
            SizedBox(height: 10),
            Align(
              alignment: Alignment.topLeft,
              child: Text("Bufferstop Inspection - ${widget.cardId}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
            ),
            SizedBox(height: 10),
            Expanded(
                child: TextFieldWidgetEnabledReadOnly(
                    readOnly: true,
                    controller: date,
                    iconData: Icons.date_range,
                    hint: "Date")),
            SizedBox(height: 10),
            Container(
              height: 63,
              color: Colors.white,
              child: FormField<String>(
                builder: (FormFieldState<String> state) {
                  return InputDecorator(
                    decoration: InputDecoration(
                      labelText: "Select Corridor",
                      labelStyle: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black38),
                      errorStyle:
                          TextStyle(color: Colors.redAccent, fontSize: 14.0),
                      hintText: 'Select Corridor',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        hint: Text("Select Corridor"),
                        value: userProvider.selectedBICorridor,
                        isDense: true,
                        onChanged: (String? newValue) async {
                          userProvider.changeBICorridor(newValue!, context);
                        },
                        items: userProvider.viewBICorridorList.map((item) {
                          return new DropdownMenuItem(
                            child: new Text("${item['corridorname']}"),
                            value: item['corridormasterid'].toString(),
                          );
                        }).toList(),
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 10),
            userProvider.loadingAuth == true
                ? LinearProgressIndicator()
                : Container(
                    height: 63,
                    color: Colors.white,
                    child: FormField<String>(
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            labelText: "Select Station Code",
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black38),
                            errorStyle: TextStyle(
                                color: Colors.redAccent, fontSize: 14.0),
                            hintText: 'Select Station Code',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              hint: Text("Select Station Code"),
                              value: userProvider.selectedBIStationCode,
                              isDense: true,
                              onChanged: (String? newValue) async {
                                await userProvider
                                    .changeBIStationCode(newValue!);
                              },
                              items: userProvider.viewBIStationCodeList
                                  .map((item) {
                                return new DropdownMenuItem(
                                  child: new Text("${item['stationname']}"),
                                  value: item['stationmasterid'].toString(),
                                );
                              }).toList(),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
            SizedBox(height: 10),
            Expanded(
                child: TextFieldWidgetEnabledReadOnly(
                    readOnly: true,
                    controller: userProvider.chainage,
                    iconData: Icons.date_range,
                    hint: "Chainage")),
            SizedBox(height: 10),
            Container(
              height: 63,
              color: Colors.white,
              child: FormField<String>(
                builder: (FormFieldState<String> state) {
                  return InputDecorator(
                    decoration: InputDecoration(
                      labelText: "Select Line",
                      labelStyle: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black38),
                      errorStyle:
                          TextStyle(color: Colors.redAccent, fontSize: 14.0),
                      hintText: 'Select Line',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        hint: Text("Select Line"),
                        value: userProvider.selectedVILine,
                        isDense: true,
                        onChanged: (String? newValue) async {
                          userProvider.changeBILine(newValue!);
                        },
                        items: userProvider.viewBILineList.map((item) {
                          return new DropdownMenuItem(
                            child: new Text("${item['linename']}"),
                            value: item['linemasterid'].toString(),
                          );
                        }).toList(),
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 10),
            Container(
              height: 63,
              color: Colors.white,
              child: FormField<String>(
                builder: (FormFieldState<String> state) {
                  return InputDecorator(
                    decoration: InputDecoration(
                      labelText: "Select Type Of Buffer",
                      labelStyle: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black38),
                      errorStyle:
                          TextStyle(color: Colors.redAccent, fontSize: 14.0),
                      hintText: 'Select Type Of Buffer',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0)),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        hint: Text("Type Of Buffer"),
                        value: userProvider.selectedTypeBufferstopItem,
                        isDense: true,
                        onChanged: (String? newValue) async {
                          userProvider.changeTypeBufferstopItems(newValue!);
                        },
                        items: userProvider.changeTypeBufferstopList
                            .map((String items) {
                          return DropdownMenuItem(
                            value: items,
                            child: Text(items),
                          );
                        }).toList(),
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 10),
            Container(
              color: Colors.white,
              child: TextFormField(
                controller: other,
                obscureText: false,
                style: TextStyle(
                  color: Colors.black,
                ),
                enabled: true,
                decoration: InputDecoration(
                    hintText: "Any other special features/defects/ Instruction",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(01)),
                    labelStyle: TextStyle(fontSize: 15)),
              ),
            ),
            SizedBox(height: 5),
            LayoutBuilder(
              builder: (context, dimens) {
                if (dimens.maxWidth <= kMobileBreakpoint) {
                  return CustomGridViewPMBufferstopInspection(
                    userProvider: userProvider,
                    ratio: 12,
                  );
                } else if (dimens.maxWidth > kMobileBreakpoint &&
                    dimens.maxWidth <= kTabletBreakpoint) {
                  return CustomGridViewPMBufferstopInspection(
                    userProvider: userProvider,
                    ratio: 6,
                  );
                } else if (dimens.maxWidth > kTabletBreakpoint &&
                    dimens.maxWidth <= kDesktopBreakPoint) {
                  return CustomGridViewPMBufferstopInspection(
                    userProvider: userProvider,
                    ratio: 3,
                  );
                } else {
                  return CustomGridViewPMBufferstopInspection(
                    userProvider: userProvider,
                    ratio: 3,
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

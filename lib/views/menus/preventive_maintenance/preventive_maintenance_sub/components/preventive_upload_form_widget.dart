import 'package:TRAMMS/components/textfield_widget.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PreventiveUploadWidget extends StatelessWidget {
  const PreventiveUploadWidget({
    required this.chooseMonthCtrl,
    required this.voidCallback,
    required this.voidCallbackMonth,
    required this.jobCardCtrl,
  });

  final TextEditingController chooseMonthCtrl;
  final TextEditingController jobCardCtrl;
  final VoidCallback voidCallback;
  final VoidCallback voidCallbackMonth;

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return LayoutBuilder(
      builder: (context, dimens) {
        if (dimens.maxWidth <= kMobileBreakpoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: voidCallbackMonth,
                    child: Expanded(child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",))),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Inspection",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Inspection',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Inspection"),
                            value:
                            userProvider.selectedInspection,
                            isDense: true,
                            onChanged:
                                (String? newValue) async{
                              userProvider.changeInspection(newValue!,context,chooseMonthCtrl.text.toString().trim());
                            },
                            items: userProvider.viewAllInspectionList
                                .map((item) {
                              return new DropdownMenuItem(
                                child: new Text(
                                    "${item['inspectionname']}"),
                                value: item['inspectionmasterid']
                                    .toString(),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Level of Inspection",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Level of Inspection',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Level of Inspection"),
                            value: userProvider.selectedPreventLevelItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveLevelItems(newValue!);
                            },
                            items: userProvider.preventLevelItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Corridor",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Corridor',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Corridor"),
                            value:
                            userProvider.selectedCorridor,
                            isDense: true,
                            onChanged:
                                (String? newValue) async{
                              userProvider.changeCorridor(newValue!,context,chooseMonthCtrl.text.toString().trim());
                            },
                            items: userProvider.viewAllCorridorList
                                .map((item) {
                              return new DropdownMenuItem(
                                child: new Text(
                                    "${item['corridorname']}"),
                                value: item['corridormasterid']
                                    .toString(),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Type",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Type',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Type"),
                            value: userProvider.selectedPreventTypeItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveTypeItems(newValue!);
                            },
                            items: userProvider.preventTypeItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                TextFieldWidgetEnabled(controller: jobCardCtrl,iconData: Icons.date_range,hint: "Value"),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Submit",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20,),
              ],
            ),
          );
        } else if (dimens.maxWidth > kMobileBreakpoint &&
            dimens.maxWidth <= kTabletBreakpoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: voidCallbackMonth,
                    child: Expanded(child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",))),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Inspection",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Inspection',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Inspection"),
                            value:
                            userProvider.selectedInspection,
                            isDense: true,
                            onChanged:
                                (String? newValue) async{
                              userProvider.changeInspection(newValue!,context,chooseMonthCtrl.text.toString().trim());
                            },
                            items: userProvider.viewAllInspectionList
                                .map((item) {
                              return new DropdownMenuItem(
                                child: new Text(
                                    "${item['inspectionname']}"),
                                value: item['inspectionmasterid']
                                    .toString(),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Level of Inspection",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Level of Inspection',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Level of Inspection"),
                            value: userProvider.selectedPreventLevelItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveLevelItems(newValue!);
                            },
                            items: userProvider.preventLevelItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Corridor",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Corridor',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Corridor"),
                            value:
                            userProvider.selectedCorridor,
                            isDense: true,
                            onChanged:
                                (String? newValue) async{
                              userProvider.changeCorridor(newValue!,context,chooseMonthCtrl.text.toString().trim());
                            },
                            items: userProvider.viewAllCorridorList
                                .map((item) {
                              return new DropdownMenuItem(
                                child: new Text(
                                    "${item['corridorname']}"),
                                value: item['corridormasterid']
                                    .toString(),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Type",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Type',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Type"),
                            value: userProvider.selectedPreventTypeItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveTypeItems(newValue!);
                            },
                            items: userProvider.preventTypeItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                TextFieldWidgetEnabled(controller: jobCardCtrl,iconData: Icons.date_range,hint: "Value"),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Submit",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        } else if (dimens.maxWidth > kTabletBreakpoint &&
            dimens.maxWidth <= kDesktopBreakPoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: voidCallbackMonth,
                    child: Expanded(child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",))),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Inspection",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Inspection',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Inspection"),
                            value:
                            userProvider.selectedInspection,
                            isDense: true,
                            onChanged:
                                (String? newValue) async{
                              userProvider.changeInspection(newValue!,context,chooseMonthCtrl.text.toString().trim());
                            },
                            items: userProvider.viewAllInspectionList
                                .map((item) {
                              return new DropdownMenuItem(
                                child: new Text(
                                    "${item['inspectionname']}"),
                                value: item['inspectionmasterid']
                                    .toString(),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Level of Inspection",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Level of Inspection',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Level of Inspection"),
                            value: userProvider.selectedPreventLevelItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveLevelItems(newValue!);
                            },
                            items: userProvider.preventLevelItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Corridor",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Corridor',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Corridor"),
                            value:
                            userProvider.selectedCorridor,
                            isDense: true,
                            onChanged:
                                (String? newValue) async{
                              userProvider.changeCorridor(newValue!,context,chooseMonthCtrl.text.toString().trim());
                            },
                            items: userProvider.viewAllCorridorList
                                .map((item) {
                              return new DropdownMenuItem(
                                child: new Text(
                                    "${item['corridorname']}"),
                                value: item['corridormasterid']
                                    .toString(),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Type",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Type',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Type"),
                            value: userProvider.selectedPreventTypeItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveTypeItems(newValue!);
                            },
                            items: userProvider.preventTypeItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                TextFieldWidgetEnabled(controller: jobCardCtrl,iconData: Icons.date_range,hint: "Value"),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Submit",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: voidCallbackMonth,
                    child: Expanded(child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",))),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Inspection",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Inspection',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Inspection"),
                            value:
                            userProvider.selectedInspection,
                            isDense: true,
                            onChanged:
                                (String? newValue) async{
                              userProvider.changeInspection(newValue!,context,chooseMonthCtrl.text.toString().trim());
                            },
                            items: userProvider.viewAllInspectionList
                                .map((item) {
                              return new DropdownMenuItem(
                                child: new Text(
                                    "${item['inspectionname']}"),
                                value: item['inspectionmasterid']
                                    .toString(),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Level of Inspection",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Level of Inspection',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Level of Inspection"),
                            value: userProvider.selectedPreventLevelItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveLevelItems(newValue!);
                            },
                            items: userProvider.preventLevelItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Corridor",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Corridor',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Corridor"),
                            value:
                            userProvider.selectedCorridor,
                            isDense: true,
                            onChanged:
                                (String? newValue) async{
                              userProvider.changeCorridor(newValue!,context,chooseMonthCtrl.text.toString().trim());
                            },
                            items: userProvider.viewAllCorridorList
                                .map((item) {
                              return new DropdownMenuItem(
                                child: new Text(
                                    "${item['corridorname']}"),
                                value: item['corridormasterid']
                                    .toString(),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Type",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Type',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Type"),
                            value: userProvider.selectedPreventTypeItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveTypeItems(newValue!);
                            },
                            items: userProvider.preventTypeItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                TextFieldWidgetEnabled(controller: jobCardCtrl,iconData: Icons.date_range,hint: "Value"),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Submit",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        }
      },
    );
  }
}
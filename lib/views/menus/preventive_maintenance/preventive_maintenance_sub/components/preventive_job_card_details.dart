import 'dart:ui';
import 'package:TRAMMS/views/menus/preventive_maintenance/preventive_maintenance_sub/components/preventive_update_checklist.dart';
import 'package:timelines/timelines.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../../../components/custom_dialog_box.dart';
import '../../../../../components/my_side_drawer.dart';
import '../../../../../controllers/helpers/ui_constants.dart';
import '../../../../../controllers/providers/user_provider.dart';

class JobCardDetails extends StatefulWidget {
  final int index;
  final bool isButton;
  final String? cardId,inspection,inspectionId,status,corridor,inspectionScheduled,inspectionScheduledOn,allocatedStaff,inspectionPlan;
  const JobCardDetails({required this.index,required this.isButton,this.cardId,this.inspection,this.inspectionId,this.status,this.corridor,this.inspectionScheduled,this.inspectionScheduledOn,this.allocatedStaff,this.inspectionPlan});
  @override
  _JobCardDetailsState createState() =>
      _JobCardDetailsState(cardId_:cardId,inspection_: inspection,inspectionId_: inspectionId,status_: status,corridor_: corridor,inspectionPlan_: inspectionPlan,allocatedStaff_: allocatedStaff,inspectionScheduled_: inspectionScheduled,inspectionScheduledOn_: inspectionScheduledOn);
}

class _JobCardDetailsState extends State<JobCardDetails> {
  final String? cardId_,inspection_,inspectionId_,status_,corridor_,inspectionScheduled_,inspectionScheduledOn_,allocatedStaff_,inspectionPlan_;
  _JobCardDetailsState({this.cardId_,this.inspection_,this.inspectionId_,this.status_,this.corridor_,this.inspectionScheduled_,this.inspectionScheduledOn_,this.allocatedStaff_,this.inspectionPlan_});

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return Scaffold(
      key: _scaffoldKey,
      drawer: NavigationDrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 01,
        leading: Builder(
          builder: (context) => // Ensure Scaffold is in context
          IconButton(
              icon: Icon(
                Icons.menu,
                size: 30,
                color: CustomColors.kPrimaryColor,
              ),
              onPressed: () => Scaffold.of(context).openDrawer()),
        ),
        title: Text(
          "Preventive Maintenance",
          style: TextStyle(
              color: CustomColors.kPrimaryColor, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: InkWell(
              onTap: () {},
              child: Icon(
                Icons.filter_alt_outlined,
                size: 30,
                color: CustomColors.kPrimaryColor,
              ),
            ),
          )
        ],
      ),
      body:  Padding(
        padding: EdgeInsets.symmetric(horizontal: 05),
        child: ListView(
          children: [
            SizedBox(height: 10),
            Align(
              alignment: Alignment.topLeft,
              child: Text("\t\tTimeline for Job CardID: ${cardId_}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18)),
            ),
            Container(
              decoration: BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.circular(4),
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 2),
                        blurRadius: 6)
                  ]),
              margin: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
              child: Container(
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 07, vertical: 03),
                      child: Column(
                        children: [
                          SizedBox(height: 8),
                          Text(
                            "Job Card Details: ",
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),
                          ),
                        ],
                      )
                    ),
                    Divider(thickness: 3),
                    widget.isButton==true && status_=="inprogress" ? RichTextWidget(title: "Inspected By:",value: "Qubeta-c(660066)LM") : Center(),
                    RichTextWidget(title: "Inspection Month, Year:",value: "${inspection_}"),
                    RichTextWidget(title: "Inspection ID:",value: "${inspectionId_}"),
                    RichTextWidget(title: "Current Status:",value: "${status_}"),
                    RichTextWidget(title: "Corridor Name:",value: "${corridor_}"),
                    RichTextWidget(title: "Inspection Scheduled By:",value: "${inspectionScheduled_}"),
                    RichTextWidget(title: "Inspection Scheduled On:",value: "${inspectionScheduledOn_}"),
                    RichTextWidget(title: "Allocated Staff:",value: "${allocatedStaff_}"),
                    RichTextWidget(title: "Date Of Inspection Planned:",value: "${inspectionPlan_}"),
                    SizedBox(height: 10)
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(4),
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 2),
                        blurRadius: 6)
                  ]
                ),
                margin: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
                child: widget.isButton==true && status_=="inprogress" ?
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: Center(
                    child: Column(
                      children: [
                        RaisedButton(
                          color: Colors.blue,
                          onPressed: (){
                            Navigator.push(context, PageRouteBuilder(pageBuilder: (context, animation, secondaryAnimation) => PMUpdateCheckList(cardId:cardId_)));
                          },
                          child: Container(
                            alignment: Alignment.center,
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.edit,color: Colors.white),
                                  Text("\t\tUpdate Checklist",style: TextStyle(color: Colors.white)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 5),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text("\t\tRemarks"),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          color: Colors.white,
                          child: TextFormField(
                            obscureText: false,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                            enabled: true,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(01)
                                ),
                                labelStyle: TextStyle(
                                    fontSize: 15
                                )
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text("\t\tDefects Attention Req."),
                        ),
                        SizedBox(height: 15),
                        Container(
                          height: 63,
                          color: Colors.white,
                          child: FormField<String>(
                            builder: (FormFieldState<String>
                            state) {
                              return InputDecorator(
                                decoration: InputDecoration(
                                  labelText:
                                  "Select Defects Attention",
                                  labelStyle: TextStyle(
                                      fontWeight:
                                      FontWeight.bold,
                                      color: Colors.black38),
                                  errorStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontSize: 14.0),
                                  hintText: 'Select Defects Attention',
                                  border: OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius
                                          .circular(0)),
                                ),
                                child:
                                DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                    value: userProvider.selectedDefectsItem,
                                    isDense: true,
                                    onChanged: (String? newValue) async{
                                      userProvider.changeDefectsItems(newValue!);
                                    },
                                    items: userProvider.changeDefectsList.map((String items) {
                                      return DropdownMenuItem(
                                        value: items,
                                        child: Text(items),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                        userProvider.selectedDefectsItem=="Feasible" || userProvider.selectedDefectsItem=="Not Feasible" ?
                        Column(
                          children: [
                            const SizedBox(height: 10),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text("\t\tDefects Noticed"),
                            ),
                            const SizedBox(height: 15),
                            Container(
                              height: 63,
                              color: Colors.white,
                              child: FormField<String>(
                                builder: (FormFieldState<String>
                                state) {
                                  return InputDecorator(
                                    decoration: InputDecoration(
                                      labelText:
                                      "Select Defects Noticed",
                                      labelStyle: TextStyle(
                                          fontWeight:
                                          FontWeight.bold,
                                          color: Colors.black38),
                                      errorStyle: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 14.0),
                                      hintText: 'Select Defects Noticed',
                                      border: OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius
                                              .circular(0)),
                                    ),
                                    child:
                                    DropdownButtonHideUnderline(
                                      child:
                                      DropdownButton<String>(
                                        value: userProvider.selectedDefectsItem,
                                        isDense: true,
                                        onChanged: (String? newValue) async{
                                          userProvider.changeDefectsItems(newValue!);
                                        },
                                        items: userProvider.changeDefectsList.map((String items) {
                                          return DropdownMenuItem(
                                            value: items,
                                            child: Text(items),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ) :
                        const SizedBox(height: 10),
                        Align(
                          alignment: Alignment.bottomRight,
                          child:Padding(
                            padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                            child: RaisedButton(
                              color: kPrimaryColor,
                              onPressed: (){
                                Navigator.of(context).pop();
                              },
                              child: Text("Submit",style: TextStyle(fontSize: 18,color: Colors.white)),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                      ],
                    ),
                  ),
                ) :
                widget.isButton==false && status_=="open" ?
                TimelineTile(
                  oppositeContents: SizedBox(
                      width: MediaQuery.of(context).size.width / 2,
                      child: Card(
                          elevation: 3,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("JOB CARD STATUS: ${status_}".toUpperCase(),style: TextStyle(fontSize: 20)),
                                Divider(thickness: 3,color: Colors.red),
                                RichTextWidget(title: "Scheduled By:",value: "${inspectionScheduled_}"),
                                RichTextWidget(title: "Allocated Staff:",value: "${allocatedStaff_}"),
                                RichTextWidget(title: "Date Of Inspection Planned:",value: "${inspectionPlan_}"),
                                RichTextWidget(title: "Remark Given: ----",value: ""),
                                Align(alignment:Alignment.centerRight,child: RichTextWidget(title: "",value: "${inspectionScheduledOn_}")),
                              ],
                            ),
                          ))),
                  node: TimelineNode(
                    indicator: DotIndicator(),
                    startConnector: SolidLineConnector(),
                    endConnector: SolidLineConnector(),
                  ),
                ) :
                FixedTimeline.tileBuilder(
                  builder: TimelineTileBuilder.connectedFromStyle(
                    connectorStyleBuilder: (context, index) => ConnectorStyle.solidLine,
                    indicatorStyleBuilder: (context, index) => IndicatorStyle.dot,
                    itemCount: 2,
                    contentsAlign: ContentsAlign.alternating,
                    oppositeContentsBuilder: (context, index) => Card(
                      elevation: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: index==0 ?
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("JOB CARD STATUS: ${status_}".toUpperCase(),style: TextStyle(fontSize: 20)),
                            Divider(thickness: 3,color: Colors.amber),
                            RichTextWidget(title: "Status Changed by:",value: "${userProvider.viewJobCardTimeLineList.isEmpty ? "" : userProvider.viewJobCardTimeLineList.where((element) => element.jobcardid == widget.cardId).first.details?.split("^").first.split(":").last}"),
                            RichTextWidget(title: "Remark Given:",value: "${userProvider.viewJobCardTimeLineList.isEmpty ? "" : userProvider.viewJobCardTimeLineList.where((element) => element.jobcardid == widget.cardId).first.remarks}"),
                            Align(alignment:Alignment.centerRight,child: RichTextWidget(title: "",value: "${inspectionScheduledOn_}")),
                          ],
                        ) :
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("JOB CARD STATUS: OPEN",style: TextStyle(fontSize: 20)),
                            Divider(thickness: 3,color: Colors.red),
                            RichTextWidget(title: "Scheduled By:",value: "${inspectionScheduled_}"),
                            RichTextWidget(title: "Allocated Staff:",value: "${allocatedStaff_}"),
                            RichTextWidget(title: "Date Of Inspection Planned:",value: "${inspectionPlan_}"),
                            RichTextWidget(title: "Remark Given:",value: "sdfghj"),
                            Align(alignment:Alignment.centerRight,child: RichTextWidget(title: "",value: "${inspectionScheduledOn_}")),
                          ],
                        ),
                  ),
                    ),
                ),
              )),
            ),
          ],
        )
      ),
    );
  }
}
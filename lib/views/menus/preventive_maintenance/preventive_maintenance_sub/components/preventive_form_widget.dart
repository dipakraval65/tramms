import 'package:TRAMMS/components/textfield_widget.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PreventiveWidget extends StatelessWidget {
  const PreventiveWidget({
    required this.chooseMonthCtrl,
    required this.voidCallback,
    required this.voidCallbackMonth,
    required this.jobCardCtrl,
  });

  final TextEditingController chooseMonthCtrl;
  final TextEditingController jobCardCtrl;
  final VoidCallback voidCallback;
  final VoidCallback voidCallbackMonth;

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return LayoutBuilder(
      builder: (context, dimens) {
        if (dimens.maxWidth <= kMobileBreakpoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: (){},
                    child: TextFieldWidgetEnabled(controller: jobCardCtrl,iconData: Icons.date_range,hint: "JobCard ID")),
                const SizedBox(height: 20,),
                InkWell(
                    onTap: voidCallbackMonth,
                    child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month")),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Status",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Status',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            hint: Text("Select Status"),
                            value: userProvider.selectedPreventItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveItems(newValue!);
                            },
                            items: userProvider.preventItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Search",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        } else if (dimens.maxWidth > kMobileBreakpoint &&
            dimens.maxWidth <= kTabletBreakpoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: (){},
                    child: Expanded(child: TextFieldWidgetEnabled(controller: jobCardCtrl,iconData: Icons.date_range,hint: "JobCard ID",))),
                const SizedBox(height: 20,),
                InkWell(
                    onTap: voidCallbackMonth,
                    child: Expanded(child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",))),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Status",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Status',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            value: userProvider.selectedPreventItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveItems(newValue!);
                            },
                            items: userProvider.preventItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Search",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        } else if (dimens.maxWidth > kTabletBreakpoint &&
            dimens.maxWidth <= kDesktopBreakPoint) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: (){},
                    child: Expanded(child: TextFieldWidgetEnabled(controller: jobCardCtrl,iconData: Icons.date_range,hint: "JobCard ID",))),
                const SizedBox(height: 20,),
                InkWell(
                    onTap: voidCallbackMonth,
                    child: Expanded(child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",))),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Status",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Status',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            value: userProvider.selectedPreventItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveItems(newValue!);
                            },
                            items: userProvider.preventItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Search",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                InkWell(
                    onTap: (){},
                    child: Expanded(child: TextFieldWidgetEnabled(controller: jobCardCtrl,iconData: Icons.date_range,hint: "JobCard ID",))),
                const SizedBox(height: 20,),
                InkWell(
                    onTap: voidCallbackMonth,
                    child: Expanded(child: TextFieldWidget(controller: chooseMonthCtrl,iconData: Icons.date_range,hint: "Choose Month",))),
                const SizedBox(height: 20,),
                Container(
                  height: 63,
                  color: Colors.white,
                  child: FormField<String>(
                    builder: (FormFieldState<String>
                    state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          labelText:
                          "Select Status",
                          labelStyle: TextStyle(
                              fontWeight:
                              FontWeight.bold,
                              color: Colors.black38),
                          errorStyle: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 14.0),
                          hintText: 'Select Status',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius
                                  .circular(0)),
                        ),
                        child:
                        DropdownButtonHideUnderline(
                          child:
                          DropdownButton<String>(
                            value: userProvider.selectedPreventItem,
                            isDense: true,
                            onChanged: (String? newValue) async{
                              userProvider.changePreventiveItems(newValue!);
                            },
                            items: userProvider.preventItemsList.map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SizedBox(
                      height: 40,
                      width: 300,
                      child: RaisedButton(
                        onPressed: voidCallback,
                        color: kPrimaryColor,
                        child: Text("Search",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        }
      },
    );
  }
}
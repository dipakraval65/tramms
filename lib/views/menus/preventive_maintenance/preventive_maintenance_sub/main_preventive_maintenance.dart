import 'dart:async';
import 'dart:io';
import 'package:TRAMMS/components/custom_grid_layout.dart';
import 'package:TRAMMS/components/my_side_drawer.dart';
import 'package:TRAMMS/controllers/helpers/database_helper.dart';
import 'package:TRAMMS/controllers/helpers/size_config.dart';
import 'package:TRAMMS/controllers/helpers/ui_constants.dart';
import 'package:TRAMMS/controllers/providers/user_provider.dart';
import 'package:TRAMMS/views/menus/preventive_maintenance/preventive_maintenance_sub/components/preventive_upload_form_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'components/preventive_form_widget.dart';

class MainPreventiveMaintenanceScreen extends StatefulWidget {
  final String? title;
  const MainPreventiveMaintenanceScreen(this.title);
  @override
  _MainPreventiveMaintenanceScreenState createState() =>
      _MainPreventiveMaintenanceScreenState();
}

class _MainPreventiveMaintenanceScreenState extends State<MainPreventiveMaintenanceScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DateTime selectedDateFrom = DateTime.now();
  DateTime selectedDateTo = DateTime.now();
  DateTime selectedSearchMonth = DateTime.now();

  TextEditingController scheduleFromCtrl = new TextEditingController(),
      searchFieldCtrl = new TextEditingController(text: "${DateTime.now().month}-${DateTime.now().year}"),
      scheduleToCtrl = new TextEditingController(),jobCardCtrl = new TextEditingController();

  final PdfViewerController _pdfViewerController = PdfViewerController();

  String selectedFileName = "";
  File selectedFilePath = File("");
  int? selectedIndexOfSearch;
  bool validateTextFieldFrom = false,validateSelectedFile = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   loadData();
  }

  loadData()
  async {
    UserProvider userProvider = Provider.of<UserProvider>(context,listen: false);
    userProvider.clearPreventData();
    userProvider.getAllCorridorListApi(context);
    userProvider.getAllInspectionListApi(context);
    userProvider.getBICorridorListApi(context);
    userProvider.getBILineListApi(context);
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);

    return DefaultTabController(
      length: userProvider.userData?.userrole == "cmrl-ex" ||
          userProvider.userData?.userrole == "cmrl-je"
          ? 2
          : 1,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: NavigationDrawerWidget(),
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 01,
          bottom: TabBar(
              labelStyle: GoogleFonts.poppins(
                fontWeight: FontWeight.w600,
              ),
              labelColor: kPrimaryColor,
              indicatorColor: kPrimaryColor,
              tabs: userProvider.userData?.userrole == "cmrl-ex" ||
                  userProvider.userData?.userrole == "cmrl-je"
                  ? [
                  Tab(
                    text: "Upload Preventive Maintenance",
                  ),
              Tab(
                text: "View Preventive Maintenance",
              ),
              ]
                  : [
              Tab(
              text: "Preventive Maintenance",
          ),
          ],
        ),
        leading: Builder(
          builder: (context) => // Ensure Scaffold is in context
          IconButton(
              icon: Icon(
                Icons.menu,
                size: 30,
                color: CustomColors.kPrimaryColor,
              ),
              onPressed: () => Scaffold.of(context).openDrawer()),
        ),
        title: Text(
          "${widget.title.toString().toUpperCase()}",
          style: TextStyle(
              color: CustomColors.kPrimaryColor, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: InkWell(
              onTap: () {

              },
              child: Icon(
                Icons.filter_alt_outlined,
                size: 30,
                color: CustomColors.kPrimaryColor,
              ),
            ),
          )
        ],
      ),
      body: userProvider.userData?.userrole == "cmrl-ex" ||
          userProvider.userData?.userrole == "cmrl-je" ? TabBarView(
        children: [
          Container(
            height: SizeConfig.screenHeight,
            width: double.infinity,
            color: CustomColors.kBackgroundColor,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  PreventiveUploadWidget(
                    chooseMonthCtrl: searchFieldCtrl,
                    jobCardCtrl: jobCardCtrl,
                    voidCallback: () async {

                    },
                    voidCallbackMonth: () {
                      showMonthPicker(
                        context: context,
                        firstDate: DateTime(DateTime.now().year - 1, 5),
                        lastDate: DateTime(DateTime.now().year + 1, 9),
                        initialDate: selectedSearchMonth,
                      ).then((date) {
                        if (date != null) {
                          setState(() {
                            selectedSearchMonth = date;
                            searchFieldCtrl.text =
                            "${date.month}-${date.year}";
                          });
                        }
                      });
                    },
                  ),
                  // Visibility(
                  //     visible: userProvider.viewWeeklyPlanList == null ||
                  //         userProvider.viewWeeklyPlanList.isEmpty
                  //         ? false
                  //         : true,
                  //     child: Padding(
                  //       padding: const EdgeInsets.only(top: 20),
                  //       child: LayoutBuilder(
                  //         builder: (context, dimens) {
                  //           if (dimens.maxWidth <= kMobileBreakpoint) {
                  //             return CustomGridView(
                  //               userProvider: userProvider,
                  //               ratio: 12,
                  //               pdfViewerController: _pdfViewerController,
                  //             );
                  //           } else if (dimens.maxWidth > kMobileBreakpoint &&
                  //               dimens.maxWidth <= kTabletBreakpoint) {
                  //             return CustomGridView(
                  //               userProvider: userProvider,
                  //               ratio: 6,
                  //               pdfViewerController: _pdfViewerController,
                  //             );
                  //           } else if (dimens.maxWidth > kTabletBreakpoint &&
                  //               dimens.maxWidth <= kDesktopBreakPoint) {
                  //             return CustomGridView(
                  //               userProvider: userProvider,
                  //               ratio: 3,
                  //               pdfViewerController: _pdfViewerController,
                  //             );
                  //           } else {
                  //             return CustomGridView(
                  //               userProvider: userProvider,
                  //               ratio: 2,
                  //               pdfViewerController: _pdfViewerController,
                  //             );
                  //           }
                  //         },
                  //       ),
                  //     )),
                ],
              ),
            ),
          ),
          Container(
            height: SizeConfig.screenHeight,
            width: double.infinity,
            color: CustomColors.kBackgroundColor,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  PreventiveWidget(
                    chooseMonthCtrl: searchFieldCtrl,
                    jobCardCtrl: jobCardCtrl,
                    voidCallback: () {
                      if (selectedSearchMonth == null ||
                          selectedSearchMonth.month == "") {
                        Fluttertoast.showToast(
                            msg: "Please Choose Month and Year");
                      } else {
                        if (selectedSearchMonth.month.toInt() <= 9) {
                          userProvider.getPreventiveMaintenanceData(context,"${selectedSearchMonth.year}-0${selectedSearchMonth.month}",
                              "${jobCardCtrl.text.toString()}");
                        } else {
                          userProvider.getPreventiveMaintenanceData(context,"${selectedSearchMonth.year}-${selectedSearchMonth.month}","${jobCardCtrl.text.toString()}");
                        }
                      }
                    },
                    voidCallbackMonth: () {
                      showMonthPicker(
                        context: context,
                        firstDate: DateTime(DateTime.now().year - 1, 5),
                        lastDate: DateTime(DateTime.now().year + 1, 9),
                        initialDate: selectedSearchMonth,
                      ).then((date) {
                        if (date != null) {
                          setState(() {
                            selectedSearchMonth = date;
                            searchFieldCtrl.text =
                            "${date.month}-${date.year}";
                          });
                        }
                      });
                    },
                  ),
                  Visibility(
                      visible: userProvider.viewPreventiveMaintenanceData == null ||
                          userProvider.viewPreventiveMaintenanceData.isEmpty
                          ? false
                          : true,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: LayoutBuilder(
                          builder: (context, dimens) {
                            if (dimens.maxWidth <= kMobileBreakpoint) {
                              return CustomGridViewPMJobData(
                                jobCardCtrl: searchFieldCtrl,
                                userProvider: userProvider,
                                ratio: 12,
                                pdfViewerController: _pdfViewerController,
                              );
                            } else if (dimens.maxWidth > kMobileBreakpoint &&
                                dimens.maxWidth <= kTabletBreakpoint) {
                              return CustomGridViewPMJobData(
                                jobCardCtrl: searchFieldCtrl,
                                userProvider: userProvider,
                                ratio: 12,
                                pdfViewerController: _pdfViewerController,
                              );
                            } else if (dimens.maxWidth > kTabletBreakpoint &&
                                dimens.maxWidth <= kDesktopBreakPoint) {
                              return CustomGridViewPMJobData(
                                jobCardCtrl: searchFieldCtrl,
                                userProvider: userProvider,
                                ratio: 12,
                                pdfViewerController: _pdfViewerController,
                              );
                            } else {
                              return CustomGridViewPMJobData(
                                jobCardCtrl: searchFieldCtrl,
                                userProvider: userProvider,
                                ratio: 12,
                                pdfViewerController: _pdfViewerController,
                              );
                            }
                          },
                        ),
                      )),
                ],
              ),
            ),
          ),
        ],
      ) : TabBarView(
        children: [
          Container(
            height: SizeConfig.screenHeight,
            width: double.infinity,
            color: CustomColors.kBackgroundColor,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  PreventiveWidget(
                    chooseMonthCtrl: searchFieldCtrl,
                    jobCardCtrl: jobCardCtrl,
                    voidCallback: () {
                      if (selectedSearchMonth == null ||
                          selectedSearchMonth.month == "") {
                        Fluttertoast.showToast(
                            msg: "Please Choose Month and Year");
                      } else {
                        if (selectedSearchMonth.month.toInt() <= 9) {
                          userProvider.getPreventiveMaintenanceData(context,"${selectedSearchMonth.year}-0${selectedSearchMonth.month}",
                              "${jobCardCtrl.text.toString()}");
                        } else {
                          userProvider.getPreventiveMaintenanceData(context,"${selectedSearchMonth.year}-${selectedSearchMonth.month}","${jobCardCtrl.text.toString()}");
                        }
                      }
                    },
                    voidCallbackMonth: () {
                      showMonthPicker(
                        context: context,
                        firstDate: DateTime(DateTime.now().year - 1, 5),
                        lastDate: DateTime(DateTime.now().year + 1, 9),
                        initialDate: selectedSearchMonth,
                      ).then((date) {
                        if (date != null) {
                          setState(() {
                            selectedSearchMonth = date;
                            searchFieldCtrl.text =
                            "${date.month}-${date.year}";
                          });
                        }
                      });
                    },
                  ),
                  Visibility(
                      visible: userProvider.viewPreventiveMaintenanceData == null ||
                          userProvider.viewPreventiveMaintenanceData.isEmpty
                          ? false
                          : true,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: LayoutBuilder(
                          builder: (context, dimens) {
                            if (dimens.maxWidth <= kMobileBreakpoint) {
                              return CustomGridViewPMJobData(
                                jobCardCtrl: searchFieldCtrl,
                                userProvider: userProvider,
                                ratio: 12,
                                pdfViewerController: _pdfViewerController,
                              );
                            } else if (dimens.maxWidth > kMobileBreakpoint &&
                                dimens.maxWidth <= kTabletBreakpoint) {
                              return CustomGridViewPMJobData(
                                jobCardCtrl: searchFieldCtrl,
                                userProvider: userProvider,
                                ratio: 12,
                                pdfViewerController: _pdfViewerController,
                              );
                            } else if (dimens.maxWidth > kTabletBreakpoint &&
                                dimens.maxWidth <= kDesktopBreakPoint) {
                              return CustomGridViewPMJobData(
                                jobCardCtrl: searchFieldCtrl,
                                userProvider: userProvider,
                                ratio: 12,
                                pdfViewerController: _pdfViewerController,
                              );
                            } else {
                              return CustomGridViewPMJobData(
                                jobCardCtrl: searchFieldCtrl,
                                userProvider: userProvider,
                                ratio: 12,
                                pdfViewerController: _pdfViewerController,
                              );
                            }
                          },
                        ),
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    ),

    );
  }

  Future<void> _selectDateFrom(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDateFrom,
        builder: (BuildContext? context, Widget? child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme:
              ColorScheme.light(primary: CustomColors.kPrimaryColor),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child!,
          );
        },
        firstDate: DateTime.now().subtract(Duration(days: 3)),
        lastDate: DateTime.now().add(Duration(days: 10)));
    if (picked != null && picked != selectedDateFrom)
      setState(() {
        selectedDateFrom = picked;
        scheduleFromCtrl.text =
        "${"${selectedDateFrom.toLocal()}".split(' ')[0]}";
      });
  }

  Future<void> _selectDateTo(BuildContext context) async {
    print("to........");
    final DateTime? picked = await showDatePicker(
        context: context,
        builder: (BuildContext context, Widget? child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme:
              ColorScheme.light(primary: CustomColors.kPrimaryColor),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child!,
          );
        },
        firstDate: selectedDateTo,
        initialDate: DateTime.now(),
        lastDate: DateTime.now().add(Duration(days: 18)));
    if (picked != null && picked != selectedDateTo)
      setState(() {
        selectedDateTo = picked;
        scheduleToCtrl.text = "${"${selectedDateTo.toLocal()}".split(' ')[0]}";
      });
    print(selectedDateTo);
  }

  void showTopFlash(String msg) {
    Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_LONG);
  }

  Future getPdfAndUpload() async {
    FilePickerResult? file = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: ['pdf']);
    if (file != null) {
      PlatformFile file1 = file.files.first;
      File fileDor = File(file.files.single.path!);
      setState(() {
        selectedFileName = file1.name;
        selectedFilePath = fileDor;
      });
      print(file1.name);
      print(file1.bytes);
      print(file1.size);
      print(file1.extension);
      print(file1.path);
      print("File Path is:" + file1.toString());
      return file1.name;
    }
  }
}

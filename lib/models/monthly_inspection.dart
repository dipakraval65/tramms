class MonthlyInspection {
  String? details;
  String? monthyear;
  String? inspectionname;
  String? inspectioncode;
  String? levelofinspection;
  String? pMUnderGround;
  String? pMEleveted;

  MonthlyInspection(
      {this.details,
        this.monthyear,
        this.inspectionname,
        this.inspectioncode,
        this.levelofinspection,
        this.pMUnderGround,
        this.pMEleveted});

  MonthlyInspection.fromJson(Map<String, dynamic> json) {
    details = json['details'];
    monthyear = json['monthyear'];
    inspectionname = json['inspectionname'];
    inspectioncode = json['inspectioncode'];
    levelofinspection = json['levelofinspection'];
    pMUnderGround = json['PMUnderGround'];
    pMEleveted = json['PMEleveted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['details'] = this.details;
    data['monthyear'] = this.monthyear;
    data['inspectionname'] = this.inspectionname;
    data['inspectioncode'] = this.inspectioncode;
    data['levelofinspection'] = this.levelofinspection;
    data['PMUnderGround'] = this.pMUnderGround;
    data['PMEleveted'] = this.pMEleveted;
    return data;
  }
}
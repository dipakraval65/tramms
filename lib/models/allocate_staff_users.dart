class AllocateStaffUsers {
  String? usermasterid;
  String? employeeid;
  String? loginid;
  String? loginpassword;
  String? username;
  String? emailid;
  String? mobileno;
  String? userrole;
  String? isactive;
  String? designationname;
  String? designationmasterid;

  AllocateStaffUsers(
      {this.usermasterid,
        this.employeeid,
        this.loginid,
        this.loginpassword,
        this.username,
        this.emailid,
        this.mobileno,
        this.userrole,
        this.isactive,
        this.designationname,
        this.designationmasterid});

  AllocateStaffUsers.fromJson(Map<String, dynamic> json) {
    usermasterid = json['usermasterid'];
    employeeid = json['employeeid'];
    loginid = json['loginid'];
    loginpassword = json['loginpassword'];
    username = json['username'];
    emailid = json['emailid'];
    mobileno = json['mobileno'];
    userrole = json['userrole'];
    isactive = json['isactive'];
    designationname = json['designationname'];
    designationmasterid = json['designationmasterid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['usermasterid'] = this.usermasterid;
    data['employeeid'] = this.employeeid;
    data['loginid'] = this.loginid;
    data['loginpassword'] = this.loginpassword;
    data['username'] = this.username;
    data['emailid'] = this.emailid;
    data['mobileno'] = this.mobileno;
    data['userrole'] = this.userrole;
    data['isactive'] = this.isactive;
    data['designationname'] = this.designationname;
    data['designationmasterid'] = this.designationmasterid;
    return data;
  }
}

class AllocateStaffUsersResponse {
  String? message;
  String? jobCardID;

  AllocateStaffUsersResponse({this.message, this.jobCardID});

  AllocateStaffUsersResponse.fromJson(Map<String, dynamic> json) {
    message = json['Message'];
    jobCardID = json['JobCardID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Message'] = this.message;
    data['JobCardID'] = this.jobCardID;
    return data;
  }
}
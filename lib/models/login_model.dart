class LoginModel {
  String? loginid;
  String? username;
  String? userrole;
  String? designationname;

  LoginModel(
      {this.loginid, this.username, this.userrole, this.designationname});

  LoginModel.fromJson(Map<String, dynamic> json) {
    loginid = json['loginid'];
    username = json['username'];
    userrole = json['userrole'];
    designationname = json['designationname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['loginid'] = this.loginid;
    data['username'] = this.username;
    data['userrole'] = this.userrole;
    data['designationname'] = this.designationname;
    return data;
  }
}
class PMJobCardTimeLineData {
  String? trackmasterid;
  String? type;
  String? jobcardid;
  String? employeeid;
  String? status;
  String? details;
  String? remarks;
  String? createddate;

  PMJobCardTimeLineData(
      {this.trackmasterid,
        this.type,
        this.jobcardid,
        this.employeeid,
        this.status,
        this.details,
        this.remarks,
        this.createddate});

  PMJobCardTimeLineData.fromJson(Map<String, dynamic> json) {
    trackmasterid = json['trackmasterid'];
    type = json['type'];
    jobcardid = json['jobcardid'];
    employeeid = json['employeeid'];
    status = json['status'];
    details = json['details'];
    remarks = json['remarks'];
    createddate = json['createddate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['trackmasterid'] = this.trackmasterid;
    data['type'] = this.type;
    data['jobcardid'] = this.jobcardid;
    data['employeeid'] = this.employeeid;
    data['status'] = this.status;
    data['details'] = this.details;
    data['remarks'] = this.remarks;
    data['createddate'] = this.createddate;
    return data;
  }
}
class LogoutData {
  Data? data;
  Activity? activity;

  LogoutData({this.data, this.activity});

  LogoutData.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    activity = json['activity'] != null
        ? new Activity.fromJson(json['activity'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    if (this.activity != null) {
      data['activity'] = this.activity!.toJson();
    }
    return data;
  }
}

class Data {
  String? loginID;

  Data({this.loginID});

  Data.fromJson(Map<String, dynamic> json) {
    loginID = json['LoginID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LoginID'] = this.loginID;
    return data;
  }
}

class Activity {
  String? iP;
  String? deviceUsed;
  String? pageVisted;
  String? action;
  String? location;

  Activity(
      {this.iP,
        this.deviceUsed,
        this.pageVisted,
        this.action,
        this.location,
        });

  Activity.fromJson(Map<String, dynamic> json) {
    iP = json['IP'];
    deviceUsed = json['DeviceUsed'];
    pageVisted = json['PageVisted'];
    action = json['Action'];
    location = json['Location'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['IP'] = this.iP;
    data['DeviceUsed'] = this.deviceUsed;
    data['PageVisted'] = this.pageVisted;
    data['Action'] = this.action;
    data['Location'] = this.location;
    return data;
  }
}
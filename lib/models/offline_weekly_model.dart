class OfflineWeeklyModel {
  String? iP;
  String? pageVisted;
  String? deviceUsed;
  String? action;
  String? txtScheduleFrom;
  String? txtScheduleTo;
  String? hdnLocation;
  String? method;

  OfflineWeeklyModel(
      {this.iP,
        this.pageVisted,
        this.deviceUsed,
        this.action,
        this.txtScheduleFrom,
        this.txtScheduleTo,
        this.hdnLocation,
        this.method});

  OfflineWeeklyModel.fromJson(Map<String, dynamic> json) {
    iP = json['IP'];
    pageVisted = json['PageVisted'];
    deviceUsed = json['DeviceUsed'];
    action = json['Action'];
    txtScheduleFrom = json['txt_ScheduleFrom'];
    txtScheduleTo = json['txt_ScheduleTo'];
    hdnLocation = json['hdn_location'];
    method = json['Method'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['IP'] = this.iP;
    data['PageVisted'] = this.pageVisted;
    data['DeviceUsed'] = this.deviceUsed;
    data['Action'] = this.action;
    data['txt_ScheduleFrom'] = this.txtScheduleFrom;
    data['txt_ScheduleTo'] = this.txtScheduleTo;
    data['hdn_location'] = this.hdnLocation;
    data['Method'] = this.method;
    return data;
  }
}

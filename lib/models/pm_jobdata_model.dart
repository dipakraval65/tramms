class PMJobDataModel {
  String? allocateduser;
  String? scheduledbyuser;
  String? mobileno;
  String? defectname;
  String? preventivemasterid;
  String? jobcardid;
  String? annualplanid;
  String? monthyear;
  String? inspectionid;
  String? corridor;
  String? levelofinspection;
  String? type;
  String? linename;
  String? value;
  String? scheduledby;
  String? scheduledon;
  String? allocatedstaff;
  String? alertNotification;
  String? staffdesignation;
  String? dateofinspectionplanned;
  String? status;
  String? checkliststatus;
  String? billPlanvsactual;
  String? defectnoticed;
  String? pmremarks;
  String? oldstaff;
  String? reasonbycontractor;
  String? remarksbycontractor;
  String? reasonimage;
  String? defectmasterid;
  String? isactive;
  String? createddate;
  String? reallocationby;

  PMJobDataModel(
      {this.allocateduser,
        this.scheduledbyuser,
        this.mobileno,
        this.defectname,
        this.preventivemasterid,
        this.jobcardid,
        this.annualplanid,
        this.monthyear,
        this.inspectionid,
        this.corridor,
        this.levelofinspection,
        this.type,
        this.linename,
        this.value,
        this.scheduledby,
        this.scheduledon,
        this.allocatedstaff,
        this.alertNotification,
        this.staffdesignation,
        this.dateofinspectionplanned,
        this.status,
        this.checkliststatus,
        this.billPlanvsactual,
        this.defectnoticed,
        this.pmremarks,
        this.oldstaff,
        this.reasonbycontractor,
        this.remarksbycontractor,
        this.reasonimage,
        this.defectmasterid,
        this.isactive,
        this.createddate,
        this.reallocationby});

  PMJobDataModel.fromJson(Map<String, dynamic> json) {
    allocateduser = json['allocateduser'];
    scheduledbyuser = json['scheduledbyuser'];
    mobileno = json['mobileno'];
    defectname = json['defectname'];
    preventivemasterid = json['preventivemasterid'];
    jobcardid = json['jobcardid'];
    annualplanid = json['annualplanid'];
    monthyear = json['monthyear'];
    inspectionid = json['inspectionid'];
    corridor = json['corridor'];
    levelofinspection = json['levelofinspection'];
    type = json['type'];
    linename = json['linename'];
    value = json['value'];
    scheduledby = json['scheduledby'];
    scheduledon = json['scheduledon'];
    allocatedstaff = json['allocatedstaff'];
    alertNotification = json['alert_notification'];
    staffdesignation = json['staffdesignation'];
    dateofinspectionplanned = json['dateofinspectionplanned'];
    status = json['status'];
    checkliststatus = json['checkliststatus'];
    billPlanvsactual = json['bill_planvsactual'];
    defectnoticed = json['defectnoticed'];
    pmremarks = json['pmremarks'];
    oldstaff = json['oldstaff'];
    reasonbycontractor = json['reasonbycontractor'];
    remarksbycontractor = json['remarksbycontractor'];
    reasonimage = json['reasonimage'];
    defectmasterid = json['defectmasterid'];
    isactive = json['isactive'];
    createddate = json['createddate'];
    reallocationby = json['reallocationby'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['allocateduser'] = this.allocateduser;
    data['scheduledbyuser'] = this.scheduledbyuser;
    data['mobileno'] = this.mobileno;
    data['defectname'] = this.defectname;
    data['preventivemasterid'] = this.preventivemasterid;
    data['jobcardid'] = this.jobcardid;
    data['annualplanid'] = this.annualplanid;
    data['monthyear'] = this.monthyear;
    data['inspectionid'] = this.inspectionid;
    data['corridor'] = this.corridor;
    data['levelofinspection'] = this.levelofinspection;
    data['type'] = this.type;
    data['linename'] = this.linename;
    data['value'] = this.value;
    data['scheduledby'] = this.scheduledby;
    data['scheduledon'] = this.scheduledon;
    data['allocatedstaff'] = this.allocatedstaff;
    data['alert_notification'] = this.alertNotification;
    data['staffdesignation'] = this.staffdesignation;
    data['dateofinspectionplanned'] = this.dateofinspectionplanned;
    data['status'] = this.status;
    data['checkliststatus'] = this.checkliststatus;
    data['bill_planvsactual'] = this.billPlanvsactual;
    data['defectnoticed'] = this.defectnoticed;
    data['pmremarks'] = this.pmremarks;
    data['oldstaff'] = this.oldstaff;
    data['reasonbycontractor'] = this.reasonbycontractor;
    data['remarksbycontractor'] = this.remarksbycontractor;
    data['reasonimage'] = this.reasonimage;
    data['defectmasterid'] = this.defectmasterid;
    data['isactive'] = this.isactive;
    data['createddate'] = this.createddate;
    data['reallocationby'] = this.reallocationby;
    return data;
  }
}
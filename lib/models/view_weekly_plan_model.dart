class ViewWeeklyPlanModel {
  String? username;
  String? loginid;
  String? weeklyplanid;
  String? path;
  String? filename;
  String? schduleby;
  String? designationmasterid;
  String? schedulefrom;
  String? scheduleto;

  ViewWeeklyPlanModel(
      {this.username,
        this.loginid,
        this.weeklyplanid,
        this.path,
        this.filename,
        this.schduleby,
        this.designationmasterid,
        this.schedulefrom,
        this.scheduleto});

  ViewWeeklyPlanModel.fromJson(Map<String, dynamic> json) {
    username = json['username'].toString();
    loginid = json['loginid'].toString();
    weeklyplanid = json['weeklyplanid'].toString();
    path = json['path'].toString();
    filename = json['filename'].toString();
    schduleby = json['schduleby'].toString();
    designationmasterid = json['designationmasterid'].toString();
    schedulefrom = json['schedulefrom'].toString();
    scheduleto = json['scheduleto'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['loginid'] = this.loginid;
    data['weeklyplanid'] = this.weeklyplanid;
    data['path'] = this.path;
    data['filename'] = this.filename;
    data['schduleby'] = this.schduleby;
    data['designationmasterid'] = this.designationmasterid;
    data['schedulefrom'] = this.schedulefrom;
    data['scheduleto'] = this.scheduleto;
    return data;
  }
}